<?php $listed = []; ?>
<?php if ($breadcrumbs) { ?>

    <div>
		<?php foreach ($breadcrumbs as $id => $item) { ?>

			<?php if (in_array($item['href'], $listed)) {
				continue;
			} ?>

			<?php if (!isset($item['is_last'])) { ?>
                <a href="<?php html($item['href']); ?>" class="breadcrumb" itemprop="url"><span itemprop="title"><?php html($item['title']); ?></span></a>
			<?php } else { ?>
                <span class="breadcrumb"><?php html($item['title']); ?></span>
			<?php } ?>


			<?php $listed[] = $item['href']; ?>

		<?php } ?>
    </div>
<?php } ?>