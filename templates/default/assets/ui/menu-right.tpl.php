<ul class="right">
	<?php foreach ($menu as $id => $item) { ?>
		<?php

		$css_classes = [];
		if ($item['childs_count'] > 0) {
			$css_classes[] = 'folder';
		}
		if (!empty($item['options']['class'])) {
			$css_classes[] = $item['options']['class'];
		}

		$onclick = isset($item['options']['onclick']) ? $item['options']['onclick'] : false;
		$onclick = isset($item['options']['confirm']) ? "return confirm('{$item['options']['confirm']}')" : $onclick;

		$target = isset($item['options']['target']) ? $item['options']['target'] : false;
		$data_attr = '';
		if (!empty($item['data'])) {
			foreach ($item['data'] as $key => $val) {
				$data_attr .= 'data-' . $key . '="' . $val . '" ';
			}
		}
		?>

        <li><a <?php if (!empty($item['title'])) { ?>title="<?php echo html($item['title']); ?>"<?php } ?>
               class="item <?php if ($css_classes) { ?><?php echo implode(' ', $css_classes); ?><?php } ?>"
               href="<?= htmlspecialchars($item['url']) ?>"
		       <?php if ($onclick) { ?>onclick="<?php echo $onclick; ?>"<?php } ?>
		       <?php if ($target) { ?>target="<?php echo $target; ?>"<?php } ?>>
				<?php if (!empty($item['title'])) {
					html($item['title']);
				} ?>
            </a></li>
	<?php } ?>
</ul>
