<style type="text/css">
.content_list_item {
    overflow: hidden;
    position: relative;
    margin-bottom: 15px;
    color: #666;
}

.photo {
    margin-right: 15px;
    position: relative;
    min-height: 90px;
    box-sizing: border-box;

    float: left;
}
.fields {
    padding-top: 15px;
}
.content_list_item .ft_caption a {
    font-size: 21px;
    color: #2c3e50;
    text-decoration: none;
}
</style>

<ul class="links premium_list">
<?php 
if($controllers){
foreach ($controllers as $controller) {

if($controller['controller_info']){
	?>

    <li>
        <a href="/admin/controllers/edit/icontroller/edit/<?=$controller['controller']?>">
            <?=$controller['controller_info']['title']?> <small style="color: #97A5A6"><?=$controller['controller_info']['version']?></small></a>
        <div>Посешение: <b><?=$controller['counter']?> раз</b></div>
    </li>
  <?php } ?>
<?php }

}
 ?>
</ul>				                    

