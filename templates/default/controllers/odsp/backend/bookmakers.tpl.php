<?php
	$this->addBreadcrumb("Букмекеры");


	$this->addToolButton([
		                     'class' => 'save',
		                     'title' => LANG_SAVE,
		                     'href' => null,
		                     'onclick' => "icms.datagrid.submit('{$this->href_to('reorder', 'bookmakers')}')"
	                     ]);
?>
<?php $this->renderGrid($this->href_to('bookmakers_ajax'), $grid); ?>
<div class="buttons">
	<?php echo html_button(LANG_SAVE_ORDER, 'save_button', "icms.datagrid.submit('{$this->href_to('reorder', 'bookmakers')}')"); ?>
</div>