<?php
	$this->addBreadcrumb("Виды прогнозов");


	$this->addToolButton([
		                     'class' => 'save',
		                     'title' => LANG_SAVE,
		                     'href' => null,
		                     'onclick' => "icms.datagrid.submit('{$this->href_to('reorder', 'bettings')}')"
	                     ]);
?>
<?php $this->renderGrid($this->href_to('bettings_ajax'), $grid); ?>
<div class="buttons">
	<?php echo html_button(LANG_SAVE_ORDER, 'save_button', "icms.datagrid.submit('{$this->href_to('reorder', 'bettings')}')"); ?>
</div>