<script src="/templates/default/js/filetree.js"></script>
<link rel="stylesheet" href="/templates/default/css/filetree.css">

 <script src="/templates/default/js/src-min-noconflict/ace.js" type="text/javascript" ></script>
<script src="/templates/default/js/src-min-noconflict/ext-language_tools.js"></script>
<style>
button.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
    background-color: #ccc; 
}

div.panel {
    
    display: none;
    background-color: white;
}
</style>
<?php 
echo '

<span id="line"></span>

<div id="oxir"></div>
<div id="saves"></div>
'; 

?>

<table width="100%">
<tr>
<td width="220" valign="top ">
    <button class="accordion">Контроллер</button>
    <div id="filetree" class="filetree panel"></div>

    <button class="accordion">Шаблон</button>
    <div id="filetemplate" class="filetemplate panel"></div>

    <button class="accordion">Язык</button>
    <div id="filelang" class="filelang panel"></div>


</td>
<td valign="top">

<div id="editor" style="border:solid 1px #BBB;height: 510px; padding:5px;position:relative;"></div>
</td></tr>
</table>
<script type="text/javascript">
<?php
if($options['theme']){
$exp=explode('-', $options['theme']);
$th="$exp[0]/$exp[1]";
}else{
    $th='theme/chrome';
}
?>

    ace.require("ace/ext/language_tools");
    var editor = ace.edit("editor");
    editor.setTheme("ace/<?=$th?>");
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
<?php 
if($options['font_size']){
echo 'fontSize: "'.$options['font_size'].'pt",';
}
else{
echo 'fontSize: "12pt",';
}
?>


        enableLiveAutocompletion: <?php if($options['autocomplate']){ echo $options['autocomplate'];}else{echo "false";}?>,
    });

    editor.session.setMode('ace/mode/php');
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }


$(document).ready( function() {
    editor.session.setMode("ace/mode/php");
    $('#filetree').fileTree({ root: 'system/controllers/<?=$controller?>/', script: '/admin/controllers/edit/icontroller/save', folderEvent: 'click', expandSpeed: 750, collapseSpeed: 750, multiFolder: false }, function(file) { 
    

        $.post('/admin/controllers/edit/icontroller/save', { action: "load", file: file}, function(data){
    var decoded = $('<div/>').html(data.content).text();
    editor.setValue(decoded);
    editor.scrollToLine(1, true, true, function () {});
    editor.gotoLine(1, 0, true);

 $( "#saves" ).html(data.save);
 $("#line").html("Путь файла: "+data.line);

    var type=data.type;
    editor.commands.addCommand({
    name: 'savefile',
    bindKey: {win: 'Ctrl-<?=$options['keyboard_key']?>',  mac: 'Command-<?=$options['keyboard_key']?>'},
    exec: function(editor) {
       savefile(data.line);
    }
});
   
             
        },'json');
    },'json');
/////////////////****************************************////////////////////////////
    $('#filetemplate').fileTree({ root: 'templates/default/controllers/<?=$controller?>/', script: '/admin/controllers/edit/icontroller/save', folderEvent: 'click', expandSpeed: 750, collapseSpeed: 750, multiFolder: false }, function(file) { 
    

        $.post('/admin/controllers/edit/icontroller/save', { action: "load", file: file}, function(data){
    var decoded = $('<div/>').html(data.content).text();
    editor.setValue(decoded);
    editor.scrollToLine(1, true, true, function () {});
    editor.gotoLine(1, 0, true);
    if(data.type=='js'){
        editor.getSession().setMode("ace/mode/javascript");
    }else{
        editor.getSession().setMode("ace/mode/"+data.type);
    }
/*    var mode = getModeByFileExtension(data.line);

    editor.getSession().setMode(mode);*/


 $( "#saves" ).html(data.save);
 $("#line").html("Путь файла: "+data.line);

    var type=data.type;
    editor.commands.addCommand({
    name: 'savefile',
    bindKey: {win: 'Ctrl-<?=$options['keyboard_key']?>',  mac: 'Command-<?=$options['keyboard_key']?>'},
    exec: function(editor) {
       savefile(data.line);
    }
});
   
             
        },'json');
    },'json');

////////////////////////////////**********************************///////////////////////
  $('#filelang').fileTree({ root: 'system/languages/ru/controllers/<?=$controller?>/', script: '/admin/controllers/edit/icontroller/save', folderEvent: 'click', expandSpeed: 750, collapseSpeed: 750, multiFolder: false }, function(file) { 
    

        $.post('/admin/controllers/edit/icontroller/save', { action: "load", file: file}, function(data){
    var decoded = $('<div/>').html(data.content).text();
    editor.setValue(decoded);
    editor.scrollToLine(1, true, true, function () {});
    editor.gotoLine(1, 0, true);

//    var mode = getModeByFileExtension(data.line);

   // editor.getSession().setMode(mode);


 $( "#saves" ).html(data.save);
 $("#line").html("Путь файла: "+data.line);

    var type=data.type;
    editor.commands.addCommand({
    name: 'savefile',
    bindKey: {win: 'Ctrl-<?=$options['keyboard_key']?>',  mac: 'Command-<?=$options['keyboard_key']?>'},
    exec: function(editor) {
       savefile(data.line);
    }
});
   
             
        },'json');
    },'json');

////////////////////////////////**********************************///////////////////////
});
function getModeByFileExtension(path){
    var modelist = ace.require("ace/ext/modelist");
    return modelist.getModeForPath(path).mode;
}

function savefile( file ){
    var content = editor.getValue();

      
    $.post('/admin/controllers/edit/icontroller/save', { action: "save", file: file, content: content}, function(data){
            

            
        if ( data == "ok" ) {

$( "#oxir" ).html("Сохранено");
 $("#oxir").hide(3000);
        } else {
//alert("Ошибка");
        }

    },"json");

};
</script>



<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>