<?php $core = cmsCore::getInstance();
	$user = []; ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php $this->title(); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php $this->addMainCSS("templates/{$this->name}/css/theme-text.css?" . time()); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/theme-layout.css?" . time()); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/theme-gui.css"); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/bookmakers.css"); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/theme-widgets.css"); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/theme-content.css"); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/theme-modal.css"); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/materialize.min.css"); ?>
	<?php $this->addMainJS("templates/default/js/jquery.js"); ?>
	<?php $this->addMainCSS("templates/{$this->name}/css/tipsy.css"); ?>
	<?php $this->addMainJS("templates/{$this->name}/js/jquery.tipsy.js"); ?>
	<?php $this->addMainJS("templates/default/js/jquery-modal.js"); ?>
	<?php $this->addMainJS("templates/{$this->name}/js/core.js?" . time()); ?>
	<?php $this->addMainJS("templates/default/js/modal.js"); ?>
	<?php $this->addMainJS("templates/{$this->name}/js/materialize.min.js?" . time()); ?>
	<?php #$this->addMainJS("templates/{$this->name}/js/closewasyouso.js"); ?>
	<?php #$this->addMainJS("templates/{$this->name}/js/soclose.js"); ?>

	<?php if (cmsUser::isLogged()) { ?>
		<?php $this->addMainJS("templates/default/js/messages.js"); ?>
	<?php } ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/r29/html5.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/livingston-css3-mediaqueries-js/1.0.0/css3-mediaqueries.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<?php $this->head(); ?>
    <meta name="csrf-token" content="<?php echo cmsForm::getCSRFToken(); ?>"/>
</head>
<body id="<?php echo $device_type; ?>_device_type">

<header>
	<?php $this->widgets('slide-left', false, "wrapper_plain"); ?>
    <nav class="nav-extended">
        <div class="nav-wrapper">
			<?php if ($core->uri) { ?>
                <a href="<?php echo href_to_home(); ?>" class="brand-logo center">BetIt</a>
			<?php } else { ?>
                <span class="brand-logo center">BetIt</span>
			<?php } ?>
            <a href="#" data-target="slide-left" id="left_menu_but" class="left sidenav-trigger"><i class="material-icons">menu</i></a>
	        <?php $this->widgets('slide-right', false, "wrapper_plain"); ?>
			<?php $this->breadcrumbs(['strip_last' => false]); ?>
        </div>
    </nav>
</header>
<div class="widget_ajax_wrap" id="widget_pos_left-top"><?php $this->widgets('content-top'); ?></div>
<main>
    <section>
		<?php if ($this->isBody()) { ?>
            <article>
                <div id="controller_wrap"><?php $this->body(); ?></div>
            </article>
		<?php } ?>
        <div class="widget_ajax_wrap" id="widget_pos_left-bottom"><?php $this->widgets('content-bottom'); ?></div>
    </section>
</main>

<?php if ($config->debug && cmsUser::isAdmin()) { ?>
    <div id="debug_block">
		<?php $this->renderAsset('ui/debug', ['core' => $core]); ?>
    </div>
<?php } ?>

</body>
</html>