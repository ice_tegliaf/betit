<?php
	$this->addBreadcrumb($league['sport_title'], "#");
	$this->addBreadcrumb($league["country_name"] ? $league["country_name"] : "Без страны", "##");
	$this->addBreadcrumb($league['title']);
?>
<?php
	function getPath($name) {
		return "/templates/betit/images/" . str_replace(" ", "-", strtolower($name)) . "_l.jpg";
	}

?>


<div class="nav-img" style=" background: url(<?php echo getPath($league['sport_title']) ?>)"></div>
<?php if (!is_array($events) || count($events) == 0): ?>
    <div class="progress">
        <div class="indeterminate cyan"></div>
    </div>
    <script>
        $(function () {
            $.ajax({
                url: "<?=$this->href_to("ajax", ["league_events", $league['id']])?>",
                success: function (text) {
                    console.log(text);
                    if (text === '0')
                        window.location.href = "/";
                    else
                        document.location.reload();
                }
            });

        });
    </script>
<?php else: ?>
    <div class="row row_flex">
		<?php foreach ($events as $event):

			$event['result'] = json_decode($event['result'], true); ?>

            <div class="pinc_event" data-id="<?= $event['id'] ?>" style="margin: 25px;">
                <div class="card card_league">
                    <div class="card-content" style="text-align: center; height: 150px;">
                        <a href="<?= href_to("odsp", "event", $event['id']) ?>">
                            <span class="h2"><b><?= $event['title']; ?></b></span>
                            <br>
                            <br>
                            Дата: <?= date("Y-m-d H-i-s", $event['date_event']); ?>
                            <br>
                            Матч закончен. Счёт:
                            <b><?= "(" . $event['result'][1] . ", " . $event['result'][1] . ")"; ?></b>
                        </a>
                        <?php
								$keys = [];
								$spreadsVal = [];
//								dump($event['periods'][0], false);
								$period = $event['periods'][0];//TODO: тут надо сделать проход по периодам и нарыть с них мин или макс значения коэфов
								$spreads = json_decode($period['spreads'], true); // Run line - гандикап
								$total = json_decode($period['totals'], true); // Over Under
								$teamtotal = json_decode($period['teamTotal'], true); // командный OverUnder
								$moneyline = $period['moneyline']; /////// 1X2 - есть не всегда в первом периоде.
								$koeffs = $moneyline;
								$hdp = null;
								if ($koeffs == null && $spreads[0]['hdp']!=null&& $spreads[0]!= null) {
									$koeffs = $spreads[0];
									$hdp = $koeffs['hdp'];
									unset($koeffs['hdp']);
								}
							?>
                        <div class="spreads">
                            <div class="keys">
								<?php foreach ($koeffs as $key => $t):
									echo("<div>" . $key . "</div>");
								endforeach; ?>
                            </div>
	                        <?php if($hdp !=null){
		                        echo("<div class='handi'>HDP " . $hdp . "</div></a>");
		                        $hdp = null;
	                        } ?>
                            <div class="spreadNames">
								<?php foreach ($koeffs as $line):
									echo("<a href=\"javascript://\" data-target=\"makeforecast\" data-odd=\"$line\" class=\"modal-trigger\"><div class='card'>" . $line . "</div></a>");
								endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<?php endforeach; ?>
    </div>
<?php endif; ?>

<div id="makeforecast" class="forecast_modal modal">
    <div class="modal-content">
        <h4>Сделать прогноз</h4>
        <input type="hidden" id="hdjs">

        <table class="striped" cellspacing="0">
            <tbody id="maindata" data-matchid="<?= $event['id'] ?>" data-sportid="<?= $event['sport_id'] ?>">
            <tr style="font-weight:900;">
                <td>Спорт</td>
                <td>Дата</td>
                <td>Страна<br>Турнир</td>
                <td colspan="2">Событие</td>
            </tr>
            <tr>
                <td>
					<?= $league['sport_title']; ?>
                </td>
                <td id="fc_date"></td>
                <td>
					<?= $league["country_name"] ? $league["country_name"] : "Без страны"; ?>
                    <br>
					<?= $league['title']; ?>
                </td>
                <td id="fc_title"></td>
            </tr>
            <tr style="font-weight:900;">
                <td>Прогноз</td>
                <td>Размер</td>
                <td>Коэфф.</td>
                <td>Контора</td>
                <td>Вид прогноза</td>
            </tr>
            <tr>
                <td id="fc_prog"></td>
                <td><input type="number" step="0.01" min="1" max="100" id="fc_perc"></td>
                <td><label>
                        <input type="checkbox" name="is_express" id="fc_exp">
                        <span>Добавить в купон экспрессов</span>
                    </label></td>
                <td id="fc_coef"></td>
                <td>Pinnacle</td>
                <td>
                    <select class="fan" id="fc_paid">
                        <option value="0">Бесплатный</option>
                        <option value="1">Платный</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
        <textarea id="fc_desc"></textarea>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close"></a>
        <a href="javascript://" id="forecast_agree" class="waves-effect waves-green btn-flat green">OK</a>
        <a href="javascript://" class="modal-action modal-close waves-effect waves-green btn-flat red">Cancel</a>
    </div>
</div>

<script>
    $(function () {
        $('.forecast_modal').modal({
            onOpenStart: function (mw, a) {
                var arr = {};
                arr['event_id'] = $(a).closest(".pinc_event").data("id");
                arr['betting_id'] = $(a).closest(".scope").data("betting");
                arr['period_id'] = $(a).closest(".scope").data("scope");
                arr['handicap'] = $(a).closest(".handi").data("handi");
                arr['bookmaker_id'] = 0;
                arr['column_id'] = $(a).closest(".column").data("column");
                arr['odd'] = $(a).data("odd");

                if (arr['handicap'] === undefined) arr['handicap'] = "0.00";
                console.log(arr);
                $("#fc_prog").text();

                $("#fc_coef").text(arr['odd']);
                $("#fc_perc").val("1");
                $("#fc_book").text("");
                $(mw).find("input#hdjs").val(JSON.stringify(arr));
            },
            onCloseEnd: function (mw, a) {
                $("#fc_prog").text("");
                $("#fc_exp").prop("checked", false);
                $("#fc_perc").val("");
                $("#fc_book").text("");
                $(mw).find("input#hdjs").val("");
            }
        });

        $("#forecast_agree").click(function () {
            var forecast_data = JSON.parse($("input#hdjs").val());
            forecast_data['percent'] = parseInt($("#fc_perc").val());
            forecast_data['is_express'] = $("#fc_exp").prop("checked");
            forecast_data['is_paid'] = parseInt($("#fc_paid").val());
            forecast_data['type'] = "pinc";
            forecast_data['description'] = $("#fc_desc").val();
            if (isNaN(forecast_data['percent']) || forecast_data['percent'] <= 0) {
                $("#fc_perc").addClass("invalid");
                M.toast({
                    html: 'Проверьте поле "Размер"',
                    classes: "pulse",
                    displayLength: 2000
                });
                return;
            }
            forecast_data = JSON.stringify(forecast_data);
            M.Modal.getInstance(document.getElementById("makeforecast")).close();
            $.ajax({
                method: "post",
                url: "<?=href_to("betit", "ajax", "add_forecast")?>",
                data: {
                    forecast: forecast_data
                },
                dataType: "json",
                success: function (data) {
                    if (data === true)
                        M.toast({
                            html: 'Прогноз записан'
                        });
                    else
                        M.toast({
                            html: 'Что-то пошло не так'
                        });
                }
            });
        });
    });
</script>