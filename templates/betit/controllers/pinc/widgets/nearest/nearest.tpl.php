<div class="betit-events-wrap">
    <table cellspacing="0" class="list">
        <thead>
        <tr>
            <td>Дата</td>
            <td>Событие</td>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($events as $event): ?>
            <tr>
                <td>
					<?= date("Y-m-d H-i-s", $event['date_event']); ?>
                </td>
                <td>
                    <a href="<?= href_to("betit", "event", $event['id']) ?>"><?= $event['title']; ?></a>
                </td>
            </tr>
		<?php endforeach; ?>
        </tbody>
    </table>
</div>
