<?php
	$this->addBreadcrumb($league['sport_title_ru'], "#");
	$this->addBreadcrumb($league["country_name"] ? $league["country_name"] : "Без страны", "##");
	$this->addBreadcrumb($league['title'], $this->href_to("league", $event['league_id']));
	$this->addBreadcrumb($event['title']);
	dump($event); ?>

<div class="row"></div>
<div class="row valign-wrapper center-align">
    <div class="col s3">Спорт</div>
    <div class="col s3">Дата</div>
    <div class="col s3">Страна<br>Турнир</div>
    <div class="col s3">Событие</div>
</div>
<div class="row valign-wrapper center-align">
    <div class="col s3"><?= $event['sport_title_ru']; ?></div>
    <div class="col s3"><?= date("Y-m-d H-i-s", $event['date_event']); ?></div>
    <div class="col s3">
		<?= $event['league_country']; ?>
        <br>
        <a href="<?= $this->href_to("league", $event['league_id']); ?>"><?= $event['league_title'] ?></a>
    </div>
    <div class="col s3"><?= $event['title']; ?><a href="<?= $event['url']; ?>" target="_blank">#</a></div>
    <div class="col s3">
        <b><?= (isset($event['result'][0]) ? $event['result'][0] : "") . (isset($event['result'][1]) ? (":" . $event['result'][1]) : "") ?></b>
    </div>
</div>

<div id="match_data"></div>

<div id="makeforecast" class="forecast_modal modal">
    <div class="modal-content">
        <h4>Сделать прогноз</h4>
        <input type="hidden" id="hdjs">

        <table class="striped" cellspacing="0">
            <tbody id="maindata" data-matchid="<?= $event['id'] ?>" data-sportid="<?= $event['sport_id'] ?>">
            <tr style="font-weight:900;">
                <td>Спорт</td>
                <td>Дата</td>
                <td>Страна<br>Турнир</td>
                <td colspan="2">Событие</td>
            </tr>
            <tr>
                <td>
					<?= $event['sport_title_ru']; ?>
                </td>
                <td>
					<?= date("Y-m-d H-i-s", $event['date_event']); ?>
                </td>
                <td>
					<?= $event['league_country']; ?>
                    <br>
					<?= $event['league_title']; ?>
                </td>
                <td>
					<?= $event['title']; ?>
                </td>
            </tr>
            <tr style="font-weight:900;">
                <td>Прогноз</td>
                <td>Размер</td>
                <td>Коэфф.</td>
                <td>Контора</td>
                <td>Вид прогноза</td>
            </tr>
            <tr>
                <td id="fc_prog"></td>
                <td><input type="number" step="0.01" min="1" max="100" id="fc_perc"></td>
                <td><input type="number" step="0.01" min="0" max="100" id="fc_coef"></td>
				<?php /*<td id="fc_coef"></td>*/ ?>
                <td id="fc_book"></td>
                <td>
                    <select id="fc_paid">
                        <option value="0">Бесплатный</option>
                        <option value="1">Платный</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
        <textarea id="fc_desc"></textarea>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close"></a>
        <a href="javascript://" id="forecast_agree"
           class="waves-effect waves-green btn-flat green">OK</a>
        <a href="javascript://" class="modal-action modal-close waves-effect waves-green btn-flat red">Cancel</a>
    </div>
</div>