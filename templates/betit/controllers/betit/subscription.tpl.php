<h1><?= $subscription['title'] ?></h1>
<a href="javascript://" class="subscribe"><?= $is_subscribed ? "Отписаться" : "Подписаться" ?></a><br>
<?php if ($subscription['user_id'] == $user->id): ?><a href="<?= $this->href_to("subscription", [$subscription['id'], "settings"]) ?>">Настройки</a><br><?php endif; ?>

<a href="<?= $this->href_to("subscription", [$subscription['id'], "forecasts"]) ?>">Прогнозы</a>
<a href="<?= $this->href_to("subscription", [$subscription['id'], "statistics"]) ?>">Статистика</a>
<a href="<?= $this->href_to("add_event") ?>">Добавить прогноз через ручной ввод</a>
<a href="<?= $this->href_to("subscription", [$subscription['id'], "verify_self"]) ?>">Верифицировать ручной ввод</a>

<div class="row">
	<?php foreach ($expresses as $express): $sum_perc = 1; ?>
        <div class="row hoverable">
            <div class="col s1 valign-wrapper center-align"><?= $express["bookmaker_name"] ?></div>
            <div class="row col s8">
				<?php foreach ($express['forecasts'] as $forecast): $sum_perc = $sum_perc * floatval($forecast['odd']); ?>
                    <div class="row valign-wrapper center-align hoverable">
                        <div class="col s2">
							<?= str_replace(" ", "<br>", date("Y-m-d H-i-s", $forecast['date_event'])); ?>
                        </div>
                        <div class="col s3">
                            <small><?= $forecast['sport_title_ru'] ?></small>
                            <br>
                            <a href="<?= href_to($forecast["type"] == "self" ? "betit" : $forecast["type"], "event", $forecast["id"]) ?>"><?= $forecast['title']; ?></a>
                        </div>
                        <div class="col s2">
							<?= str_replace(" ", "<br>", date("Y-m-d H-i-s", $forecast['date_forecast'])); ?>
                        </div>
                        <div class="col s2">
							<?= $forecast['betting_name'] ?>
                            <br>
							<?= $forecast['scope_name'] ?>
                            <br>
							<?= $forecast['column_name'] ?>
							<?= $forecast['betting_id'] == 3
							|| $forecast['betting_id'] == 2
							|| $forecast['betting_id'] == 8
							|| $forecast['betting_id'] == 5
							|| $forecast['betting_id'] == 9
								? (($forecast['handicap_value'] > 0) ? "+" . $forecast['handicap_value'] : $forecast['handicap_value']) : "" ?>
                        </div>
                        <div class="col s1">
							<?= $forecast['odd'] ?>
                        </div>
                        <div class="col s3 center-align">
							<?php html($forecast['description']); ?>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
            <form data-sum="<?= $sum_perc ?>" class="col s2 offset-s1" method="post" action="<?= $this->href_to("subscription", [$subscription['id'], "express"]) ?>?express_id=<?=$express['id']?>">
                <div class="">
                    Суммарный коэффициент: <?= round($sum_perc, 2) ?>
                </div>
                <div class="input-field">
                    <input type="number" id="percent-<?= $express['id'] ?>" class="percent_input" name="percent" step="0.01" min="1"
                           max="100" <?= $express['status'] != 0 ? "disabled='disabled'" : "" ?>
                           value="<?= $express['percent'] ?>">
                    <label for="percent-<?= $express['id'] ?>">Ставка</label>
                </div>
                <div class="possible">
                    Возможный выигрыш: <?= $express['status'] == 0 ? "<span>0</span>" : $express['percent'] * $sum_perc ?>
                </div>
				<?php if ($express['status'] == 0 && count($express['forecasts']) != 1): ?>
                    <div>
                        <input type="hidden" name="express_id" value="<?= $express['id'] ?>">
						<?= html_submit("Сохранить и запустить", "submit-open") ?>
                    </div>
				<?php endif; ?>
            </form>
        </div>
	<?php endforeach; ?>
</div>

<?php
	dump($subscription, false); ?>

<script>
    var currentEventRequest = null;
    $(".subscribe").click(function () {
        currentEventRequest = $.ajax({
            type: "POST",
            url: "/betit/subscribe",
            data: {value: <?=$subscription['id']?>},
            beforeSend: function () {
                if (currentEventRequest != null) {
                    currentEventRequest.abort();
                }
            },
            success: function (data) {
                currentEventRequest = null;
                if (data === false)
                    $(".subscribe").text("Подписаться");
                else if (data === true)
                    $(".subscribe").text("Отписаться");
            },
            dataType: "json"
        });
    });
    $(".percent_input").keyup(function () {
        $(this).closest("form").find(".possible span").text(($(this).val() * $(this).closest("form").data("sum")).toFixed(2));
    });

</script>
