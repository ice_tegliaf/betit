<div class="betit-events-wrap">
    <div class="row">
	    <?php foreach ($events as $event): ?>
        <div class="col s3">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                    <span class="card-title">
					<?= date("Y-m-d H-i-s", $event['date_event']); ?></span>
                    <p>I am a very simple card. I am good at containing small bits of information.
                        I am convenient because I require little markup to use effectively.</p>
                </div>
                <div class="card-action">
                    <a href="<?= href_to("betit", "event", $event['id']) ?>"><?= $event['title']; ?></a>
                </div>
            </div>
        </div>
	    <?php endforeach; ?>
    </div>
</div>
<!--
    <table cellspacing="0" class="list">
        <thead>
        <tr>
            <td>Дата</td>
            <td>Событие</td>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($events as $event): ?>
            <tr>
                <td>
					<?= date("Y-m-d H-i-s", $event['date_event']); ?>
                </td>
                <td>
                    <a href="<?= href_to("betit", "event", $event['id']) ?>"><?= $event['title']; ?></a>
                </td>
            </tr>
		<?php endforeach; ?>
        </tbody>
    </table>
-->