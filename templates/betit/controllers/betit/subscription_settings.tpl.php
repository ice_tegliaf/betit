<style>
    #f_bookmakers {
        height: 500px;
        overflow: scroll;
    }
</style>
<div class="row"></div>
<form class="row" method="post">
    <div class="input-field col s2">
        <select name="status" id="status_select">
            <option value="0">Открыта</option>
            <option value="1">Приостановлена</option>
            <option value="2">Закрыта</option>
        </select>
        <label>Статус рассылки</label>
    </div>
    <div class="input-field col s2 hidden status_pause">
        <input type="text" class="datepicker" name="status_pause">
        <label>Остановлена до</label>
    </div>
    <div class="input-field col s2">
		<?= html_submit(LANG_SAVE, "status_set") ?>
    </div>
</form>
<?php
	$this->renderForm($form, $data, [
		'action' => "",
		'method' => 'post',
		'submit' => [
			'title' => LANG_CONTINUE
		]
	], $errors); ?>

<script>
    $('#status_select').change(function () {
        if ($(this).val() === "1") $(".status_pause").removeClass("hidden");
        else $(".status_pause").addClass("hidden");
    }).formSelect();

    M.Datepicker.init(document.querySelectorAll('.datepicker'), {
        autoClose: true,
        disableDayFn: function (date) {
            return Date.parse(date) <= Date.now();
        }
    });
</script>
