<h1><a href="<?= $this->href_to("subscription", $subscription['id']) ?>"><?= $subscription['title'] ?></a></h1>
<a href="javascript://" class="subscribe"><?= $is_subscribed ? "Отписаться" : "Подписаться" ?></a><br>
<?php if ($subscription['user_id'] == $user->id): ?><a href="<?= $this->href_to("subscription", [$subscription['id'], "settings"]) ?>">Настройки</a><br><?php endif; ?>


<a href="<?= $this->href_to("subscription", [$subscription['id'], "forecasts"]) ?>">Прогнозы</a>
<a href="<?= $this->href_to("subscription", [$subscription['id'], "statistics"]) ?>">Статистика</a>

<?php
	$this->addJS("https://pixinvent.com/materialize-material-design-admin-template/vendors/chartjs/chart.min.js");
?>
<form class="row" action="" id="filter_form" method="get">
    <div class="input-field col s5">
        <select id="filter_type" name="type">
            <option value="" disabled selected>Тип фильтра</option>
            <option value="sports">По виду спорта</option>
            <option value="input">По типу ввода</option>
            <option value="bookmakers">По букмекерам</option>
            <option value="bets">По типу ставки</option>
            <option value="time">По времени</option>
        </select>
        <label>Фильтр</label>
    </div>
    <div class="col s6" id="filter_value_field">
    </div>
    <div class="col s1" id="filter_value_field">
        <a class="waves-effect waves-light btn-large" id="filter">Отобрать</a>
    </div>
    <input type="hidden" name="filter" value="1">
</form>
<div style="width:98%;">
    <canvas id="canvas"></canvas>
</div>
<script>
    window.onload = function () {
        var ctx = document.getElementById("canvas").getContext("2d");
        var gradient = ctx.createLinearGradient(1000, 0, 0, 200);
        gradient.addColorStop(0, 'rgba(255, 110, 64, 0.5)');
        gradient.addColorStop(0.5, 'rgba(123, 31, 162 ,0.5)');
        gradient.addColorStop(1, 'rgba(41, 98, 255 ,0.5)');
        new Chart(ctx).Line({
            labels: [<?php foreach($statistics['graph'] as $point): ?><?=$point[0]?>,<?php endforeach; ?>],
            datasets: [{
                label: 'Bankroll',
                fill: true,
                fillColor: gradient,
                data: [<?php foreach($statistics['graph'] as $point): ?><?=$point[1]?>,<?php endforeach; ?>],
                borderColor: 'rgba(255, 110, 64, 1)',
                borderWidth: 1
            }, {
                label: 'Bankroll Flat',
                data: [<?php foreach($statistics['graph_flat'] as $point): ?><?=$point[1]?>,<?php endforeach; ?>],
                borderColor: 'rgba(0, 0, 0, 1)',
                borderWidth: 1
            }]
        }, {
            responsive: true,
            title: {
                display: false
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                backgroundColor: 'rgba(41, 98, 255, 1)'
            },
            hover: {
                mode: 'nearest',
                display: true,
                intersect: false,
                backgroundColor: 'rgba(41, 98, 255, 0.8)'
            },
            scales: {
                xAxes: [{
                    display: true
                }],
                yAxes: [{
                    display: true
                }]
            }
        },);
    };

</script>

<?php if ($statistics) { ?>
    <div style="display: flex;justify-content: space-around;flex-wrap: wrap;">
        <div class="card gradient-45deg-blue-indigo gradient-shadow min-height-100 white-text stat_card card-content ">
            <span class="card-stats-title white-text">Чистая прибыль:</span>
            <span class="card-stats-desc white-text"><?php echo $statistics["profit"]; ?></span>
        </div>
        <div class="card gradient-45deg-indigo-purple gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Чистая прибыль в процентах: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["clear_profit_per"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-purple-deep-orange gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Процент с оборота: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["turnover_per_bet"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-blue-indigo gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Прогнозов сделано: </span>
            <span class="card-stats-desc white-text">
                +<?php echo $statistics["count_of_predict"][0]; ?>
                -<?php echo $statistics["count_of_predict"][1]; ?>
                =<?php echo $statistics["count_of_predict"][2]; ?>
            </span>
        </div>
        <div class="card gradient-45deg-indigo-purple gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Средний размер ставки: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["mean_bet"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-purple-deep-orange gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Оборот: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["turnover_per"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-blue-indigo gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Максимальная просадка: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["dropdown"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-indigo-purple gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">ROI: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["ROI"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-purple-deep-orange gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Рацио: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["racio"]; ?></span>
        </div>


        <div class="card gradient-45deg-blue-indigo gradient-shadow min-height-100 white-text stat_card card-content" style="margin-top:40px;">
            <span class="card-stats-title white-text">Флэт:</span>
            <span class="card-stats-desc white-text"><?php echo $statistics["flat"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-indigo-purple gradient-shadow min-height-100 white-text stat_card card-content" style="margin-top:40px;">
            <span class="card-stats-title white-text">Чистая прибыль флэт:</span>
            <span class="card-stats-desc white-text"><?php echo $statistics["profit_flat"]; ?></span>
        </div>
        <div class="card gradient-45deg-purple-deep-orange gradient-shadow min-height-100 white-text stat_card card-content" style="margin-top:40px;">
            <span class="card-stats-title white-text">Процент с оборота флэт:</span>
            <span class="card-stats-desc white-text"><?php echo $statistics["turnover_flat_per"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-blue-indigo gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Максиальная просадка флэт:</span>
            <span class="card-stats-desc white-text"><?php echo $statistics["dropdown_flat"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-indigo-purple gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">ROI флэт:</span>
            <span class="card-stats-desc white-text"><?php echo $statistics["ROI_flat"]; ?>%</span>
        </div>
        <div class="card gradient-45deg-purple-deep-orange gradient-shadow min-height-100 white-text stat_card card-content">
            <span class="card-stats-title white-text">Рацио флэт: </span>
            <span class="card-stats-desc white-text"><?php echo $statistics["racio_flat"]; ?></span>
        </div>
    </div>
<?php } else { ?>
    <p>Пока считать нечего</p>
<?php } ?>

<script>
    $('select').formSelect();
    var currentEventRequest = null;
    $(".subscribe").click(function () {
        currentEventRequest = $.ajax({
            type: "POST",
            url: "/betit/subscribe",
            data: {value: <?=$subscription['id']?>},
            beforeSend: function () {
                if (currentEventRequest != null) {
                    currentEventRequest.abort();
                }
            },
            success: function (data) {
                currentEventRequest = null;
                if (data === false)
                    $(".subscribe").text("Подписаться");
                else if (data === true)
                    $(".subscribe").text("Отписаться");
            },
            dataType: "json"
        });
    });
    $("#filter_type").change(function () {
        currentEventRequest = $.ajax({
            type: "POST",
            url: "/betit/filter_statistics_field",
            data: {value: $(this).val()},
            beforeSend: function () {
                if (currentEventRequest != null) {
                    currentEventRequest.abort();
                }
            },
            success: function (data) {
                currentEventRequest = null;
                $("#filter_value_field").html(data);
                $('select').formSelect();
                $('.datepicker').datepicker();
            }
        });
    });
    $("#filter").click(function () {
        $("#filter_form").submit();
        return false;
    });
</script>
