<?php
$this->addJS("https://pixinvent.com/materialize-material-design-admin-template/vendors/chartjs/chart.min.js");
?>

<div class="row">
    <div class="col s3">
        <div class="collection">
            <h4 class="collection-item"> <?php echo $capper['nickname'] ?></h4>
            <p class="collection-item"> <?php
                if ($capper === false) echo "Юзер";
                else echo "Капер" ?></p>
            <p class="collection-item"> <?php echo $capper['city_cache'] ?></p>
            <p class="collection-item"> <?php echo $capper['count_subscription'] ?></p>
        </div>
    </div>
    <div class="col s9">

            <?php foreach ($subscriptions as $subscription): ?>
                <div class="col s4 offset-s1" style="height: 300px">
                    <div class="card" style="height: 100%">
                        <div class="card-content black-text" style="height: 100%">
                            <span class="card-title">
                                <a href="<?=$this->href_to("subscription",$subscription['id'])?>"><?php echo $subscription["title"] ?></a>
                            </span>
                            <p> <?php echo $subscription["description"] ?> </p>
                            <p>прогнозов сделано: <?php echo $capper["count_subscription"] ?> </p>
                            <div style="width:98%;">
                                <canvas id="canvas-<?=$subscription['id']?>"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    $(function () {
                        var ctx = document.getElementById("canvas-<?=$subscription['id']?>").getContext("2d");
                        var gradient = ctx.createLinearGradient(1000, 0, 0, 200);
                        gradient.addColorStop(0, 'rgba(255, 110, 64, 0.5)');
                        gradient.addColorStop(0.5, 'rgba(123, 31, 162 ,0.5)');
                        gradient.addColorStop(1, 'rgba(41, 98, 255 ,0.5)');
			            <?php $statistics = $subscription['statistics'];?>
                        new Chart(ctx).Line({
                            labels: [<?php foreach($statistics['graph'] as $point): ?><?=$point[0]?>,<?php endforeach; ?>],
                            datasets: [{
                                label: 'Bankroll',
                                fill: true,
                                fillColor: gradient,
                                data: [<?php foreach($statistics['graph'] as $point): ?><?=$point[1]?>,<?php endforeach; ?>],
                                borderColor: 'rgba(255, 110, 64, 1)',
                                borderWidth: 1
                            }, {
                                label: 'Bankroll Flat',
                                data: [<?php foreach($statistics['graph_flat'] as $point): ?><?=$point[1]?>,<?php endforeach; ?>],
                                borderColor: 'rgba(0, 0, 0, 1)',
                                borderWidth: 1
                            }]
                        }, {
                            responsive: true,
                            title: {
                                display: false
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                backgroundColor: 'rgba(41, 98, 255, 1)'
                            },
                            hover: {
                                mode: 'nearest',
                                display: true,
                                intersect: false,
                                backgroundColor: 'rgba(41, 98, 255, 0.8)'
                            },
                            scales: {
                                xAxes: [{
                                    display: true
                                }],
                                yAxes: [{
                                    display: true
                                }]
                            }
                        },);
                    });

                </script>
            <?php endforeach; ?>


    </div>
</div>

