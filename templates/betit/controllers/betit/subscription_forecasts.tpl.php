<h1><a href="<?=$this->href_to("subscription",$subscription['id'])?>"><?= $subscription['title'] ?></a></h1>

<a href="javascript://" class="subscribe"><?= $is_subscribed ? "Отписаться" : "Подписаться" ?></a><br>
<?php if($subscription['user_id'] == $user->id): ?><a href="<?=$this->href_to("subscription",[$subscription['id'],"settings"])?>">Настройки</a><br><?php endif;  ?>


<a href="<?=$this->href_to("subscription",[$subscription['id'],"forecasts"])?>">Прогнозы</a>
<a href="<?=$this->href_to("subscription",[$subscription['id'],"statistics"])?>">Статистика</a>

<form class="row" action="" id="filter_form" method="get">
    <div class="input-field col s5">
        <select id="filter_type" name="type">
            <option value="" disabled selected>Тип фильтра</option>
            <option value="sports">По виду спорта</option>
            <option value="input">По типу ввода</option>
            <option value="bookmakers">По букмекерам</option>
            <option value="bets">По типу ставки</option>
            <option value="time">По времени</option>
        </select>
        <label>Фильтр</label>
    </div>
    <div class="col s6" id="filter_value_field">
    </div>
    <div class="col s1" id="filter_value_field">
        <a class="waves-effect waves-light btn-large" id="filter">Отобрать</a>
    </div>
    <input type="hidden" name="filter" value="1">
</form>

<ul class="collapsible expandable">
    <li>
        <div class="collapsible-header">Активные</div>
        <div class="collapsible-body">
            <div class="row valign-wrapper center-align">
                <div class="col s2">
                    Дата события
                </div>
                <div class="col s2">
                    Событие
                </div>
                <div class="col s2">
                    Дата прогноза
                </div>
                <div class="col s2">
                    Прогноз
                </div>
                <div class="col s2">
                    Ставка
                </div>
                <div class="col s1">
                    Тип
                </div>
                <div class="col s2">
                    Контора
                </div>
                <div class="col s2">
                    Комментарий
                </div>
            </div>
			<?php foreach ($events as $forecast):
				if ($forecast['is_finished']) continue;
				?>
                <div class="row valign-wrapper center-align hoverable">
                    <div class="col s2">
						<?= str_replace(" ", "<br>", date("Y-m-d H-i-s", $forecast['date_event'])); ?>
                    </div>
                    <div class="col s2">
                        <small><?= $forecast['sport_title_ru'] ?></small>
                        <br>
                        <a href="<?= href_to($forecast["type"] == "self" ? "betit" : $forecast["type"], "event", $forecast["id"]) ?>"><?= $forecast['title']; ?></a>
                    </div>
                    <div class="col s2">
						<?= str_replace(" ", "<br>", date("Y-m-d H-i-s", $forecast['date_forecast'])); ?>
                    </div>
                    <div class="col s2">
						<?= $forecast['betting_name'] ?>
                        <br>
						<?= $forecast['scope_name'] ?>
                        <br>
						<?= $forecast['column_name'] ?>
						<?= $forecast['betting_id'] == 3
						|| $forecast['betting_id'] == 2
						|| $forecast['betting_id'] == 8
						|| $forecast['betting_id'] == 5
						|| $forecast['betting_id'] == 9
							? (($forecast['handicap_value'] > 0) ? "+" . $forecast['handicap_value'] : $forecast['handicap_value']) : "" ?>
                    </div>
                    <div class="col s2">
						<?= $forecast['odd'] ?>
                    </div>
                    <div class="col s1">
						<?php echo $forecast['is_paid'] == 0 ? "Free" : "Paid" ?>
                    </div>
                    <div class="col s2">
						<?= $forecast['bookmaker_name'] ?>
                    </div>
                    <div class="col s2">
						<?php html($forecast['description']); ?>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Завершённые</div>
        <div class="collapsible-body">
            <div class="row valign-wrapper center-align">
                <div class="col s3">
                    Событие
                </div>
                <div class="col s2">
                    Дата события
                </div>
                <div class="col s2">
                    Дата прогноза
                </div>
                <div class="col s2">
                    Результат
                </div>
                <div class="col s2">
                    Прогноз
                </div>
                <div class="col s1">
                    Коэф
                </div>
                <div class="col s1">
                    Ставка
                </div>
                <div class="col s1">
                    Тип
                </div>
                <div class="col s2">
                    Контора
                </div>
                <div class="col s2">
                    Комментарий
                </div>
                <div class="col s2">
                    Процент
                </div>
            </div>
			<?php foreach ($events as $forecast):
				if (!$forecast['is_finished']) continue;
				$date = str_replace(" ", "<br>", date("Y-m-d H-i-s", $forecast['date_event']));
				?>
                <div class="row valign-wrapper center-align hoverable">
                    <div class="col s3">
                        <small><?= $forecast['sport_title_ru'] ?></small>
                        <br>
                        <a href="<?= href_to($forecast["type"] == "self" ? "betit" : $forecast["type"], "event", $forecast["id"]) ?>"><?= $forecast['title']; ?></a>
                    </div>
                    <div class="col s2">
						<?= $date; ?>
                    </div>
                    <div class="col s2">
						<?= str_replace(" ", "<br>", date("Y-m-d H-i-s", $forecast['date_forecast'])); ?>
                    </div>
                    <div class="col s2">
                        <b><?= implode(":", $forecast['final_result']) ?></b>
                        <br>
						<?php if ($forecast['result']) {
							echo "(" . implode(", ", array_map(function ($item) {
									return implode(":", $item);
								}, $forecast['result'])) . ")";
						} ?>
                    </div>
                    <div class="col s2">
						<?= $forecast['betting_name'] ?>
                        <br>
						<?= $forecast['scope_name'] ?>
                        <br>
						<?= $forecast['column_name'] ?>
						<?= $forecast['betting_id'] == 8
						|| $forecast['betting_id'] == 9
							? $forecast['handicap_value'] : "" ?>
						<?= $forecast['betting_id'] == 2 ? $forecast['handicap_value'] : "" ?>
						<?php
							if ($forecast['betting_id'] == 5):
								if ($forecast['column_id'] == 0) { //Home
									echo ($forecast['handicap_value'] > 0) ? "+" . number_format($forecast['handicap_value'], 2) : number_format($forecast['handicap_value'], 2);
								} else { //Away
									echo (-$forecast['handicap_value'] > 0) ? "+" . number_format(-$forecast['handicap_value'], 2) : number_format(-$forecast['handicap_value'], 2);
								}
							endif;
						?>
						<?= $forecast['handicap_type'] == 1 ? "Sets" : ($forecast['handicap_type'] == 2 ? "Games" : "") ?>
                    </div>
                    <div class="col s1">
						<?= $forecast['odd'] ?>
                    </div>
                    <div class="col s1">
						<?= $forecast['percent'] ?>%
                    </div>
                    <div class="col s1">
						<?php echo $forecast['is_paid'] == 0 ? "Free" : "Paid" ?>
                    </div>
                    <div class="col s2">
						<?= $forecast['bookmaker_name'] ?>
                    </div>
                    <div class="col s2">
						<?php html($forecast['description']); ?>
                    </div>
                    <div class="col s2">
                        <p><?= $forecast['success'] ?></p>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </li>
</ul>
<ul class="collapsible">
    <li>
        <div class="collapsible-header">Отладка</div>
        <div class="collapsible-body"><?php dump($events, false); ?></div>
    </li>
</ul>


<script>
    var currentEventRequest = null;
    $(".subscribe").click(function () {
        currentEventRequest = $.ajax({
            type: "POST",
            url: "/betit/subscribe",
            data: {value: <?=$subscription['id']?>},
            beforeSend: function () {
                if (currentEventRequest != null) {
                    currentEventRequest.abort();
                }
            },
            success: function (data) {
                currentEventRequest = null;
                if (data === false)
                    $(".subscribe").text("Подписаться");
                else if (data === true)
                    $(".subscribe").text("Отписаться");
            },
            dataType: "json"
        });
    });
    $('select').formSelect();
    $("#filter_type").change(function () {
        currentEventRequest = $.ajax({
            type: "POST",
            url: "/betit/filter_statistics_field",
            data: {value: $(this).val()},
            beforeSend: function () {
                if (currentEventRequest != null) {
                    currentEventRequest.abort();
                }
            },
            success: function (data) {
                currentEventRequest = null;
                $("#filter_value_field").html(data);
                $('select').formSelect();
                $('.datepicker').datepicker();
            }
        });
    });
    $("#filter").click(function () {
        $("#filter_form").submit();
        return false;
    });
</script>
