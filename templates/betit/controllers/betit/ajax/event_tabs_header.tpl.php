<?php
	cmsCore::loadLib("betit_parser.class", "betit_parser");
?>
    <div class="row">
        <div class="col s12">
            <ul class="bettings_tabs" style="display: flex;">
				<?php foreach ($tabs as $bettingId => $arr): ?>
                    <li class="tab col s3"><a href="#b<?= $bettingId ?>"><?= Parser::getBettingName($bettingId)['name'] ?></a></li>
				<?php endforeach; ?>
            </ul>
        </div>
    </div>

<?php foreach ($tabs as $bettingId => $scopes): ?>
    <div id="b<?= $bettingId ?>" class="betting row">
        <div class="col s12">
            <ul class="scopes_tabs" style="display: flex;">
				<?php foreach ($scopes as $scopeId => $url): ?>
                    <li class="tab col s3"><a href="#s<?= $bettingId ?>_<?= $scopeId ?>"><?= Parser::getScopeName($scopeId) ?></a></li>
				<?php endforeach; ?>
            </ul>
        </div>

		<?php foreach ($scopes as $scopeId => $url): ?>
            <div id="s<?= $bettingId ?>_<?= $scopeId ?>" class="scope row" data-betting="<?= $bettingId ?>" data-scope="<?= $scopeId ?>">
                #s<?= $bettingId ?>_<?= $scopeId ?>
            </div>
		<?php endforeach; ?>
    </div>
<?php endforeach; ?>