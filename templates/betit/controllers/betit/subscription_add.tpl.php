<style>
    #f_bookmakers {
        height: 500px;
        overflow: scroll;
    }
</style>

<?php
	$this->renderForm($form, $data, [
		'action' => "",
		'method' => 'post',
		'submit' => [
			'title' => LANG_CONTINUE
		]
	], $errors);