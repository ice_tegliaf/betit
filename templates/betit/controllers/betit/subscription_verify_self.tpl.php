<form class="" method="post">
	<?php foreach ($forecasts as $forecast): ?>
        <div class="row">
            <div class="col s2 input-field">
                <label>
                    <input type="checkbox" name="events[<?= $forecast['event_id'] ?>][checked]">
                    <span>Подтвердить</span>
                </label>
            </div>
            <div class="col s2 input-field">
                <input id=final_result-<?= $forecast['event_id'] ?> type="text" name="events[<?= $forecast['event_id'] ?>][final_result]">
                <label for="final_result-<?= $forecast['event_id'] ?>">Финальный счет</label>
            </div>
        </div>
        <div class="results" data-id="<?= $forecast['event_id'] ?>">
            <div class="data row">

            </div>
            <a href="javascript://" class="add_result">Добавить тайм</a>
        </div>
	<?php endforeach; ?>
	<?= html_submit(LANG_SAVE) ?>
</form>
<?php dump($forecasts, false); ?>
<div class="hidden result-clone">
    <div class='col s2 input-field'>
        <i class="material-icons prefix delete_result" style="cursor:pointer;">delete</i>
        <input type="text">
        <label></label>
    </div>
</div>
<script>
    $(".add_result").click(function () {
        clone = $(".result-clone .col").clone();
        f_id = $(this).closest(".results").data("id");
        no = $(this).closest(".results").find(".col").length;
        clone.find("input").attr("name", "events[" + f_id + "][result][]").attr("id", "res-" + f_id + "-" + no);
        clone.find("label").attr("for", "res-" + f_id + "-" + no).text("Счёт тайма");
        $(this).closest(".results").find(".data").append(clone);
        $(".delete_result").click(function () {
            $(this).closest(".col").remove();
        });
    });
</script>
