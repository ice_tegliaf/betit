<?php
	function getPictures($name) {
		return "/templates/betit/images/" . str_replace(" ", "-", strtolower($name)) . ".jpg";
	}

?>

<div class="betit-events-wrap">
    <div class="row">
		<?php foreach ($events as $event): ?>
            <div class="col l2 m4 s5" style="margin: 25px;">
                <div class="card">
                    <div class="card-image">
                        <img src="<?= getPictures($event['league']['sport_title']) ?>"/>
                        <span class="card-title">
                        <b><?= $event['title']; ?></b></span>
                    </div>
                    <div class="card-content" style="text-align: left; height: 150px;">
                        <a href="<?= href_to("odsp", "event", $event['id']) ?>">
                            <span class="h2 "><b><?= $event['league']['sport_title_ru']; ?></b></span>
                            <br><br>
                            Страна: <?= $event['league']['country_name']; ?>
                            <br>
                            Лига: <?= $event['league']['title']; ?>
                            <br>
                            Дата: <?= date("Y-m-d H-i-s", $event['date_event']); ?>
                        </a>
                    </div>
                </div>
            </div>
		<?php endforeach; ?>
    </div>
</div>