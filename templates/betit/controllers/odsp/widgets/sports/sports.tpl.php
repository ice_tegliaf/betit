<div class="betit-sports-wrap">
	<?php foreach ($sports as $sport): ?>
        <div class="sport">
            <h3><?= $sport['title_ru'] ?></h3>
            <div class="countries">
				<?php foreach ($countries[$sport['id']] as $country_name => $leagues): ?>
                    <div class="country">
                        <h4><?= $country_name ?></h4>
                        <div class="leagues">
							<?php foreach ($leagues as $league): ?>
                                <div class="league">
                                    <a href="<?= href_to("betit", "league", $league['id']) ?>"><?= $league['title'] ?></a>
                                </div>
							<?php endforeach; ?>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
	<?php endforeach; ?>
</div>
