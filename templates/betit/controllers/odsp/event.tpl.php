<?php
	$names = explode(" - ", $event['title']);
?>
<div class="row"></div>
<div class="row valign-wrapper center-align">
    <div class="col s3">Спорт</div>
    <div class="col s3">Дата</div>
    <div class="col s3">Страна<br>Турнир</div>
    <div class="col s3">Событие</div>
	<?php if ($event['is_finished']): ?>
        <div class="col s3">Счёт</div><?php endif; ?>
</div>
<div class="row valign-wrapper center-align">
    <div class="col s3"><?= $event['sport_title_ru']; ?></div>
    <div class="col s3"><?= date("Y-m-d H-i-s", $event['date_event']); ?></div>
    <div class="col s3">
		<?= $event['league_country']; ?>
        <br>
        <a href="<?= $this->href_to("league", $event['league_id']); ?>"><?= $event['league_title'] ?></a>
    </div>
    <div class="col s3"><?= $event['title']; ?><a href="<?= $event['url']; ?>" target="_blank">#</a></div>
	<?php if ($event['final_result'] != null): ?>
        <div class="col s3">
			<?php if (isset($event['final_result'][1])) {
				?>
                <b><?= $event['final_result'][0] . ":" . $event['final_result'][1] ?></b>
				<?php
			} elseif (isset($event['final_result'][0])) { ?>
                <b><?= $event['final_result'][0] ?></b>
			<?php } ?>
			<?php if ($event['result']) {
				array_walk($event['result'], function (&$item, $key) {
					$item = $item[0] . ":" . $item[1];
				});
				echo "(" . implode(", ", $event['result']) . ")";
			} ?>
        </div>
	<?php endif; ?>
</div>

<div id="progress" class="progress">
    <div class="indeterminate cyan"></div>
</div>

<div id="match_data"></div>

<div id="makeforecast" class="forecast_modal modal">
    <div class="modal-content">
        <h4>Сделать прогноз</h4>
        <input type="hidden" id="hdjs">

        <table class="striped" cellspacing="0">
            <tbody id="maindata" data-matchid="<?= $event['id'] ?>" data-sportid="<?= $event['sport_id'] ?>">
            <tr style="font-weight:900;">
                <td>Спорт</td>
                <td>Дата</td>
                <td>Страна<br>Турнир</td>
                <td colspan="2">Событие</td>
            </tr>
            <tr>
                <td>
					<?= $event['sport_title_ru']; ?>
                </td>
                <td>
					<?= date("Y-m-d H-i-s", $event['date_event']); ?>
                </td>
                <td>
					<?= $event['league_country']; ?>
                    <br>
					<?= $event['league_title']; ?>
                </td>
                <td>
					<?= $event['title']; ?>
                </td>
            </tr>
            <tr style="font-weight:900;">
                <td>Прогноз</td>
                <td>Размер</td>
                <td>Коэфф.</td>
                <td>Контора</td>
                <td>Вид прогноза</td>
            </tr>
            <tr>
                <td id="fc_prog"></td>
                <td><input type="number" step="0.01" min="1" max="100" id="fc_perc" value=""></td>
                <td><label>
                        <input type="checkbox" name="is_express" id="fc_exp">
                        <span>Добавить в купон экспрессов</span>
                    </label></td>
                <td id="fc_coef"></td>
                <td id="fc_book"></td>
                <td>
                    <select class="fan" id="fc_paid">
                        <option value="0" selected>Бесплатный</option>
                        <option value="1">Платный</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
        <textarea id="fc_desc"></textarea>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close"></a>
        <a href="javascript://" id="forecast_agree"
           class="waves-effect waves-green btn-flat green">OK</a>
        <a href="javascript://" class="modal-action modal-close waves-effect waves-green btn-flat red">Cancel</a>
    </div>
</div>

<script>
    function getTab(betting_id, scope_id) {
        $("#s" + betting_id + "_" + scope_id).html("<div class=\"progress\"><div class=\"indeterminate cyan\"></div></div>");
        $.ajax({
            url: "<?=$this->href_to("ajax", ["event_tab", $event['id']])?>",
            data: {betting_id: betting_id, scope_id: scope_id},
            success: function (html) {
                $("#s" + betting_id + "_" + scope_id).html(html);
                $('.collapsible').collapsible();
                $('a[rel=tipsy]').tipsy({fade: true, gravity: 'n',html: true});
            }
        });
    }

    function getTabsHeader() {
        $("#progress").show();
        $.ajax({
            url: "<?=$this->href_to("ajax", ["event_tabs", $event['id']])?>",
            success: function (tabs) {
                $("#match_data").html(tabs);
                $('.bettings_tabs').tabs({onShow: onShowBetting});
                $('.scopes_tabs').tabs({onShow: onShowScope});
                $("#progress").hide();

                M.Tabs.getInstance($(".bettings_tabs")[0]).select($(".betting").first().attr("id"));
            }
        });
    }

    function onShowBetting(tab) {
        M.Tabs.getInstance($(tab).find("ul")[0]).select($(tab).find("div:eq(1)").attr("id"));
    }

    function onShowScope(tab) {
        if ($("#s" + $(tab).data("betting") + "_" + $(tab).data("scope")).data("time") === undefined
            || $("#s" + $(tab).data("betting") + "_" + $(tab).data("scope")).data("time") + 5 * 60 * 1000 <= new Date().getTime()) {
            getTab($(tab).data("betting"), $(tab).data("scope"));
            $("#s" + $(tab).data("betting") + "_" + $(tab).data("scope")).data("time", new Date().getTime());
        }
    }

    $(function () {
        getTabsHeader();
        $('.forecast_modal').modal({
            onOpenStart: function (mw, a) {
                var arr = {};
                arr['event_id'] = $("tbody#maindata").data("matchid");
                arr['betting_id'] = $(a).closest(".scope").data("betting");
                arr['period_id'] = $(a).closest(".scope").data("scope");
                arr['handicap'] = $(a).closest(".handi").data("handi");
                arr['bookmaker_id'] = $(a).closest(".card-content").find(".card-title").data("bookmakerid");
                arr['column_id'] = $(a).closest(".column").data("column");
                arr['odd'] = $(a).data("odd");

                if (arr['handicap'] === undefined) arr['handicap'] = "0.00";
                $("#fc_prog").text(getBettingName(
                    arr['betting_id']) + " " +
                    getScopeName(arr['period_id']) +
                    ((arr['handicap'] !== "0.00") ? "(" + arr['handicap'] + ")" : "") + " " +
                    "(" + getScopeColumnName(arr['betting_id'])[arr['column_id']] + ")"
                );

                $("#fc_coef").text(arr['odd']);
                $("#fc_perc").val("1");
                $("#fc_book").text(getBookmakerName(arr['bookmaker_id']));
                $(mw).find("input#hdjs").val(JSON.stringify(arr));
            },
            onCloseEnd: function (mw, a) {
                $("#fc_prog").text("");
                $("#fc_exp").prop("checked", false);
                $("#fc_perc").val("");
                $("#fc_book").text("");
                $(mw).find("input#hdjs").val("");
            }
        });

        $("#forecast_agree").click(function () {
            var forecast_data = JSON.parse($("input#hdjs").val());
            forecast_data['percent'] = parseInt($("#fc_perc").val());
            forecast_data['is_express'] = $("#fc_exp").prop("checked");
            forecast_data['is_paid'] = parseInt($("#fc_paid").val());
            forecast_data['type'] = "odsp";
            forecast_data['description'] = $("#fc_desc").val();
            if (isNaN(forecast_data['percent']) || forecast_data['percent'] <= 0) {
                $("#fc_perc").addClass("invalid");
                M.toast({
                    html: 'Проверьте поле "Размер"',
                    classes: "pulse",
                    displayLength: 2000
                });
                return;
            }
            forecast_data = JSON.stringify(forecast_data);
            M.Modal.getInstance(document.getElementById("makeforecast")).close();
            $.ajax({
                method: "post",
                url: "<?=href_to("betit", "ajax", "add_forecast")?>",
                data: {
                    forecast: forecast_data
                },
                dataType: "json",
                success: function (data) {
                    if (data === true)
                        M.toast({
                            html: 'Прогноз записан'
                        });
                    else
                        M.toast({
                            html: 'Что-то пошло не так'
                        });
                }
            });
        });
    });
</script>