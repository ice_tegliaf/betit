<?php
	$this->addBreadcrumb($league['sport_title_ru'], "#");
	$this->addBreadcrumb($league['country_name'], "##");
	$this->addBreadcrumb($league['title']);
?>
<?php
	function getPath($name) {
		return "/templates/betit/images/" . str_replace(" ", "-", strtolower($name)) . "_l.jpg";
	}

?>
<div class="nav-img" style=" background: url(<?php echo getPath($league['sport_title'])?>)">
</div>
<?php if (!is_array($events) || count($events) == 0): ?>
    <div class="progress">
        <div class="indeterminate cyan"></div>
    </div>
    <script>
        $(function () {
            $.ajax({
                url: "<?=$this->href_to("ajax", ["league_events", $league['id']])?>",
                success: function (text) {
                    console.log(text);
                    if (text === 0)
                        window.location.href = "/";
                    else
                        document.location.reload();
                }
            });

        });
    </script>
<?php else: ?>
    <div class="row row_flex">
		<?php foreach ($events as $event):
			$event['final_result'] = json_decode($event['final_result'], true); ?>

            <div class="col l2 m4 s5" style="margin: 25px;">
                <div class="card  card_league">
                    <div class="card-content" style="text-align: center; height: 150px;">
                        <a href="<?= href_to("odsp", "event", $event['id']) ?>">
                            <span class="h2"><b><?= $event['title']; ?></b></span>
                            <br>
                            <br>
                            Дата: <?= date("Y-m-d H-i-s", $event['date_event']); ?>
                        <?php if($event['is_finished']=='1'){?>
                            <br>
                            Матч закончен. Счёт:
							<?php if (isset($event['final_result'][1])) {
								?>
                                <b><?= $event['final_result'][0] . ":" . $event['final_result'][1] ?></b>
								<?php
							} elseif (isset($event['final_result'][0])) { ?>
                                <b><?= $event['final_result'][0] ?></b>
							<?php } ?>
							<?php if ($event['result']) {
								array_walk($event['result'], function (&$item, $key) {
									$item = $item[0] . ":" . $item[1];
								});
								echo "(" . implode(", ", $event['result']) . ")";
							} ?>
							<?php } ?>
                        </a>
                    </div>
                </div>
            </div>
		<?php endforeach; ?>
    </div>
<?php endif; ?>