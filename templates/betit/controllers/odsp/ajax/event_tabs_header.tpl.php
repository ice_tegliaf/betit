<?php
	cmsCore::loadLib("betit_odsp_parser.class", "betit_parser");
?>
    <div class="row">
        <div class="col s12">
            <ul class="bettings_tabs" style="display: flex;">
				<?php
					foreach ($bettings as $bettingId => $betting):
						if (!array_key_exists($bettingId, $tabs)): continue; endif; ?>

                        <li class="tab col s3"><a href="#b<?= $bettingId ?>"><?= Parser::getBettingName($bettingId)['name'] ?></a></li>
					<?php endforeach; ?>
            </ul>
        </div>
    </div>

<?php
	foreach ($bettings as $bettingId => $betting):
		if (!array_key_exists($bettingId, $tabs)): continue; endif;
		$scopes = $tabs[$bettingId];
		?>
        <div id="b<?= $bettingId ?>" class="betting row">
            <div class="col s12">
                <ul class="scopes_tabs" style="display: flex;">
					<?php
						foreach ($periods as $scopeId => $period):
							if (!array_key_exists($scopeId, $scopes)): continue; endif;
							?>
                            <li class="tab col s3"><a href="#s<?= $bettingId ?>_<?= $scopeId ?>"><?= Parser::getScopeName($scopeId) ?></a></li>
						<?php endforeach; ?>
                </ul>
            </div>

			<?php
				foreach ($periods as $scopeId => $period):
					if (!array_key_exists($scopeId, $scopes)): continue; endif;
					?>
                    <div id="s<?= $bettingId ?>_<?= $scopeId ?>" class="scope row" data-betting="<?= $bettingId ?>" data-scope="<?= $scopeId ?>">
                        #s<?= $bettingId ?>_<?= $scopeId ?>
                    </div>
				<?php endforeach; ?>
        </div>
	<?php endforeach; ?>
