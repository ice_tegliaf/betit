<ul class="collapsible">
	<?php
		foreach ($odds as $hadnicapName => $handicapContainer):

			$sort_param = -1;
			$sumOdds = [];
			$bookmakersOdds = $handicapContainer['odds'];
			foreach ($bookmakersOdds as $bookmakerOdds) {
				foreach ($bookmakerOdds as $playerName => $bkOdds) {
					if (!isset($sumOdds[$playerName])) $sumOdds[$playerName] = $bkOdds;
					else $sumOdds[$playerName] += $bkOdds;
				}
			}
			$sort_param = array_search(min($sumOdds), $sumOdds);

			uksort($bookmakersOdds, function ($a, $b) use ($bookmakersOdds, $sort_param) {
				return $bookmakersOdds[$b][$sort_param] <=> $bookmakersOdds[$a][$sort_param];
			});
			?>
			<?php $html = null;
			ob_start();

			foreach ($bookmakersOdds as $bookmakerId => $bookmakerOdds) :
				if (!isset($bookmakers[$bookmakerId])): continue; endif;
				$bookmaker = $bookmakers[$bookmakerId];
				$is_in_subscr = $is_capper && $subscription['bookmakers'] != null && array_key_exists($bookmakerId, $subscription['bookmakers']);

				?>
                <div class="col l3 m4 s5" style="margin: 25px;">
                    <div class="card  card_league<?= $is_in_subscr ? " cyan" : "" ?>">
                        <div class="card-content card-event" style="text-align: center; height: 150px;">
                            <a class="card-title" data-bookmakerid="<?= $bookmakerId ?>"><?= $bookmaker['title'] ?></a>
                            <div class="card-action">
								<?php foreach ($bookmakerOdds as $column_id => $odd): ?>
                                    <div style="width: 50px;text-align: center; display: inline-block;">
                                    <span class="column" data-column="<?= $column_id ?>">
                                    <?php
	                                    $odds_str = "";
	                                    if ($is_capper):
	                                    if (isset($handicapContainer['history'][$bookmakerId][$column_id]))
		                                    foreach ($handicapContainer['history'][$bookmakerId][$column_id] as $key => $val)
			                                    $odds_str .= date("d/m/Y h:i", $key) . " — " . $val . "<br>";
                                    ?>
                                        <a href="javascript://" data-target="makeforecast" data-odd="<?= $odd ?>"
                                           class="modal-trigger"
                                           data-position="bottom"
		                                    <?= $odds_str != "" ? 'rel="tipsy" title="' . $odds_str . '"' : "" ?>
                                           style="margin: 0;display: block;">
                                            <?php endif; ?>
		                                    <?= Parser::getColumnName($betting_id, $column_id) ?><br>
		                                    <?= $odd ?>
		                                    <?php if ($is_capper): ?></a><?php endif; ?>
                                    </span>
                                    </div>
								<?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endforeach;
			$html = ob_get_clean(); ?>
			<?php if ($html): ?>
            <li>
				<?php if (count($odds) > 1): ?>
                    <div class="collapsible-header"
                         style="display: block; width: 100%; text-align: center"> <?= $handicapContainer['handicapValue'] ?></div>
                    <div class="collapsible-body handi"
                         data-handi="<?= $handicapContainer['handicapType'] . "|" . $handicapContainer['handicapValue'] ?>">
                        <div style="width: 100%;display: flex;flex-wrap: wrap;justify-content: space-around;"><?= $html ?></div>
                    </div>
				<?php else: ?>
                    <div style="width: 100%;display: flex;flex-wrap: wrap;justify-content: space-around;"><?= $html ?></div>
				<?php endif; ?>
            </li>
		<?php endif; ?>
		<?php endforeach; ?>
</ul>
<script>
    $(document).ready(function () {
        $('.tooltipped').tooltip();
    });
</script>