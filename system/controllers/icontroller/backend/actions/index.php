<?php


/**
* 
*/
class actionIcontrollerIndex extends cmsAction
{
	
	public function run($do=false)
		{
			

        $grid = $this->loadDataGrid('controllers', false, 'admin.grid_filter.controllers');
        
			return cmsTemplate::getInstance()->render('backend/index', array(
            'grid' => $grid
        ));
		}
}