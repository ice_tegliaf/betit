<?php


@error_reporting ( E_ALL ^ E_WARNING ^ E_NOTICE );
@ini_set ( 'display_errors', true );
@ini_set ( 'html_errors', false );
@ini_set ( 'error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE );


@header("Content-type: text/html; charset=".$config['charset']);
/**
* Link
*/
class actionIcontrollerLoad extends cmsAction
{
	
	public function run()
		{
			
		$allowed_extensions = array ("tpl", "css", "js","php","html");
		$_POST['file'] = trim(str_replace( "..", "", urldecode($_POST['file']) ));
		$url = @parse_url ($_POST['file']);
$root = ROOT_DIR . '/templates/';
	$file_path = dirname ($this->clear_url_dir($url['path']));
	$file_name = pathinfo($url['path']);
	$file_name = $file_name['basename'];

	$type = explode( ".", $file_name );
	$type =  end( $type ) ;

	if( !file_exists($root.$file_path."/".$file_name) ) die ("error");

	$content = @htmlspecialchars( file_get_contents( $root.$file_path."/".$file_name ), ENT_QUOTES );
	if(!is_writable($root.$file_path."/".$file_name)) echo " <font color=\"red\">Fail</font>";
	echo <<<HTML

	<br>
HTML;
?>
<style type="text/css" media="screen">
    #editor { 
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        height: 98%;
    }
</style>

<div id="editor"><?=$content?>
</div><?php
echo <<<HTML

<input accesskey="s" onClick="savefile('{$file_path}/{$file_name}')" type="button" class="button-submit" value="Сохранить" style="width:100px; right: 3px; top: 1px; position:absolute;"><br /><br />
HTML;
?>


<script>
// trigger extension
    ace.require("ace/ext/language_tools");
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/chrome");
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: false
    });
	editor.commands.addCommand({
    name: 'savefile',
    bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
    exec: function(editor) {
       savefile("<?php echo "$file_path/$file_name"; ?>");
    }
});

<?php if ($type == "php") { ?>
    editor.session.setMode("ace/mode/php");
<?php } ?>
<?php if ($type == "js") { ?>
    editor.session.setMode("ace/mode/javascript");
<?php } ?>
<?php if ($type == "css") { ?>
    editor.session.setMode("ace/mode/css");
<?php } ?>
<?php if ($type == "html") { ?>
    editor.session.setMode("ace/mode/html");
<?php } ?>
<?php if ($type == "tpl") { ?>
    editor.session.setMode("ace/mode/php");
<?php } ?>
	
</script>
<?php }
	
	public function clear_url_dir($var) 
		{
		if ( is_array($var) ) return "";

		$var = str_ireplace( ".php", "", $var );
		$var = str_ireplace( ".php", ".ppp", $var );
		$var = trim( strip_tags( $var ) );
		$var = str_replace( "\\", "/", $var );
		$var = preg_replace( "/[^a-z0-9\/\_\-]+/mi", "", $var );
	return $var;

		}


}