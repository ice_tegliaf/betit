<?php



/**
* 
*/
class actionIcontrollerSettings extends cmsAction
{
	
	public function run()
	{
		$form = $this->getForm('options');
       $errors = false;
		$is_submitted = $this->request->has( 'submit' );
		$options = cmsController::loadOptions( $this->name );
			if ($is_submitted) {
				$options = $form->parse( $this->request, $is_submitted );
				$errors = $form->validate( $this, $options );
				if (!$errors) {
					cmsController::saveOptions($this->name,$options);

			}
			if ($errors) {
					cmsUser::addSessionMessage( LANG_FORM_ERRORS, 'error' );
				}
	}

	$template=cmsTemplate::getInstance();

	  return $template->render('backend/form_options', array(
            'do' => 'edit',
            'form' => $form,
            'errors' => $errors,
            'options' => $options,
           
        ));
}

}