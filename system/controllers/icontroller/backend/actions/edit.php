<?php


/**
* 
*/
class actionIcontrollerEdit extends cmsAction
{

	public function run($controller=false)
		{
			if(!$controller){cmsCore::error404();}
			$template=cmsTemplate::getInstance();

			cmsCore::loadControllerLanguage($controller);

			$cont=$this->model->getController($controller);
			if($cont){
				$this->model->incrementCount($controller);
			}else{
				$this->model->addController($controller);
			}
			$options=$this->getOptions();
			return $this->cms_template->render('backend/edit',array('controller'=>$controller,'options'=>$options));

		}
}