<?php
define ( 'ROOT_DIR', dirname ( __FILE__ ) );

/**
* Kun 
*/
class actionIcontrollerSave extends cmsAction
{
	
	public function run()
	{
		$options=$this->getOptions();
		$allowed_extensions=explode(',',$options['allow']);
//		$allowed_extensions = array ("tpl", "css", "js","php","html");

		$template=cmsTemplate::getInstance();
		$action=$this->request->get('action');
		if($action=='save'){
		$file=$this->request->get('file');
		$file = trim(str_replace( "..", "", urldecode($file)));

		if(!$file) { return $template->renderJSON('error');}
		
		$url = @parse_url ( $file);
		$root_dir=cmsConfig::get('root_path');
		$root = $root_dir . '/';
		$file_path = dirname ($this->clear_url_dir($url['path']));
		$file_name = pathinfo($url['path']);
		$file_name = $file_name['basename'];

		$type = explode( ".", $file_name );
		$type = end($type);
	


		if(!file_exists($root.$file_path."/".$file_name) ){return $template->renderJSON('error');}

		if(!cmsCore::isWritable($root.$file_path."/".$file_name)) { echo " <font color=\"red\">Нет файла</font>"; die (); }

		$handle = fopen( $root.$file_path."/".$file_name, "w" );
		fwrite( $handle, $_POST['content'] );
		fclose( $handle );
		}else
		if($action=='load'){

		$file=$this->request->get('file');
		$file = trim(str_replace( "..", "", urldecode($file)));
		$url = @parse_url ($file);
		$root_dir=cmsConfig::get('root_path');
		$root = $root_dir . '/';
		$file_path = dirname ($this->clear_url_dir($url['path']));
		$file_name = pathinfo($url['path']);
		$file_name = $file_name['basename'];
		$type = explode( ".", $file_name );
		$type =  end( $type ) ;

	if( !file_exists($root.$file_path."/".$file_name) ){return $template->renderJSON('error');}
	$content = @htmlspecialchars( file_get_contents( $root.$file_path."/".$file_name ), ENT_QUOTES );
	if(!is_writable($root.$file_path."/".$file_name)){return $template->renderJSON('error');}
	$fu="'$file_path/$file_name'";
	$file_url="$file_path/$file_name";
	$save_button='<input accesskey="s" onClick="savefile('.$fu.')" type="button" class="button-submit" value="Сохранить"
	style="width: 113px;right: 23px;top: 123px;
    position: absolute;"><br /><br />';
		return $template->renderJSON(array(
	'content'=>$content,
	'type'=>$type,
	'save'=>$save_button,
	'line'=>$file_url
		));
		}else{

		$root_dir=cmsConfig::get('root_path');
		$root = $root_dir . '/';
		$dir=$this->request->get('dir');


		if( file_exists($root . $dir) ) {
		$files = scandir($root . $dir);
		natcasesort($files);
		$subdata='';
		if( count($files) > 2 ) {
			$subdata.="<ul class=\"jqueryFileTree\" style=\"display: none;\">";
			// All dirs
			foreach( $files as $file ) {
				if( file_exists($root . $dir . $file) && $file != '.' && $file != '..' && is_dir($root . $dir . $file) ) {
					$subdata.= "<li class=\"directory collapsed\"><a href=\"#\" rel=\"" . htmlentities($_POST['dir'] . $file) . "/\">" . htmlentities($file) . "</a></li>";
				}
			}
			// All files
			foreach( $files as $file ) {
				if( file_exists($root . $dir . $file) && $file != '.' && $file != '..' && !is_dir($root . $dir . $file) ) {
					$serverfile_arr = explode( ".", $file );
					$ext =  end( $serverfile_arr ) ;
	
					if ( in_array( $ext, $allowed_extensions ) )
						$subdata.= "<li class=\"file ext_$ext\"><a href=\"#\" rel=\"" . htmlentities($_POST['dir'] . $file) . "\">" . htmlentities($file) . "</a></li>";
				}
			}
			$subdata.= "</ul>";	
		}
	}
			return $this->controller->halt($subdata);

		}

	

			
			

	}
	public function clear_url_dir($var) {
	if ( is_array($var) ) return "";

	$var = str_ireplace( ".php", "", $var );
	$var = str_ireplace( ".php", ".ppp", $var );
	$var = trim( strip_tags( $var ) );
	$var = str_replace( "\\", "/", $var );
	$var = preg_replace( "/[^a-z0-9\/\_\-]+/mi", "", $var );
	return $var;

}
}