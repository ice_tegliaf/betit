<?php

function grid_controllers($controller){

    $denied = array(
        'admin','auth','markitup','images','content','moderation','users','wall','tags','icontroller'
    );

    $options = array(
        'order_by' => 'title',
        'is_pagination' => false,
    );

    $columns = array(
        'title' => array(
            'title'        => LANG_TITLE,
            'href'         => href_to('admin', 'controllers', array('edit','icontroller','edit','{name}')),
           // 'filter'       => 'like',
            'href_handler' => function($item) {
                return $item['is_backend'];
            }
        ),

      
        'version' => array(
            'title' => LANG_VERSION,
            'width' => 150,
           // 'filter' => 'like'
        ),
        'author' => array(
            'title' => LANG_AUTHOR,
            'width' => 150,
            'href' => '{url}',
           // 'filter' => 'like'
        )
    );

    $actions = array(


    );

    return array(
        'options' => $options,
        'columns' => $columns,
        'actions' => $actions
    );

}
