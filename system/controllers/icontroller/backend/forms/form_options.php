<?php

class formIcontrollerOptions extends cmsForm {

    public function init() {

        return array(

            array(
                'type' => 'fieldset',
                'childs' => array(

                    new fieldString('allow', array(
                        'title' => 'Допустимые расширения',
                        'hint'=>'Список расширений через запятую',
                    )),
                    new fieldString('keyboard_key', array(
                        'title' => 'Клавиши для Сохранить',
                        'hint'=>'Например S. Windows: Ctrl+S Mac-OS: Command+S',
                    )),
                    new fieldString('default_template', array(
                        'title' => 'Папка шаблона',
                        'hint'=>'Например "default".',
                    )),
                    new fieldString('default_lang', array(
                        'title' => 'Папка локализация',
                        'hint'=>'Например "ru".',
                        'default'=>'ru'
                    )),
                    new fieldList('theme', array(
                        'title' => 'Костумизация редактора',
                        'default'=>'theme-chrome',
                        'generator' => function($item) {

                            $theme = cmsCore::getFilesList('/templates/default/js/src-min-noconflict','theme-*.js',true);


                            $items = array();

                            if ($theme) {
                                foreach ($theme as $item) {
                                    $items[$item] = $item;
                                }
                            }

                            return $items;

                        }

                    )),                    
                    new fieldList('font_size', array(
                        'title' => 'Размер шрифта',
                        'default'=>'14',
                        'items'=>array(
                            '12'=>'12',
                            '13'=>'13',
                            '14'=>'14',
                            '15'=>'15',
                            '16'=>'16',
                            '17'=>'17',
                            '18'=>'18',
                            '19'=>'19',
                            '20'=>'20',
                            '21'=>'21',
                            '22'=>'22',
                            '23'=>'23',
                            '24'=>'24'
                        )

                    )),
                    new fieldCheckbox('autocomplate', array(
                        'title' => 'Автозаполнения теги',
                        'default'=>0

                    )),

                )
            )

        );

    }

}
