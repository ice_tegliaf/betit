<?php

class modelIcontroller extends cmsModel {
    
    public function getController($controller)
    {

    	return $this->getItemByField('icontroller_log','controller',$controller);
    }
    public function incrementCount($controller)
    {
    	$this->filterEqual('controller',$controller);
    	return $this->increment('icontroller_log','counter');
    }
    public function addController($controller)
    {
    	$data['controller']=$controller;
    	$data['counter']=1;

    	return $this->insert('icontroller_log',$data);
    }
    public function getControllers()
    {
    	$this->orderBy('counter','desc');
    	$this->limit(5);
    	return $this->get('icontroller_log',function($item,$model)
        {
            $item['controller_info']=$model->getControllerInfo($item['controller']);
            return $item;
        });
    }
    public function getControllerInfo($controller)
    {
        $this->filterEqual('name',$controller);
        return $this->getItem('controllers');
    }
}
