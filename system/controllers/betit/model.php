<?php

	class modelBetit extends cmsModel {

		public function isCapper($id) {
			return $this->getItemByField("betit_subscriptions", "user_id", $id) != false;
		}

		public function getCappers() {
			return $this
				->joinRight("users", "u", "u.id=i.user_id")
				->selectOnly("u.*")
				->groupBy("i.user_id")
				->get("betit_subscriptions", function ($item, $model) {
					$item['active_subscription'] = $model->getActiveSubscription($item['id']);
					$item['count_subscription'] = $model->getSubscriptionsCount($item['id']);
					$this->resetFilters();
					if (is_array($item['active_subscription']))
						$item['count_followers'] = $model->getSubscriptionFollowersCount($item['active_subscription']['id']);
					else
						$item['count_followers'] = 0;
					$this->resetFilters();
					return $item;
				});
		}

		public function getCapper($id) {
			return $this->getItemById("users", $id, function ($item, $model) {
				$item['active_subscription'] = $model->getActiveSubscription($item['id']);
				$item['count_subscription'] = $model->getSubscriptionsCount($item['id']);
				return $item;
			});
		}

		public function getSubscriptions($user_id = null) {
			if (is_null($user_id))
				return $this->get("betit_subscriptions", function ($item) {
					$item['bookmakers'] = explode(",", $item['bookmakers']);
					return $item;
				});

			return $this
				->filterEqual("i.user_id", $user_id)
				->get("betit_subscriptions", function ($item) {
					$item['bookmakers'] = explode(",", $item['bookmakers']);
					return $item;
				});
		}

		public function getSubscriptionsCount($user_id) {
			return $this
				->filterEqual("i.user_id", $user_id)
				->getCount("betit_subscriptions");
		}

		public function getActiveSubscription($user_id) {
			return $this
				->filterEqual("i.user_id", $user_id)
				->filterEqual("i.status", 0)
				->orderBy("id", "desc")
				->getItem("betit_subscriptions", function ($item) {
					$item['bookmakers'] = json_decode($item['bookmakers'], true);
					return $item;
				});
		}


		public function getSubscription($id) {
			return $this->getItemById("betit_subscriptions", $id, function ($item) {
				$item['bookmakers'] = json_decode($item['bookmakers'], true);
				return $item;
			});
		}

		public function isUserSubscribed($subscription_id, $user_id) {
			return $this
					->filterEqual("subscription_id", $subscription_id)
					->filterEqual("user_id", $user_id)
					->getItem("betit_subscription_followers") != false;
		}

		public function toggleSubscribe($subscription_id, $user_id) {
			if ($this->isUserSubscribed($subscription_id, $user_id)) {
				$this
					->filterEqual("subscription_id", $subscription_id)
					->filterEqual("user_id", $user_id)
					->deleteFiltered("betit_subscription_followers");
				return false;
			} else {
				$this->insert("betit_subscription_followers", [
					"subscription_id" => $subscription_id,
					"user_id" => $user_id,
				]);
				return true;
			}
		}

		public function getSubscriptionFollowersCount($id) {
			return $this
				->filterEqual("i.subscription_id", $id)
				->getCount("betit_subscription_followers", "subscription_id");
		}

		public function getSubscriptionFollowers($id) {
			return $this
				->joinRight("users", "u", "u.id=i.user_id")
				->selectOnly("u.*")
				->filterEqual("i.subscription_id", $id)
				->get("betit_subscription_followers", function ($item) {
					return $item;
				});
		}

		public function addSubscription($data) {
			$data['bookmakers'] = json_encode($data['bookmakers']);
			$data['is_paid'] = $data['is_paid'] == null ? 0 : 1;
			return $this->insert("betit_subscriptions", $data);
		}

		public function updateSubscription($id, $data) {
			$data['bookmakers'] = json_encode($data['bookmakers']);
			$data['is_paid'] = $data['is_paid'] == null ? 0 : 1;
			return $this->update("betit_subscriptions", $id, $data);
		}

///////////////////////////////EXPRESSes///////////////////////////////
		public function getSubscriptionExpresses($subscription_id) {
			return $this
				->joinRight("betit_odsp_bookmakers", "bm", "bm.id=i.bookmaker_id")
				->select("bm.title", "bookmaker_name")
				->filterEqual("i.subscription_id", $subscription_id)
				->get("betit_expresses", function ($item, $model) {
					$item['forecasts'] = $model
						->filterEqual("express_id", $item['id'])
						->getSubscriptionForecasts($item['subscription_id']);
					return $item;
				});
		}

		public function getSubscriptionExpressForecastsCount($express_id) {
			return $this->filterEqual("express_id", $express_id)->getCount("betit_forecasts");
		}

		public function getActiveSubscriptionExpress($subscription_id, $type, $bookmaker_id) {
			$ret = $this
				->filterEqual("i.subscription_id", $subscription_id)
				->filterEqual("i.type", $type)
				->filterEqual("i.bookmaker_id", $bookmaker_id)
				->filterEqual("i.status", 0)
				->orderBy("id", "desc")
				->getItem("betit_expresses");
			if ($ret)
				return $ret;
			$i = $this->insert("betit_expresses", [
				"subscription_id" => $subscription_id,
				"type" => $type,
				"bookmaker_id" => $bookmaker_id,
			]);
			return $this->getItemById("betit_expresses", $i);
		}

		/////////////////////////////////////// OTHER

		public function getBettings() {
			$this->orderBy("ordering", 'asc')->filterEqual("is_enabled", "1");
			return $this->useCache("odsp.bettings")->get("betit_odsp_bettings");
		}

		public function getPeriods($sport_id = false) {
			if ($sport_id)
				$this->filterEqual("sport_id", $sport_id);
			return $this->orderBy("ordering", 'asc')->get("betit_pinc_periods");
		}

		public function addItem($item) {
			return $this->insert("betit_events", $item);
		}

		public function getEvents() {
			return $this
				->useCache("betit.events")
				->get("betit_events");
		}

		public function getLeagueEvents($league_id) {
			return $this
				->useCache("betit.events")
				->filterEqual("is_enabled", 1)
				->filterEqual("league_id", $league_id)
				->get("betit_events");
		}


		public function getLeagues() {
			$items = $this->filterEqual("is_enabled", 1)->get("betit_pinc_leagues");
			return $items;
		}

		public function addLeague($league) {
			return $this->insert("betit_pinc_leagues", $league);
		}

		public function getSports() {
			return $this
				->useCache("pinc.sports")
				->filterEqual("is_enabled", 1)
				->get("betit_pinc_sports");
		}

		public function getLeague($league_id) {
			return $this->useCache("betit.leagues")
			            ->joinRight("betit_pinc_sports", "bs", "bs.id=i.sport_id")
			            ->select("bs.title", "sport_title")
			            ->select("bs.title_ru", "sport_title_ru")
			            ->getItemById("betit_pinc_leagues", $league_id, function ($item) {
				            unset($item['url']);
				            return $item;
			            });
		}

		public function getBookmakers() {
			$this->orderBy("ordering", 'asc')->filterEqual("is_enabled", "1");
			return $this->useCache("betit.bookmakers")->get("betit_odsp_bookmakers");
		}

		//***************************** Forecasts **************************


		public function addForecast($forecast) {
			$forecast['subscription_id'] = $this->getActiveSubscription(cmsUser::getInstance()->id)['id'];
			$forecast['date_forecast'] = time();
			if ($forecast['is_express'] && $forecast['type'] != "self") {
				$express = $this->getActiveSubscriptionExpress($forecast['subscription_id'], $forecast['type'], $forecast['bookmaker_id']);
				if ($this->getSubscriptionExpressForecastsCount($express['id']) < 3)
					$forecast['express_id'] = $express['id'];
				else
					return false;
			}
			unset($forecast['is_express']);
			return $this->insert("betit_forecasts", $forecast);
		}

		public function getSubscriptionForecasts($subscription_id, $what = null) {
			cmsCore::loadLib("betit_odsp_parser.class", "betit_parser");
			$filters = $this->where;
			$filter_on = $this->filter_on;
			$items = [];
			if (is_null($what) || $what === false) {
/////FOR ODSP
				$_items = $this
					->joinRight("betit_odsp_events", "e", "e.id=i.event_id")
					->select("e.*")
					->joinRight("betit_odsp_bookmakers", "bm", "bm.id=i.bookmaker_id")
					->select("bm.title", "bookmaker_name")
					->joinRight("betit_odsp_leagues", "bl", "bl.id=e.league_id")
					->select("bl.title", "league_title")
					->select("bl.country_name", "league_country")
					->select("bl.sport_id", "sport_id")
					->joinRight("betit_odsp_sports", "bs", "bs.id=bl.sport_id")
					->select("bs.title", "sport_title")
					->select("bs.title_ru", "sport_title_ru")
					->filterEqual("subscription_id", $subscription_id)
					->filterEqual("type", "odsp")
					->select("i.id", "id")
					->get("betit_forecasts", function ($item) {
						$item['result'] = json_decode($item['result'], true);
						$item['final_result'] = json_decode($item['final_result'], true);
						$item['betting_name'] = Parser::getBettingName($item['betting_id'])['name'];
						$item['scope_name'] = Parser::getScopeName($item['period_id']);
						$item['column_name'] = Parser::getColumnName($item['betting_id'], $item['column_id']);
						list($item['handicap_type'], $item['handicap_value']) = strpos($item['handicap'], "|") !== false ? explode("|", $item['handicap']) : [0, $item['handicap']];

						return $item;
					});
				if (is_array($_items))
					$items = array_merge($items, $_items);
/// SAME FOR PINC
				$this->filter_on = $filter_on;
				$this->where = $filters;
				$_items = $this
					->joinRight("betit_events", "e", "i.event_id=e.id")
					->select("e.*")
					->joinLeft("betit_pinc_leagues", "bl", "bl.id=e.league_id")
					->select("bl.title", "league_title")
					->select("bl.country_name", "league_country")
					->select("bl.sport_id", "sport_id")
					->joinRight("betit_pinc_sports", "bs", "bs.id=bl.sport_id")
					->select("bs.title", "sport_title")
					->select("bs.title_ru", "sport_title_ru")
					->joinRight("betit_pinc_periods", "p", "p.sport_id=bl.sport_id AND p.number=i.period_id")
					->select("p.title", "period_name")
					->filterEqual("subscription_id", $subscription_id)
					->filterEqual("type", "pinc")
					->select("i.id", "id")
					->get("betit_forecasts", function ($item) {
						list($item['handicap_type'], $item['handicap_value']) = strpos($item['handicap'], "|") !== false ? explode("|", $item['handicap']) : [0, $item['handicap']];
						return $item;
					});

				if (is_array($_items))
					$items = array_merge($items, $_items);
			}

/// SAME FOR SELF

			if (is_null($what) || $what === true) {
				$this->filter_on = $filter_on;
				$this->where = $filters;
				$_items = $this
					->joinRight("betit_events", "e", "i.event_id=e.id")
					->select("e.*")
					->joinRight("betit_odsp_bookmakers", "bm", "bm.id=i.bookmaker_id")
					->select("bm.title", "bookmaker_name")
					->joinLeft("betit_pinc_leagues", "bl", "bl.id=e.league_id")
					->select("bl.title", "league_title")
					->select("bl.country_name", "league_country")
					->select("bl.sport_id", "sport_id")
					->joinRight("betit_pinc_sports", "bs", "bs.id=bl.sport_id")
					->select("bs.title", "sport_title")
					->select("bs.title_ru", "sport_title_ru")
					->filterEqual("subscription_id", $subscription_id)
					->filterEqual("type", "self")
					->select("i.id", "id")
					->get("betit_forecasts", function ($item) {
						$item['result'] = json_decode($item['result'], true);
						$item['final_result'] = json_decode($item['final_result'], true);
						$item['periods'] = json_decode($item['periods'], true);
						$item['betting_name'] = Parser::getBettingName($item['betting_id'])['name'];
						$item['scope_name'] = Parser::getScopeName($item['period_id']);
						$item['column_name'] = Parser::getColumnName($item['betting_id'], $item['column_id']);
						list($item['handicap_type'], $item['handicap_value']) = strpos($item['handicap'], "|") !== false ? explode("|", $item['handicap']) : [0, $item['handicap']];

						return $item;
					});

				if (is_array($_items))
					$items = array_merge($items, $_items);

			}
			return $items;
		}
	}
