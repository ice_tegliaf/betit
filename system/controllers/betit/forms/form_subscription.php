<?php

	class formBetitSubscription extends cmsForm {

		public function init() {
			/** @var modelBetit $model */
			$model = cmsCore::getModel("betit");
			$sports = $model->getSports();
			foreach ($sports as $key => &$sport) {
				$sport = $sport['title'];
			}
			$sports = array_merge([0 => ""], $sports);
			$bookmakers = $model->getBookmakers();
			foreach ($bookmakers as $key => &$bookmaker) {
				$bookmaker = $bookmaker['title'];
			}
			return [

				'league' => [
					'type' => 'fieldset',
					'title' => "Рассылка",
					'childs' => [
						new fieldString("title", [
							"title" => "Название"
						]),
						new fieldString("title_short", [
							"title" => "Короткое название"
						]),
						new fieldText("description", [
							"title" => "Описание"
						]),
						new fieldCheckbox('is_paid', [
							'title' => "Платная рассылка",
						]),
						new fieldNumber('start_bankroll', [
							'title' => "Стартовый банкролл",
						]),
						new fieldNumber('default_bet', [
							'title' => "Ставка по-умолчанию",
						]),
						new fieldList('default_sport_id', [
							'title' => "Вид спорта по-умолчанию",
							'items' => $sports
						]),

					],

				],

				'bookmakers' => [
					'type' => 'fieldset',
					'title' => "Букмекеры",
					'childs' => [
						new fieldListMultiple('bookmakers', [
							'items' => $bookmakers
						]),
					]
				]
			];

		}

	}
