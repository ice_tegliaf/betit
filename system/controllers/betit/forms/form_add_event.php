<?php

	class formBetitAddEvent extends cmsForm {

		public function init() {
			/** @var modelBetit $model */
			$model = cmsCore::getModel("betit");
			$sports = $model->getSports();
			foreach ($sports as $key => &$sport) {
				$sport = $sport['title'];
			}
			$sports = array_merge([0 => ""], $sports);


			$bettings = $model->getBettings();
			foreach ($bettings as $key => &$betting) {
				$betting = $betting['title'];
			}
			$bettings = array_merge([0 => ""], $bettings);

			$bookmakers = $model->getBookmakers();
			foreach ($bookmakers as $key => &$bookmaker) {
				$bookmaker = $bookmaker['title'];
			}
			$bookmakers = array_merge([0 => ""], $bookmakers);

			return [

				'league' => [
					'type' => 'fieldset',
					'title' => "Лига",
					'childs' => [
						new fieldList('sport_id', [
							'title' => "Вид спорта",
							'items' => $sports,
							'rules' => [
								['required']
							]
						]),

						new fieldList('league_id', [
							'title' => "Лига",
							'parent' => [
								'list' => 'sport_id',
								'url' => href_to('betit', 'list_leagues_ajax')
							],
							'items' => ['0' => 'Сначала выберите вид спорта']
						]),
						new fieldString('league_name', [
							'title' => "Название лиги",
							'hint' => "Если нет в списке",
						]),
						new fieldString('league_country', [
							'title' => "Страна лиги",
							'hint' => "Если нет в списке"
						])
					]
				],
				'event' => [
					'type' => 'fieldset',
					'title' => "Событие",
					'childs' => [
						new fieldDate('date_event', [
							"show_time" => true,
							'title' => "Начало события",
							'rules' => [
								['required']
							]
						]),
						new fieldString('title', [
							'title' => "Название события",
							'rules' => [
								['required']
							]
						]),
						new fieldHidden('event_id'),
					]
				],
				'odd' => [
					'type' => 'fieldset',
					'title' => "Прогноз",
					'childs' => [
						new fieldList('betting_id', [
							'title' => "Вид прогноза",
							'items' => $bettings,
							'rules' => [
								['required']
							]
						]),
						new fieldList('period_id', [
							'title' => "Период",
							'parent' => [
								'list' => 'sport_id',
								'url' => href_to('betit', 'list_periods_ajax')
							],
							'items' => ['0' => 'Сначала выберите вид спорта'],
							'rules' => [
								['required']
							]
						]),
						new fieldNumber('handicap', [
							'title' => "Фора"
						]),
						new fieldList('bookmaker_id', [
							'title' => "Букмекер",
							'items' => $bookmakers,
							'rules' => [
								['required']
							]
						]),
						new fieldList('column_id', [
							'title' => "Вид",
							'parent' => [
								'list' => 'betting_id',
								'url' => href_to('betit', 'list_columns_ajax')
							],
							'items' => ['0' => 'Сначала выберите вид прогноза'],
							'rules' => [
								['required']
							]
						]),
						new fieldNumber('odd', [
							'title' => "Коэффициент",
							'hint' => "Десятичный коэффициент на выбранный исход",
							'rules' => [
								['required']
							]
						]),
						new fieldList('is_paid', [
							'title' => "Тип продажи",
							'items' => [
								0 => "Бесплатный прогноз",
								1 => "Платный прогноз"
							],
							'rules' => [
								['required']
							]
						]),
						new fieldNumber('percent', [
							'title' => "Размер ставки",
							'units' => "%",
							'hint' => "Размер ставки в процентах от банкролла",
							'rules' => [
								['required']
							]
						]),
						new fieldCheckbox('is_express', [
							'title' => "Добавить прогноз в купон экспрессов"
						]),
						new fieldText('description', [
							'title' => "Комментарий / Обоснование"
						])
					],

				],

			];

		}

	}
