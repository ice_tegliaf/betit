<?php

	class actionBetitAddEvent extends cmsAction {

		public function run() {
			if (cmsUser::getInstance()->id == 0) {
				$this->redirectHome();
			}
			$form = $this->getForm('add_event');
			$data = [];
			if ($this->request->has('submit')) {
				$data = $form->parse($this->request, true);
				$errors = $form->validate($this, $data);
				if (!$errors) {
					if ($data['league_name'] != '' && $data['league_country'] != '') {
						$data["league_id"] = $this->model->addLeague([
							                                             "sport_id" => $data['sport_id'],
							                                             "user_id" => cmsUser::getInstance()->id,
							                                             "country_name" => $data['league_country'],
							                                             "title" => $data['league_name'],
						                                             ]);
						if (!$data["league_id"]) {
							$errors["league_name"] = "Что-то пошло не так с добавлением новой лиги";
						}
					}
				}
				if (!$errors) {
					if (!$data['event_id']) {
						$data["event_id"] = $this->model->addItem([
							                                          "league_id" => $data["league_id"],
							                                          "user_id" => cmsUser::getInstance()->id,
							                                          "title" => $data['title'],
							                                          "date_event" => strtotime($data['date_event']),
							                                          "periods" => json_encode([])
						                                          ]);
						if (!$data["event_id"]) {
							$errors["title"] = "Что-то пошло не так с добавлением нового события";
						}
					}
				}
				if (!$errors) {
					$event = $this->model->getItemById("betit_events", $data["event_id"]);
					$periods = json_decode($event['periods'], true);
					$periods[$data['betting_id']][$data['period_id']][$data['bookmaker_id']][$data['column_id']] = $data['odd'];
					$this->model->update("betit_events", $data["event_id"], ['periods' => json_encode($periods)]);
					$this->model->addForecast([
						                          "event_id" => $data['event_id'],
						                          "type" => "self",
						                          "betting_id" => $data['betting_id'],
						                          "period_id" => $data['period_id'],
						                          "bookmaker_id" => $data['bookmaker_id'],
						                          "is_express" => $data['is_express'],
						                          "column_id" => $data['column_id'],
						                          "handicap" => $data['handicap'],
						                          "odd" => $data['odd'],
						                          "percent" => $data['percent'],
						                          "is_paid" => $data['is_paid'],
						                          "description" => $data['description'],
					                          ]);
				}
				$this->redirectToAction("subscriptions");
			}
			return $this->cms_template->render('add_event', [
				'data' => $data,
				'form' => $form,
				'errors' => isset($errors) ? $errors : false
			]);
		}
	}