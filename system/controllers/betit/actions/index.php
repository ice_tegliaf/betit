<?php

	class actionBetitIndex extends cmsAction {
		public function run() {
			$ret = null;
			if ($this->request->get("search")) {
				$events = $this->model->filterLike("title", "%" . $this->request->get("search") . "%")->orderBy("id", "DESC")->get("betit_events");
				$ret = $events;
			}
			$this->cms_template->render("index", ['items' => $ret]);
		}
	}