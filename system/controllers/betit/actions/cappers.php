<?php

	class actionBetitCappers extends cmsAction {


		public function run() {
			$cappers = $this->model->getCappers();

			$this->cms_template->render("cappers", [
				"cappers" => $cappers
			]);
		}
	}