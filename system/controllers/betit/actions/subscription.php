<?php

	class actionBetitSubscription extends cmsAction {

		public function filterIt() {
			if (!$this->request->has("filter"))
				return null;
			$what = null;
			$param = $this->request->get("param");
			switch ($this->request->get("type")) {
				case "sports":
					$this->model->filterEqual("bl.sport_id", $param);
					break;
				case "input":
					$what = $param === "self";
					break;
				case "bookmakers":
					$this->model->filterEqual("bookmaker_id", $param);
					break;
				case "bets":
					$this->model->filterEqual("betting_id", $param);
					break;
				case "time":
					$this->model->filterBetween("e.date_event", strtotime($param[0]), strtotime($param[1]));
					break;
			}
			return $what;
		}

		public function run($id, $tab_name = false) {

			if ($id == "add") {
				$this->actionAdd();
				return;
			}
			$subscription = $this->model->getSubscription($id);
			if (!$subscription) cmsCore::error404();

			if ($tab_name == "forecasts") {
				$this->actionForecasts($subscription);
				return;
			} elseif ($tab_name == "statistics") {
				$this->actionStatistics($subscription);
				return;
			} elseif ($tab_name == "settings") {
				if ($subscription['user_id'] != $this->cms_user->id) {
					cmsCore::errorForbidden();
				}
				$this->actionSettings($subscription);
				return;
			} elseif ($tab_name == "verify_self") {
				if ($subscription['user_id'] != $this->cms_user->id) {
					cmsCore::errorForbidden();
				}
				$this->actionVerifySelf($subscription);
				return;
			} elseif ($tab_name == "express") {
				if ($subscription['user_id'] != $this->cms_user->id) {
					cmsCore::errorForbidden();
				}
				$this->actionExpress($subscription, $this->request->get("express_id"));
			}
			$is_subscribed = $this->model->isUserSubscribed($id, cmsUser::getInstance()->id);
			$expresses = [];
			if ($subscription['user_id'] == $this->cms_user->id)
				$expresses = $this->model->getSubscriptionExpresses($id);
			$this->cms_template->render("subscription", [
				"subscription" => $subscription,
				"is_subscribed" => $is_subscribed,
				"user" => $this->cms_user,
				"expresses" => $expresses
			]);
		}

		private function actionAdd() {
			$form = $this->getForm('subscription');
			$data = [];
			if ($this->request->has('submit')) {
				$data = $form->parse($this->request, true);
				$data['user_id'] = cmsUser::getInstance()->id;
				$errors = $form->validate($this, $data);
				if (!$errors) {
					$id = $this->model->addSubscription($data);
					if ($id != 0)
						$this->redirectToAction("subscription", $id);
					else
						cmsUser::addSessionMessage("Не удалось создать подписку", "error");
				}
			}

			$this->cms_template->render("subscription_add", [
				'data' => $data,
				'form' => $form,
				'errors' => isset($errors) ? $errors : false
			]);
		}

		private function actionSettings($subscription) {
			$form = $this->getForm('subscription');
			$data = $subscription;
			if ($this->request->has("status_set")) {
				$data = ['status' => $this->request->get('status')];
				if ($data['status'] == 1) $data['status_pause'] = strtotime($this->request->get('status_pause'));
				$ret = $this->model->update("betit_subscriptions", $subscription['id'], $data);
				if ($ret != false)
					$this->redirectToAction("subscription", $subscription['id']);
				else
					cmsUser::addSessionMessage("Не удалось изменить подписку", "error");
			}
			if ($this->request->has('submit')) {
				$data = $form->parse($this->request, true);
				$errors = $form->validate($this, $data);
				if (!$errors) {
					$ret = $this->model->updateSubscription($subscription['id'], $data);
					if ($ret != false)
						$this->redirectToAction("subscription", $subscription['id']);
					else
						cmsUser::addSessionMessage("Не удалось изменить подписку", "error");
				}
			}

			$this->cms_template->render("subscription_settings", [
				'data' => $data,
				'form' => $form,
				'errors' => isset($errors) ? $errors : false
			]);
		}

		private function actionForecasts($subscription) {

			cmsCore::loadLib("betit_statistics.class", "betit_statistics");
			$what = $this->filterIt();
			$userForecasts = $this->model
				->filterEqual("i.express_id", 0)->getSubscriptionForecasts($subscription['id'], $what);
			$statistics = new Statistics();
			$expresses = $this->model->getSubscriptionExpresses($subscription['id']);
			list($statistics, $events) = $statistics->generateStatistics($userForecasts, $expresses, $subscription["start_bankroll"]);

			$is_subscribed = $this->model->isUserSubscribed($subscription['id'], cmsUser::getInstance()->id);
			$this->cms_template->render('subscription_forecasts', [
				'events' => $events,
				"subscription" => $subscription,
				"is_subscribed" => $is_subscribed,
				"user" => $this->cms_user
			]);
		}

		private function actionStatistics($subscription) {

			cmsCore::loadLib("betit_statistics.class", "betit_statistics");
			$what = $this->filterIt();
			$userForecasts = $this->model
				->filterEqual("i.express_id", 0)->getSubscriptionForecasts($subscription['id'], $what);
			$statistics = new Statistics();
			$expresses = $this->model->getSubscriptionExpresses($subscription['id']);
			list($statistics, $events) = $statistics->generateStatistics($userForecasts, $expresses, $subscription["start_bankroll"]);

			$is_subscribed = $this->model->isUserSubscribed($subscription['id'], cmsUser::getInstance()->id);
			$this->cms_template->render('subscription_statistics', [
				'statistics' => $statistics,
				"subscription" => $subscription,
				"is_subscribed" => $is_subscribed,
				"user" => $this->cms_user
			]);
		}

		private function actionVerifySelf($subscription) {
			$forecasts = $this->model->filterEqual("e.is_finished", "0")
			                         ->filterEqual("i.express_id", 0)
			                         ->filterLt("i.date_event", time() - 32 * 3600) //только события, с начала которых прошло 32 часов (точно завершенные)
			                         ->getSubscriptionForecasts($subscription['id'], true);

			if ($this->request->has('submit')) {
				$events = $this->request->get("events");
				foreach ($events as $f_id => $event) {
					if ($event['checked'] == "on") {
						unset($event['checked']);
						$event['final_result'] = json_encode(explode(":", $event['final_result']));
						foreach ($event['result'] as &$res) {
							$res = explode(":", $res);
						}
						$event['is_finished'] = 1;
						$event['result'] = json_encode($event['result']);
						$this->model->update("betit_events", $f_id, $event);
					}
				}
				$this->redirectToAction("subscription", [$subscription['id'], "forecasts"]);
			}
			$this->cms_template->render('subscription_verify_self', [
				"subscription" => $subscription,
				"forecasts" => $forecasts,
				"user" => $this->cms_user
			]);
		}

		private function actionExpress($subscription, $express_id) {
			if ($this->request->has('submit-open')) {
				$this->model->update("betit_expresses", $express_id, [
					"percent" => $this->request->get("percent"),
					"status" => 1,
					"open_itme" => time()
				]);
				$this->redirectToAction("subscription", $subscription['id']);
			}
		}
	}