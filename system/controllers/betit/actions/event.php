<?php

	class actionBetitEvent extends cmsAction {

		public function run($id = -1) {
			if ($id == -1)
				$this->controller->redirectToAction("index");
			$event = $this->model->getEvent($id);
			$pre_tabs = $this->model->getEventTabs($id);
			$tabs = [];
			if (is_array($pre_tabs))
				foreach ($pre_tabs as $tab) {
					$tabs[] = [$tab["betting_id"], $tab["scope_id"]];
				}
			$this->cms_template->render("event", [
				"event" => $event,
				"tabs" => $tabs
			]);
		}
	}