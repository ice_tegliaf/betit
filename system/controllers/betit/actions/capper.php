<?php

	class actionBetitCapper extends cmsAction {


		public function run($id) {
			$capper = $this->model->getCapper($id);

			cmsCore::loadLib("betit_statistics.class", "betit_statistics");
			$subscriptions = $this->model->getSubscriptions($id);
			$statistics = new Statistics();
			foreach ($subscriptions as &$subscription) {
				$userForecasts = $this->model
					->filterEqual("i.express_id", 0)->getSubscriptionForecasts($subscription['id'], null);
				$expresses = $this->model->getSubscriptionExpresses($subscription['id']);
				list($subscription['statistics'], $events) = $statistics->generateStatistics($userForecasts, $expresses,$subscription["start_bankroll"]);
			}
			$this->cms_template->render("capper", [
				"capper" => $capper,
				"subscriptions" => $subscriptions
			]);
		}
	}