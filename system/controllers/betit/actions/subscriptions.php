<?php

	class actionBetitSubscriptions extends cmsAction {


		public function run() {
			$subscriptions = $this->model->getSubscriptions();

			$this->cms_template->render("subscriptions", [
				"subscriptions" => $subscriptions
			]);
		}
	}