<?php

	class actionBetitAjax extends cmsAction {
		public function run($action, $id = -1) {
			if (!$this->request->isAjax() && !cmsUser::getInstance()->is_admin)
				$this->controller->redirectToAction("index");
			switch ($action) {
				case "add_forecast":
					$forecast = json_decode($this->request->get("forecast"), true);
					$ret = $this->model->addForecast($forecast);
					$this->cms_template->renderJSON($ret != 0, true);
					break;
			}
			exit;
		}

	}