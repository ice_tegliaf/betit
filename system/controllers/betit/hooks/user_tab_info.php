<?php

	class onBetitUserTabInfo extends cmsAction {

		public function run($profile, $tab_name) {
			if ($profile['id'] != $this->cms_user->id) {
				//return false;
			}
			$model = cmsCore::getModel("betit");

			if ($tab_name == "capper" && $model->isCapper($profile['id'])) {
				return true;
			} elseif ($tab_name == "subscription")
				if (($subscr = $model->getActiveSubscription($profile['id'])) !== false) {
					return ['title' => "Рассылка"];
				} elseif ($profile['id'] == $this->cms_user->id) {
					return ['title' => "Открыть рассылку"];
				}
			return false;
		}

	}
