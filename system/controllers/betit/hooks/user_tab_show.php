<?php

	class onBetitUserTabShow extends cmsAction {

		public function run($profile, $tab_name, $tab) {
			$model = cmsCore::getModel("betit");
			if ($tab_name == "capper" && $model->isCapper($profile['id'])) {
				$this->redirectTo("betit", "capper", [$profile['id']]);
				return true;
			} elseif ($tab_name == "subscription")
				if (($subscr = $model->getActiveSubscription($profile['id'])) !== false) {
					$this->redirectTo("betit", "subscription", [$subscr['id']]);
					return true;
				} elseif ($profile['id'] == $this->cms_user->id) {
					$this->redirectTo("betit", "subscription", ["add"]);
					return true;
				}
			return false;
		}

	}

