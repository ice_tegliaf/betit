<?php

	class betit extends cmsFrontend {


		public function actionFilterStatisticsField() {
			switch ($this->request->get("value")) {
				case "sports":
					$items = $this->model->filterEqual("is_enabled", 1)->get("betit_sports_dict", null, "odsp_id");
					$items = array_collection_to_list($items, "odsp_id", "title_ru");
					?>
                    <div class="input-field">
						<?= html_select("param", $items, "") ?>
                        <label>Вид спорта</label>
                    </div>
					<?php
					break;
				case "input":
					?>
                    <div class="input-field">
                        <select name="param">
                            <option value="100">Со 100% верификацией</option>
                            <option value="self">Ручной ввод</option>
                        </select>
                        <label>Тип ввода</label>
                    </div>
					<?php
					break;
				case "bookmakers":
					$items = $this->model->filterEqual("is_enabled", 1)->get("betit_odsp_bookmakers");
					$items = array_collection_to_list($items, "id", "title");
					?>
                    <div class="input-field">
						<?= html_select("param", $items, "") ?>
                        <label>Букмекер</label>
                    </div>
					<?php
					break;
				case "bets":
					$items = $this->model->filterEqual("is_enabled", 1)->get("betit_odsp_bettings");
					$items = array_collection_to_list($items, "id", "title");
					?>
                    <div class="input-field">
						<?= html_select("param", $items, "") ?>
                        <label>Тип ставки</label>
                    </div>
					<?php
					break;
				case "time":
					?>
                    <div class="col s6 input-field"><input type="text" name="param[0]" class="datepicker"><label>Временной промежуток</label></div>
                    <div class="col s6 input-field"><input type="text" name="param[1]" class="datepicker"><label>Временной промежуток</label></div>
					<?php
					break;
			}
			$this->halt();
		}

		public function actionSubscribe() {
			cmsTemplate::getInstance()->renderJSON($this->model->toggleSubscribe($this->request->get("value"), cmsUser::getInstance()->id));
		}

		public function actionListLeaguesAjax() {
			$leagues = $this->model
				->filterEqual("sport_id", $this->request->get("value"))
				->getLeagues();
			$leagues = [0 => "-- Своя лига --"] + array_collection_to_list($leagues, "id", "title");
			cmsTemplate::getInstance()->renderJSON($leagues);
		}

		public function actionListPeriodsAjax() {
			$scopes = $this->model
				->getPeriods($this->request->get("value"));
			$scopes = [0 => ""] + array_collection_to_list($scopes, "id", "title");
			cmsTemplate::getInstance()->renderJSON($scopes);
		}

		public function actionListColumnsAjax() {
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$columns = Parser::getColumnNames($this->request->get("value"));
			cmsTemplate::getInstance()->renderJSON($columns);
		}

		public function actionListEventsAjax() {
			$events = $this->model
				->filterLike("title", "%" . $this->request->get("value") . "%")
				->getEvents();
			foreach ($events as &$event) {
				$event = ["title" => $event['title'], "value" => $event['id']];
			}
			$events = array_merge([0 => ""], $events);
			cmsTemplate::getInstance()->renderJSON($events);
		}
	}
