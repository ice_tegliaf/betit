<?php

	class actionBetitBettings extends cmsAction {

		public function run() {

			$grid = $this->loadDataGrid('bettings');

			return cmsTemplate::getInstance()->render('backend/bettings', [
				'grid' => $grid
			]);
		}

	}
