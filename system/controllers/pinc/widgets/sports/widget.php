<?php

	class widgetBetitSports extends cmsWidget {

		public function run() {

			$betit = cmsCore::getController('betit');

			$sports = $betit->model->getSports();
			$leagues = $betit->model->getLeagues(true);
			if (!$leagues) {
				return false;
			}

			return [
				"sports" => $sports,
				"countries" => $leagues
			];

		}

	}
