<?php

	class widgetBetitNearest extends cmsWidget {

		public function run() {

			$betit = cmsCore::getController('betit');

			$events = $betit->model
				->limit($this->getOption('limit', 10))
				->orderBy("date_event", "ASC")
				->filterGtEqual("date_event", time())
				->getEvents();
			if (!$events) {
				return false;
			}

			return [
				"events" => $events
			];

		}

	}
