<?php

	class backendBetit extends cmsBackend {

		protected $useOptions = true;
		public $useDefaultOptionsAction = true;

		public function actionIndex() {
			$this->redirectToAction('options');
		}

		public function getBackendMenu() {
			return [
				[
					'title' => "Настройки",
					'url' => href_to($this->root_url, 'options')
				],
				[
					'title' => "Виды прогнозов",
					'url' => href_to($this->root_url, 'bettings')
				],
			];
		}

		public function actionBettingsAjax() {
			if (!$this->request->isAjax()) {
				cmsCore::error404();
			}
			$grid = $this->loadDataGrid('bettings');
			$model = cmsCore::getModel($this->name);
			$model->setPerPage(admin::perpage);
			$filter = [];
			$filter_str = $this->request->get('filter', '');
			$filter_str = cmsUser::getUPSActual('betit.bettings_list', $filter_str);
			if ($filter_str) {
				parse_str($filter_str, $filter);
				$model->applyGridFilter($grid, $filter);
			}
			$total = $model->getBettingsCount();
			$perpage = isset($filter['perpage']) ? $filter['perpage'] : admin::perpage;
			$pages = ceil($total / $perpage);
			$categories = $model->getBettings();
			cmsTemplate::getInstance()->renderGridRowsJSON($grid, $categories, $total, $pages);
			$this->halt();
		}

		public function actionReorder($what) {
			$items = $this->request->get('items');
			if (!$items) {
				cmsCore::error404();
			}
			$model = cmsCore::getModel($this->name);
			switch ($what) {
				case 'bettings':
					$model->reorderBettings($items);
					break;
				case 'bookmakers':
					$model->reorderBookmakers($items);
					break;
			}
			$this->redirectBack();
		}

		public function actionDelete($what, $id) {
			$model = cmsCore::getModel($this->name);
			switch ($what) {
				case 'category':
					$model->deleteEshopCategory($id);
					break;
				case 'bookmaker':
					$model->deleteBookmaker($id);
					break;
			}
			$this->redirectBack();
		}

	}
