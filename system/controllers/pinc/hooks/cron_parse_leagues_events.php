<?php

	set_time_limit(0);

	class onPincCronParseLeaguesEvents extends cmsAction {

		public function run() {
			cmsCore::loadLib("betit_pinc_parser.class", "betit_pinnacle");
			$pinnacle = new Pinnacle();
			$sports = $pinnacle->getSports();
			if (!$sports)
				return true;

			foreach ($sports as $sport) {
				if ($sport['eventCount'] == 0) {
					$this->model->filterEqual("sport_id", $sport['id'])->updateFiltered("betit_pinc_leagues", [
						'is_enabled' => '0'
					]);
					continue;
				}
				$this->model->filterEqual("sport_id", $sport['id'])->updateFiltered("betit_pinc_leagues", [
					'is_enabled' => '1'
				]);
				$leagues = $pinnacle->getLeagues($sport['id']);
				if (!$leagues)
					continue;
				foreach ($leagues as $league) {
					$this->model->insertOrUpdate("betit_pinc_leagues",
					                             [
						                             "id" => $league['id'],
						                             "sport_id" => $sport['id'],
						                             "country_name" => $league['country'],
						                             "title" => $league['name'],
						                             "count" => $league['eventCount'],
						                             "is_enabled" => 1,
					                             ],
					                             [
						                             "is_enabled" => 1,
						                             "count" => $league['eventCount']
					                             ]);
				}
			}
			$sports = $this->model->getSports();
			foreach ($sports as $sport) {
				if ($sport['last_update'] != null && time() - $sport['last_update'] + 1 < $sport['update_interval'])
					continue;
				echo PHP_EOL . $sport['id'] . $sport['title'] . PHP_EOL;
				$this->parseSport($sport['id']);
				$this->cms_database->update("betit_pinc_sports", "id=" . $sport['id'], ['last_update' => time()]);
			}
			return true;
		}

		private function parseSport($id) {
			$leagues = $this->model->filterEqual("sport_id", $id)->getLeagues();
			if (!$leagues)
				return;

			foreach ($leagues as $league) {
				$pinnacle = new Pinnacle();
				$events = $pinnacle->getOdds($league['sport_id'], $league['id']);

				echo "(" . $league["id"] . "," . count($events) . ")\t";
				if (!$events || count($events) == 0) {
					$this->cms_database->update("betit_pinc_leagues", "id=" . $league['id'], ['is_enabled' => 0]);
					return;
				}

				$events_ids = array_values(array_collection_to_list($events, "id"));
				$events_data = $pinnacle->getEvent($league['sport_id'], $league['id'], $events_ids);
				foreach ($events as $event) {
					foreach ($event["periods"] as &$period) {
						$period["spreads"] = json_encode($period["spreads"]);
						$period["totals"] = json_encode($period["totals"]);
						$period["teamTotal"] = json_encode($period["teamTotal"]);
					}
					$this->model->insertOrUpdate("betit_pinc_events",
					                             $events_data[$event['id']] + [
						                             "result" => json_encode([$event['homeScore'], $event['awayScore']]),
						                             "periods" => json_encode($event["periods"])
					                             ],
					                             [
						                             "result" => json_encode([$event['homeScore'], $event['awayScore']]),
						                             "periods" => json_encode($event["periods"]),
						                             "is_finished" => $events_data[$event['id']]['is_finished']
					                             ]);
				}
			}

		}
	}