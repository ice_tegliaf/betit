<?php

	class modelPinc extends cmsModel {
		public function getBettingsCount() {
			return $this->useCache("betit.bettings")->getCount("betit_bettings");
		}

		public function getBettings() {
			return $this->useCache("betit.bettings")->get("betit_bettings");
		}

		public function reorderBettings($ids_list) {
			(new cmsCache())->clean("betit.bettings");
			$this->reorderByList('betit_bettings', $ids_list);
			return true;
		}

		public function getPeriods($betting_id = 0) {
			if ($betting_id == 0)
				return $this->get("betit_periods");
			else
				return $this->joinRight("betit_periods", "p", "p.id=i.period_id")
				            ->select("p.id")
				            ->select("p.title")
				            ->filterEqual("i.betting_id", $betting_id)
				            ->get("betit_bettings_periods");
		}

		public function getEvent($id) {
			return $this
				->joinLeft("betit_pinc_leagues", "bl", "bl.id=i.league_id")
				->select("bl.title", "league_title")
				->select("bl.country_name", "league_country")
				->select("bl.sport_id", "sport_id")
				->joinRight("betit_pinc_sports", "bs", "bs.id=bl.sport_id")
				->select("bs.title", "sport_title")
				->select("bs.title_ru", "sport_title_ru")
				->useCache("betit.event")
				->getItemById("betit_pinc_events", $id, function ($item) {
					$item['result'] = json_decode($item['result'], true);
					$item['periods'] = json_decode($item['periods'], true);
					return $item;
				});
		}

		public function getLeagues($group_by_sport_and_country = false, $with_hidden = false) {
			if ($with_hidden)
				$items = $this->get("betit_pinc_leagues");
			else
				$items = $this
					->filterEqual("is_enabled", 1)
					->filterNotEqual("count", 0)
					->get("betit_pinc_leagues");
			if (!$group_by_sport_and_country)
				return $items;
			$items_grouped = [];
			foreach ($items as $item) {
				$items_grouped[$item["sport_id"]][$item["country_name"] ? $item["country_name"] : "Без страны"][] = $item;
			}
			return $items_grouped;
		}

		public function getSports($is_hidden = false) {
			if (!$is_hidden)
				$this
					->filterEqual("is_enabled", 1)
					->filter("EXISTS (SELECT 1 FROM {#}betit_pinc_leagues bpl WHERE bpl.sport_id = i.id AND bpl.is_enabled=1 AND bpl.count != 0)");
			return $this->get("betit_pinc_sports");
		}

		public function getLeague($league_id) {
			return $this
				->joinRight("betit_pinc_sports", "bs", "bs.id=i.sport_id")
				->select("bs.title", "sport_title")
				->select("bs.title_ru", "sport_title_ru")
				->getItemById("betit_pinc_leagues", $league_id);
		}

		public function getEvents() {
			return $this->useCache("betit.events")->get("betit_events", function ($item, $model) {
				$item['league'] = $model->getLeague($item['league_id']);
				unset($item['url']);
				return $item;
			});
		}

		public function getLeagueEvents($league_id) {
			return $this->filterEqual("league_id", $league_id)->get("betit_pinc_events", function ($item) {
				$item['periods'] = json_decode($item['periods'], true);
				return $item;
			});
		}

	}
