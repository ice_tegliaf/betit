<?php

	class actionPincEvent extends cmsAction {

		public function run($id = -1) {
			if ($id == -1)
				$this->controller->redirectToAction("index");
			$event = $this->model->getEvent($id);
			$league = $this->model->getLeague($event['league_id']);
			$this->cms_template->render("event", [
				"event" => $event,
				"league" => $league
			]);
		}
	}