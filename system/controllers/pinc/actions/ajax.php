<?php
	//1x2 ou hdp

	class actionPincAjax extends cmsAction {
		public function run($action, $id = -1) {
			if (!$this->request->isAjax() && !cmsUser::getInstance()->is_admin)
				$this->controller->redirectToAction("index");
			switch ($action) {
				case "sports_menu":
					$this->cms_template->renderJSON($this->model->getSports(), true);
					break;
				case "leagues_menu":
					$this->cms_template->renderJSON($this->model->getLeagues(true), true);
					break;
				case "leagues":
					$this->getLeagues();
					break;
				case "league_events":
					echo $this->getLeagueEvents($id);
					break;
			}
			exit;
		}

		private function getLeagues() {
			cmsCore::loadLib("betit_pinc_parser.class", "betit_pinnacle");
			$pinnacle = new Pinnacle();
			$sports = $pinnacle->getSports();
			if ($sports)
				foreach ($sports as $sport) {
					if ($sport['eventCount'] == 0) {
						$this->model->filterEqual("sport_id", $sport['id'])->updateFiltered("betit_pinc_leagues", [
							'is_enabled' => '0'
						]);
						continue;
					}
					$this->model->filterEqual("sport_id", $sport['id'])->updateFiltered("betit_pinc_leagues", [
						'is_enabled' => '1'
					]);
					$leagues = $pinnacle->getLeagues($sport['id']);
					if ($leagues)
						foreach ($leagues as $league) {
							$this->model->insertOrUpdate("betit_pinc_leagues",
							                             [
								                             "id" => $league['id'],
								                             "sport_id" => $sport['id'],
								                             "country_name" => $league['country'],
								                             "title" => $league['name'],
								                             "count" => $league['eventCount'],
								                             "is_enabled" => 1,
							                             ],
							                             [
								                             "is_enabled" => 1,
								                             "count" => $league['eventCount']
							                             ]);
						}
				}
		}

		private function getLeagueEvents($league_id) {
			$league = $this->model->getItemById("betit_pinc_leagues", $league_id);
			cmsCore::loadLib("betit_pinc_parser.class", "betit_pinnacle");
			$pinnacle = new Pinnacle();
			$events = $pinnacle->getOdds($league['sport_id'], $league_id);
			$events_ids = array_values(array_collection_to_list($events, "id"));
			$events_data = $pinnacle->getEvent($league['sport_id'], $league_id, $events_ids);
			foreach ($events as $event) {
				foreach ($event["periods"] as &$period) {
					$period["spreads"] = json_encode($period["spreads"]);
					$period["totals"] = json_encode($period["totals"]);
					$period["teamTotal"] = json_encode($period["teamTotal"]);
				}
				$this->model->insertOrUpdate("betit_pinc_events",
				                             $events_data[$event['id']] + [
					                             "result" => json_encode([$event['homeScore'], $event['awayScore']]),
					                             "periods" => json_encode($event["periods"])
				                             ],
				                             [
					                             "result" => json_encode([$event['homeScore'], $event['awayScore']]),
					                             "periods" => json_encode($event["periods"])
				                             ]);
			}
			return count($events);
		}
	}