<?php

	class actionBetitAddEvent extends cmsAction {

		public function run() {
			$form = $this->getForm('add_event');
			$data = [];
			if ($this->request->has('submit')) {
				$data = $form->parse($this->request, true);
				$errors = $form->validate($this, $data);
				if (!$errors) {
					$calc = $this->model->requestOrderCreate($data);
				}
			}
			return $this->cms_template->render('add_event', [
				'data' => $data,
				'form' => $form,
				'errors' => isset($errors) ? $errors : false
			]);
		}
	}