<?php

	class actionPincLeague extends cmsAction {

		public function run($id = -1) {
			if ($id == -1)
				$this->controller->redirectToAction("index");
			$league = $this->model->getLeague($id);
			$events = $this->model->getLeagueEvents($id);
			$this->cms_template->render("league", [
				"league" => $league,
				"events" => $events
			]);
		}
	}