<?php

	class actionBetitParse extends cmsAction {
		public $lock_explicit_call = true;
		/** @var cmsCacheFiles */
		private $cache;

		public function run($type, $id = -1) {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_parser.class", "betit_parser");
			$this->cache = new cmsCacheFiles(cmsConfig::getInstance());
			ob_implicit_flush(true);
			$ret = [];
			switch ($type) {
				case "bookmakers":
					$ret = $this->parseBookmakers();
					break;
				case "leagues":
					$ret = $this->parseLeagues();
					break;
				case "league":
					$ret = $this->parseLeagueEvents($id);
					break;
				case "event_result":
					$ret = $this->parseEventResult($id);
					break;
			}
			$this->cms_template->renderJSON($ret, true);
		}

		private function parseLeagueEvents($league_id) {
			$parser = new Parser();
			$league = $this->cms_database->getRow("betit_leagues", "id=" . $league_id);
			$events = $parser->getLeagueEvents($league['url']);
			if (!$events || count($events) == 0) {
				$this->cms_database->update("betit_leagues", "id=" . $league_id, ['is_hidden' => 1]);
				cmsUser::addSessionMessage("В Лиге \"" . $league['title'] . "\" пока нет матчей", "error");
				return 0;
			}
			foreach ($events as &$event) {
				$this->cms_database
					->insertOrUpdate("betit_events",
					                 [
						                 'url' => $event['url'],
						                 'sport_id' => $league["sport_id"],
						                 'league_id' => $league["id"],
						                 'date_event' => $event["date_event"],
						                 'title' => $event["title"]],
					                 [
						                 'date_event' => $event["date_event"],
						                 'title' => $event["title"]
					                 ]);
				unset($event['url']);
			}
			return $events;
		}



		private function parseLeagues() {
			$parser = new Parser();
			$leagues = $parser->getLeagues();
			cmsCache::getInstance()->clean('betit.leagues');
			$sports = $this->model->getSports("name", true);
			$this->model->filterGtEqual("id", "1")->updateFiltered("betit_leagues", [
				"is_hidden" => 1
			]);
			foreach ($leagues as $league) {
				$league['sport_id'] = $sports[$league['sport_id']]["id"];
				$this->model->insertOrUpdate("betit_leagues", $league, [
					"is_hidden" => 0
				]);
			}
			return count($leagues);
		}

		private function parseEventResult($id) {
			$parser = new Parser();
			$item = $this->model->getEvent($id);
			$result = $parser->getEventResult($item['url']);
			if ($result['result'] == null) {
				unset($result['result']);
			} else {
				$result['result'] = json_encode($result['result']);
			}
			if ($result['final_result'] == null) {
				unset($result['final_result']);
			} else {
				$result['final_result'] = json_encode($result['final_result']);
			}
			$this->model->update("betit_events", $item['id'], $result);
			return $result;
		}
	}