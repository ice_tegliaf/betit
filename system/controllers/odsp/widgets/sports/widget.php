<?php

	class widgetOdspSports extends cmsWidget {

		public function run() {

			$betit = cmsCore::getController('odsp');

			$sports = $betit->model->getSports();
			$leagues = $betit->model->getLeagues(true);
			if (!$leagues) {
				return false;
			}

			return [
				"sports" => $sports,
				"countries" => $leagues
			];

		}

	}
