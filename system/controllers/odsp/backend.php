<?php

	class backendOdsp extends cmsBackend {

		protected $useOptions = true;
		public $useDefaultOptionsAction = true;

		public function actionIndex() {
			$this->redirectToAction('options');
		}

		public function getBackendMenu() {
			return [
				[
					'title' => "Настройки",
					'url' => href_to($this->root_url, 'options')
				],
				[
					'title' => "Виды спорта",
					'url' => href_to($this->root_url, 'sports')
				],
				[
					'title' => "Виды прогнозов",
					'url' => href_to($this->root_url, 'bettings')
				],
				[
					'title' => "Виды периодов",
					'url' => href_to($this->root_url, 'periods')
				],
				[
					'title' => "Букмекеры",
					'url' => href_to($this->root_url, 'bookmakers')
				],
			];
		}

		public function actionSportsAjax() {
			if (!$this->request->isAjax()) {
				cmsCore::error404();
			}
			$grid = $this->loadDataGrid('sports');
			$model = cmsCore::getModel($this->name);
			$model->setPerPage(admin::perpage);
			$filter = [];
			$filter_str = $this->request->get('filter', '');
			$filter_str = cmsUser::getUPSActual('odsp.sports_list', $filter_str);
			if ($filter_str) {
				parse_str($filter_str, $filter);
				$model->applyGridFilter($grid, $filter);
			}
			$total = $model->getSportsCount("id", true);
			$perpage = isset($filter['perpage']) ? $filter['perpage'] : admin::perpage;
			$pages = ceil($total / $perpage);
			$categories = $model->getSports("id", true);
			cmsTemplate::getInstance()->renderGridRowsJSON($grid, $categories, $total, $pages);
			$this->halt();
		}

		public function actionBettingsAjax() {
			if (!$this->request->isAjax()) {
				cmsCore::error404();
			}
			$grid = $this->loadDataGrid('bettings');
			$model = cmsCore::getModel($this->name);
			$model->setPerPage(admin::perpage);
			$filter = [];
			$filter_str = $this->request->get('filter', '');
			$filter_str = cmsUser::getUPSActual('odsp.bettings_list', $filter_str);
			if ($filter_str) {
				parse_str($filter_str, $filter);
				$model->applyGridFilter($grid, $filter);
			}
			$total = $model->getBettingsCount();
			$perpage = isset($filter['perpage']) ? $filter['perpage'] : admin::perpage;
			$pages = ceil($total / $perpage);
			$categories = $model->getBettings();
			cmsTemplate::getInstance()->renderGridRowsJSON($grid, $categories, $total, $pages);
			$this->halt();
		}

		public function actionPeriodsAjax() {
			if (!$this->request->isAjax()) {
				cmsCore::error404();
			}
			$grid = $this->loadDataGrid('periods');
			$model = cmsCore::getModel($this->name);
			$model->setPerPage(admin::perpage);
			$filter = [];
			$filter_str = $this->request->get('filter', '');
			$filter_str = cmsUser::getUPSActual('odsp.periods_list', $filter_str);
			if ($filter_str) {
				parse_str($filter_str, $filter);
				$model->applyGridFilter($grid, $filter);
			}
			$total = $model->getPeriodsCount();
			$perpage = isset($filter['perpage']) ? $filter['perpage'] : admin::perpage;
			$pages = ceil($total / $perpage);
			$categories = $model->getPeriods();
			cmsTemplate::getInstance()->renderGridRowsJSON($grid, $categories, $total, $pages);
			$this->halt();
		}

		public function actionBookmakersAjax() {
			if (!$this->request->isAjax()) {
				cmsCore::error404();
			}
			$grid = $this->loadDataGrid('bookmakers');
			$model = cmsCore::getModel($this->name);
			$model->setPerPage(admin::perpage);
			$filter = [];
			$filter_str = $this->request->get('filter', '');
			$filter_str = cmsUser::getUPSActual('odsp.bookmakers_list', $filter_str);
			if ($filter_str) {
				parse_str($filter_str, $filter);
				$model->applyGridFilter($grid, $filter);
			}
			$total = $model->getBookmakersCount();
			$perpage = isset($filter['perpage']) ? $filter['perpage'] : admin::perpage;
			$pages = ceil($total / $perpage);
			$categories = $model->getBookmakers();
			cmsTemplate::getInstance()->renderGridRowsJSON($grid, $categories, $total, $pages);
			$this->halt();
		}

		public function actionReorder($what) {
			$items = $this->request->get('items');
			if (!$items) {
				cmsCore::error404();
			}
			$model = cmsCore::getModel($this->name);
			switch ($what) {
				case 'sports':
					$model->reorderSports($items);
					break;
				case 'bettings':
					$model->reorderBettings($items);
					break;
				case 'periods':
					$model->reorderPeriods($items);
					break;
				case 'bookmakers':
					$model->reorderBookmakers($items);
					break;
			}
			$this->redirectBack();
		}

		public function actionDelete($what, $id) {
			$model = cmsCore::getModel($this->name);
			switch ($what) {
				case 'sports':
					$model->deleteSport($id);
					break;
				case 'bettings':
					$model->deleteBetting($id);
					break;
				case 'periods':
					$model->deletePeriod($id);
					break;
				case 'bookmaker':
					$model->deleteBookmaker($id);
					break;
			}
			$this->redirectBack();
		}

	}
