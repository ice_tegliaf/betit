<?php

	function grid_bookmakers($controller) {

		$options = [
			'is_auto_init' => true,
			'is_sortable' => true,
			'is_filter' => true,
			'is_pagination' => true,
			'is_draggable' => true,
			'order_by' => 'ordering',
			'order_to' => 'asc',
			'show_id' => true
		];

		$columns = [
			'id' => [
				'title' => 'id',
				'width' => 30,
			],
			'title' => [
				'title' => "Название",
				//'href' => href_to($controller->root_url, 'edit', ['bookmaker', '{id}']),
			],
			'is_enabled' => [
				'title' => LANG_IS_ENABLED,
				'width' => 30,
				'flag' => true,
				'flag_toggle' => href_to($controller->root_url, 'toggle_item', ['{id}', 'betit_odsp_bookmakers', 'is_enabled']),
			],
		];

		$actions = [
//			[
//				'title' => LANG_EDIT,
//				'class' => 'edit',
//				'href' => href_to($controller->root_url, 'edit', ['bookmaker', '{id}']),
//			],
			[
				'title' => LANG_DELETE,
				'class' => 'delete',
				'href' => href_to($controller->root_url, 'delete', ['bookmaker', '{id}']),
				'confirm' => "Точно удалить букмекера {title}",
			]
		];

		return [
			'options' => $options,
			'columns' => $columns,
			'actions' => $actions
		];
	}
