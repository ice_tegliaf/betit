<?php

	function grid_sports($controller) {

		$options = [
			'is_auto_init' => true,
			'is_sortable' => true,
			'is_filter' => true,
			'is_pagination' => true,
			'is_draggable' => true,
			'order_by' => 'ordering',
			'order_to' => 'asc',
			'show_id' => true
		];

		$columns = [
			'id' => [
				'title' => 'id',
				'width' => 30,
			],
			'title' => [
				'title' => "Название (en)",
				//'href' => href_to($controller->root_url, 'edit', ['betting', '{id}']),
			],
			'title_ru' => [
				'title' => "Название(ru)"
			],
			'last_update' => [
				'title' => "Последнее обновление",
				'handler' => function ($value, $row) {
					if ($value == null)
						$value = mktime(0, 0, 0, 1, 1, 2000);
					return date("d.m H:i:s", $value);
				}
			],
			'update_interval' => [
				'title' => "Интервал обновлений",
				'handler' => function ($value, $row) {
					if ($value == null)
						$value = 0;
					$str = "";
					$str .= (int)($value / (3600 * 24)) . "д ";
					$str .= (int)($value % (3600 * 24) / 3600) . "ч ";
					$str .= ($value % (3600) / 60) . "м";
					return $str;
				},
				'editable' => [
					'table' => 'betit_odsp_sports'
				]
			],
			'is_enabled' => [
				'title' => LANG_IS_ENABLED,
				'width' => 30,
				'flag' => true,
				'flag_toggle' => href_to($controller->root_url, 'toggle_item', ['{id}', 'betit_odsp_sports', 'is_enabled']),
			],
		];

		$actions = [
//			[
//				'title' => LANG_EDIT,
//				'class' => 'edit',
//				'href' => href_to($controller->root_url, 'edit', ['betting', '{id}']),
//			],
//			[
//				'title' => LANG_DELETE,
//				'class' => 'delete',
//				'href' => href_to($controller->root_url, 'delete', ['betting', '{id}']),
//				'confirm' => "Точно удалить вид прогноза {title}",
//			]
		];

		return [
			'options' => $options,
			'columns' => $columns,
			'actions' => $actions
		];
	}
