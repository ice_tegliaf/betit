<?php

	class actionOdspPeriods extends cmsAction {

		public function run() {

			$grid = $this->loadDataGrid('periods');

			return cmsTemplate::getInstance()->render('backend/periods', [
				'grid' => $grid
			]);
		}

	}
