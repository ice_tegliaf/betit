<?php

	class actionOdspSports extends cmsAction {

		public function run() {

			$grid = $this->loadDataGrid('sports');

			return cmsTemplate::getInstance()->render('backend/sports', [
				'grid' => $grid
			]);
		}

	}
