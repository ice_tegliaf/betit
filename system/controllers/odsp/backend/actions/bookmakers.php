<?php

	class actionOdspBookmakers extends cmsAction {

		public function run() {

			$grid = $this->loadDataGrid('bookmakers');

			return cmsTemplate::getInstance()->render('backend/bookmakers', [
				'grid' => $grid
			]);
		}

	}
