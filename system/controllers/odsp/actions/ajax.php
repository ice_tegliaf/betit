<?php

	class actionOdspAjax extends cmsAction {
		public function run($action, $id = -1) {
			if (!$this->request->isAjax() && !cmsUser::getInstance()->is_admin)
				$this->controller->redirectToAction("index");
			switch ($action) {
				case "leagues_menu":
					$this->cms_template->renderJSON($this->model->getLeagues(true), true);
					break;
				case "sports_menu":
					$this->cms_template->renderJSON($this->model->getSports(), true);
					break;
				case "bookmakers":
					$bms = $this->model->getBookmakers();
					foreach ($bms as &$bm) {
						unset($bm['id'], $bm['url']);
					}
					$this->cms_template->renderJSON($bms, true);
					break;
				case "event_tabs":
					$tabs = $this->getEventTabs($id);
					$bettings = $this->model->getBettings(true);
					$periods = $this->model->getPeriods(true);
					echo cmsTemplate::getInstance()->renderInternal($this, "ajax/event_tabs_header", [
						"tabs" => $tabs,
						"bettings" => $bettings,
						"periods" => $periods
					]);
					break;
				case "event_tab":
					$odds = $this->getEventTab($id, $this->request->get("betting_id"), $this->request->get("scope_id"));
					$bookmakers = $this->model->getBookmakers(true);
					$betit_model = cmsCore::getModel("betit");
					$subscription = $betit_model->getActiveSubscription($this->cms_user->id);
					$is_capper = true;
					if ($subscription)
						$subscription['bookmakers'] = $subscription['bookmakers'] != null ? array_combine(array_values($subscription['bookmakers']), array_values($subscription['bookmakers'])) : null;
					else
						$is_capper = false;

					echo cmsTemplate::getInstance()->renderInternal($this, "ajax/event_tab", [
						"odds" => $odds,
						"bookmakers" => $bookmakers,
						"betting_id" => $this->request->get("betting_id"),
						"scope_id" => $this->request->get("scope_id"),
						"subscription" => $subscription,
						"is_capper" => $is_capper
					]);
					break;
				case "bookmakers":
					$this->getBookmakers();
					break;
				case "leagues":
					$this->getLeagues();
					break;
				case "league_events":
					echo $this->getLeagueEvents($id);
					break;
				case "event_result":
					$this->getEventResult($id);
					break;
			}
			exit;
		}

//TODO: do cache
		private function getBookmakers() {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$html = Parser::getHtmlContentFromUrl('http://www.oddsportal.com/res/x/bookies-161111135702-' . time() . '.js');
			$html = substr($html, 19, -164);
			$bookmakers = json_decode($html, true);
			$this->cms_database->delete("betit_odsp_bookmakers", "1");
			foreach ($bookmakers as $id => &$bookmaker) {
				$bookmaker = [
					'id' => $bookmaker['idProvider'],
					'title' => $bookmaker['WebName'],
					'url' => $bookmaker['Url']];
				$this->cms_database->insert("betit_odsp_bookmakers", $bookmaker);
			}
			return $bookmakers;
		}

//TODO: do cache
		private function getLeagues() {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$parser = new Parser();
			$leagues = $parser->getLeagues();
			cmsCache::getInstance()->clean('odsp.leagues');
			$sports = $this->model->getSports("name", true);
			$this->model->filterGtEqual("id", "1")->updateFiltered("betit_odsp_leagues", [
				"is_enabled" => 0
			]);
			foreach ($leagues as $league) {
				$league['sport_id'] = $sports[$league['sport_id']]["id"];
				$this->model->insertOrUpdate("betit_odsp_leagues", $league, [
					"is_enabled" => 1
				]);
			}
			return count($leagues);
		}

		private function getLeagueEvents($id) {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$parser = new Parser();
			$league = $this->cms_database->getRow("betit_odsp_leagues", "id=" . $id);
			$events = $parser->getLeagueEvents($league['url']);
			if (!$events || count($events) == 0) {
				$this->cms_database->update("betit_odsp_leagues", "id=" . $id, ['is_enabled' => 0]);
				return 0;
			}
			foreach ($events as &$event) {
				$this->cms_database
					->insertOrUpdate("betit_odsp_events",
					                 [
						                 'url' => $event['url'],
						                 'league_id' => $league["id"],
						                 'date_event' => $event["date_event"],
						                 'title' => $event["title"]],
					                 [
						                 'date_event' => $event["date_event"],
						                 'title' => $event["title"]
					                 ]);
				unset($event['url']);
			}
			return count($events);
		}

//TODO: do cache
		private function getEventResult($id) {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$parser = new Parser();
			$item = $this->model->getEvent($id);
			$result = $parser->getEventResult($item['url']);
			if ($result['result'] == null) {
				unset($result['result']);
			} else {
				$result['result'] = json_encode($result['result']);
			}
			if ($result['final_result'] == null) {
				unset($result['final_result']);
			} else {
				$result['final_result'] = json_encode($result['final_result']);
			}
			$this->model->update("betit_odsp_events", $item['id'], $result);
			return $result;
		}

		private function getEventTabs($id) {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$cache = new cmsCacheFiles(cmsConfig::getInstance());
			$cacheKey = "odsp.events.tabs.$id";
			if ($cache->has($cacheKey) && ($tabs = $cache->get($cacheKey)) != false) {

			} else {
				$parser = new Parser();
				$url = $this->model->getField("betit_odsp_events", $id, "url");
				$eventInfo = $parser->getEventOddsTabs($url);

				if ($eventInfo[0]['final_result'] == null)
					unset($eventInfo[0]['final_result']);
				else
					$eventInfo[0]['final_result'] = json_encode($eventInfo[0]['final_result']);

				if ($eventInfo[0]['result'] == null)
					unset($eventInfo[0]['result']);
				else
					$eventInfo[0]['result'] = json_encode($eventInfo[0]['result']);
				$this->model->update("betit_odsp_events", $id, $eventInfo[0]);
				$tabs = $eventInfo[1];
				$cache->set($cacheKey, $tabs, isset($eventInfo[0]['final_result']) ? 60 * 60 * 24 : 60 * 15);
			}
			return $tabs;
		}

		private function getEventTab($id, $bettingId, $scopeId, $nested = false) {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");

			$cache = new cmsCacheFiles(cmsConfig::getInstance());
			$cacheKey = "odsp.events.tabs.$id-$bettingId-$scopeId";
			if ($cache->has($cacheKey) && ($odds = $cache->get($cacheKey)) != false) {

			} else {
				$tabs = $this->getEventTabs($id);

				$eventUrl = $this->model->getField("betit_odsp_events", $id, "url");
				if (isset($tabs[$bettingId][$scopeId])) {
					$tabUrl = $tabs[$bettingId][$scopeId];

					$parser = new Parser();
					$res = $parser->getEventOddsForTab($eventUrl, $tabUrl);
				}
				if (!isset($res['d']['oddsdata'])) {
					if ($nested)
						return [];
					$cache->clean("odsp.events.tabs.$id");
					return $this->getEventTab($id, $bettingId, $scopeId, true);
				}
				$allOddsData = $res['d']['oddsdata']['back'];
				$odds = [];
				foreach ($allOddsData as $iden => $oddsData) {
					$bookmakers = $res['d']['nav'][$bettingId][$scopeId];
					$history = [];
					foreach ($bookmakers as $bookmaker) {
						$h = [];
						foreach ($oddsData["OutcomeID"] as $outcomeId => $outcomeHash) {
							if (isset($res['d']['history']['back'][$outcomeHash][$bookmaker])) {
								$t = [];
								foreach ($res['d']['history']['back'][$outcomeHash][$bookmaker] as $historyRow) {
									$t[$historyRow[2]] = round($historyRow[0], 2);
								}
								$h[$outcomeId] = $t;
							}
						}
						if (isset($oddsData["change_time"][$bookmaker]))
							foreach ($oddsData["change_time"][$bookmaker] as $outcomeId => $time) {
								$h[$outcomeId][$time] = round($oddsData["odds"][$bookmaker][$outcomeId], 2);
							}
						if (count($h) != 0)
							$history[$bookmaker] = $h;
					}
					$odds[$iden] = [
						"handicapType" => $oddsData["handicapType"],
						"handicapValue" => floatval($oddsData["handicapValue"]),
						"mixedParameterId" => $oddsData["mixedParameterId"],
						"bookmakers" => $bookmakers,
						"odds" => $oddsData["odds"],
						"history" => $history
					];
				}
				uasort($odds, function ($a, $b) {
					return $a['handicapValue'] < $b['handicapValue'] ? -1 : ($a['handicapValue'] > $b['handicapValue'] ? 1 : 0);
				});
				$cache->set($cacheKey, $odds, 60 * 15);
			}
			return $odds;
		}
	}