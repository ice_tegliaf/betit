<?php

	class odsp extends cmsFrontend {

		public function actionListLeaguesAjax() {
			$leagues = $this->model
				->filterEqual("sport_id", $this->request->get("value"))
				->getLeagues();
			foreach ($leagues as $key => &$league) {
				$league = $league['title'];
			}
			$leagues = array_merge([0 => ""], $leagues);
			cmsTemplate::getInstance()->renderJSON($leagues);
		}

		public function actionListPeriodsAjax() {
			$scopes = $this->model
				->getPeriods($this->request->get("value"));
			foreach ($scopes as $key => &$scope) {
				$scope = $scope['title'];
			}
			$scopes = array_merge([0 => ""], $scopes);
			cmsTemplate::getInstance()->renderJSON($scopes);
		}
	}
