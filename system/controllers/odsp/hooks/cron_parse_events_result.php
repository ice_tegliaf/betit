<?php

	set_time_limit(0);

	class onOdspCronParseEventsResult extends cmsAction {

		public function run() {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$parser = new Parser();
			$events = $this->model
				->filterEqual("is_finished", 0)
				->filterLt("date_event", time() - 30 * 60)
				->orderBy("date_event", "ASC")
				->limit(2000)
				->get("betit_odsp_events");
			if (!is_array($events))
				return;
			echo Color::set(count($events), "red+underline") . PHP_EOL;
			$i = 0;
			foreach ($events as $event) {
				$result = $parser->getEventResult($event['url']);
				if ($result['result'] == null) {
					unset($result['result']);
				} else {
					$result['result'] = json_encode($result['result']);
				}
				if ($result['final_result'] == null) {
					unset($result['final_result']);
				} else {
					$result['final_result'] = json_encode($result['final_result']);
				}
				if (isset($result['final_result']) && strpos($result['final_result'], "match")) {
					$result['is_finished'] = 0;
				}
				$this->model->update("betit_odsp_events", $event['id'], $result);
				echo ++$i . "(" . $event["id"] . ")\t";
				if ($i % 5 == 0) echo PHP_EOL;
			}
		}

	}