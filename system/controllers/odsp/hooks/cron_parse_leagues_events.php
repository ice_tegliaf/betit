<?php

	set_time_limit(0);

	class onOdspCronParseLeaguesEvents extends cmsAction {

		public function run() {
			cmsCore::loadLib("simple_html_dom.class", "simple_html_dom");
			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");
			$parser = new Parser();
			$leagues = $parser->getLeagues();
			if (!$leagues)
				return true;

			cmsCache::getInstance()->clean('odsp.leagues');
			$sports = $this->model->getSports("name", true);
			$this->model->filterGtEqual("id", "1")->updateFiltered("betit_odsp_leagues", [
				"is_enabled" => 0
			]);
			foreach ($leagues as $league) {
				$league['sport_id'] = $sports[$league['sport_id']]["id"];
				$this->model->insertOrUpdate("betit_odsp_leagues", $league, [
					"is_enabled" => 1
				]);
			}
			echo "leagues count: " . count($leagues) . PHP_EOL;
			foreach ($sports as $sport) {
				if ($sport['last_update'] != null && time() - $sport['last_update'] + 1 < $sport['update_interval'])
					continue;
				echo PHP_EOL . $sport['id'] . PHP_EOL;
				$this->parseSport($sport['id']);
				$this->cms_database->update("betit_odsp_sports", "id=" . $sport['id'], ['last_update' => time()]);
			}
			return true;
		}

		private function parseSport($id) {
			$leagues = $this->model->filterEqual("sport_id", $id)->getLeagues();
			if (!$leagues)
				return;
			$parser = new Parser();
			foreach ($leagues as $league) {
				$events = $parser->getLeagueEvents($league['url']);
				echo "(" . $league["id"] . "," . count($events) . ")\t";
				if (!$events || count($events) == 0) {
					$this->cms_database->update("betit_odsp_leagues", "id=" . $league['id'], ['is_enabled' => 0]);
					return;
				}
				foreach ($events as &$event) {
					$this->cms_database->insertOrUpdate("betit_odsp_events",
					                                    [
						                                    'url' => $event['url'],
						                                    'league_id' => $league["id"],
						                                    'date_event' => $event["date_event"],
						                                    'title' => $event["title"]],
					                                    [
						                                    'date_event' => $event["date_event"],
						                                    'title' => $event["title"]
					                                    ]);
				}
			}
		}
	}