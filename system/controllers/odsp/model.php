<?php

	class modelOdsp extends cmsModel {
		public function getBettingsCount($for_tab = false) {
			if ($for_tab) {
				$this->filterEqual("is_enabled", "1");
			}
			return $this->useCache("odsp.bettings")->getCount("betit_odsp_bettings");
		}

		public function getBettings($for_tab = false) {
			if ($for_tab) {
				$this->orderBy("ordering", 'asc')->filterEqual("is_enabled", "1");
			}
			return $this->useCache("odsp.bettings")->get("betit_odsp_bettings");
		}

		public function reorderBettings($ids_list) {
			(new cmsCache())->clean("odsp.bettings");
			$this->reorderByList('betit_odsp_bettings', $ids_list);
			return true;
		}

		public function getPeriodsCount($for_tab = false) {
			if ($for_tab) {
				$this->filterEqual("is_enabled", "1");
			}
			return $this->getCount("betit_odsp_periods");
		}

		public function getPeriods($for_tab = false) {
			if ($for_tab) {
				$this->orderBy("ordering", 'asc')->filterEqual("is_enabled", "1");
			}
			return $this->get("betit_odsp_periods");
		}


		public function getEvent($id) {
			return $this
				->joinLeft("betit_odsp_leagues", "bl", "bl.id=i.league_id")
				->select("bl.title", "league_title")
				->select("bl.country_name", "league_country")
				->select("bl.sport_id", "sport_id")
				->joinRight("betit_odsp_sports", "bs", "bs.id=bl.sport_id")
				->select("bs.title", "sport_title")
				->select("bs.title_ru", "sport_title_ru")
				->useCache("odsp.event")
				->getItemById("betit_odsp_events", $id, function ($item) {
					$item['result'] = json_decode($item['result'], true);
					$item['final_result'] = json_decode($item['final_result'], true);
					return $item;
				});
		}

		public function getEventTabs($id, $betting_id = -1, $scope_id = -1) {
			if ($scope_id != -1)
				return $this
					->filterEqual("event_id", $id)
					->filterEqual("betting_id", $betting_id)
					->filterEqual("scope_id", $scope_id)
					->getItem("betit_odsp_event_tabs");
			else
				return $this
					->filterEqual("event_id", $id)
					->get("betit_odsp_event_tabs", false, false);
		}

		public function getLeagues($group_by_sport_and_country = false, $with_hidden = false) {
			if ($with_hidden)
				$items = $this->useCache("odsp.leagues_h")->get("betit_odsp_leagues", function ($item) {
					return $item;
				});
			else
				$items = $this->useCache("odsp.leagues")->filterEqual("is_enabled", 1)->get("betit_odsp_leagues", function ($item) {
					return $item;
				});
			if (!$group_by_sport_and_country)
				return $items;
			$items_grouped = [];
			foreach ($items as $item) {
				$items_grouped[$item["sport_id"]][$item["country_name"]][] = $item;
			}
			return $items_grouped;
		}

		public function deleteSport($id) {
			return $this->delete("betit_odsp_sports", $id);
		}

		public function reorderSports($list) {
			return $this->reorderByList("betit_odsp_sports", $list);
		}

		public function getSportsCount($key = "id", $is_enabled = false) {
			if (!$is_enabled)
				$this->filterEqual("is_enabled", 1);
			return $this->useCache("odsp.sports." . $key)->getCount("betit_odsp_sports", $key);
		}

		public function getSports($key = "id", $is_enabled = false) {
			if (!$is_enabled)
				$this->filterEqual("is_enabled", 1);
			return $this->useCache("odsp.sports." . $key)->get("betit_odsp_sports", function ($item) {
				unset($item['url']);
				return $item;
			}, $key);
		}

		public function getLeague($league_id) {
			return $this->useCache("odsp.leagues")
			            ->joinRight("betit_odsp_sports", "bs", "bs.id=i.sport_id")
			            ->select("bs.title", "sport_title")
			            ->select("bs.title_ru", "sport_title_ru")
			            ->getItemById("betit_odsp_leagues", $league_id, function ($item) {
				            unset($item['url']);
				            return $item;
			            });
		}

		public function getEvents() {
			return $this->useCache("odsp.events")->get("betit_odsp_events", function ($item, $model) {
				$item['league'] = $model->getLeague($item['league_id']);
				unset($item['url']);
				return $item;
			});
		}

		public function getLeagueEvents($league_id) {
			return $this->useCache("odsp.events")->filterEqual("is_enabled", 1)->filterEqual("league_id", $league_id)->get("betit_odsp_events", function ($item) {
				unset($item['url']);
				return $item;
			});
		}

		public function getBookmakersCount($for_tab = false) {
			if ($for_tab) {
				$this->filterEqual("is_enabled", "1");
			}
			return $this->useCache("odsp.bookmakers")->getCount("betit_odsp_bookmakers");
		}

		public function getBookmakers($for_tab = false) {
			if ($for_tab) {
				$this->orderBy("ordering", 'asc')->filterEqual("is_enabled", "1");
			}
			return $this->useCache("odsp.bookmakers")->get("betit_odsp_bookmakers");
		}

		public function reorderBookmakers($ids_list) {
			(new cmsCache())->clean("odsp.bookmakers");
			$this->reorderByList('betit_odsp_bookmakers', $ids_list);
			return true;
		}

		public function addForecast($forecast) {
			$forecast['user_id'] = cmsUser::getInstance()->id;
			$forecast['date_forecast'] = time();
			return $this->insert("betit_odsp_forecasts", $forecast);
		}

		public function getUserForecastsCount($user_id) {
			return $this->filterEqual("user_id", $user_id)->getCount("betit_odsp_forecasts");
		}

		public function getUserForecasts($user_id, $expand = false) {
			$items = $this->joinRight("betit_odsp_bookmakers", "bm", "bm.id=i.bookmaker_id")->select("bm.title", "bookmaker_name")->filterEqual("user_id", $user_id)->get("betit_odsp_forecasts");

			cmsCore::loadLib("betit_odsp_parser.class", "betit_odsp_parser");

			if ($expand && $items) {
				foreach ($items as &$item) {
					$item['event'] = $this->getEvent($item['event_id']);
					$item['betting_name'] = Parser::getBettingName($item['betting_id'])['name'];
					$item['scope_name'] = Parser::getScopeName($item['scope_id']);
					$item['column_name'] = Parser::getColumnName($item['betting_id'], $item['column_id']);
				}
			}
			return $items;
		}

		public function convertForecastsPerEvent($forecasts) {
			$bets = [];
			if ($forecasts)
				foreach ($forecasts as $forecast) {
					$event_id = $forecast["event_id"];
					if (!array_key_exists($forecast["event_id"], $bets)) {
						$forecast["event"]["has_result"] = $forecast["event"]["is_finished"] == 1 && $forecast["event"]["is_postponed"] == 0 && $forecast["event"]["is_canceled"] == 0;
						$bets[$event_id] = $forecast["event"];
						$bets[$event_id]["forecasts"] = [];
					}
					unset($forecast["event"]);
					$bets[$event_id]["forecasts"][] = $forecast;
				}
			usort($bets, function ($a, $b) {
				return $a['date_event'] - $b['date_event'];
			});
			return $bets;
		}
	}
