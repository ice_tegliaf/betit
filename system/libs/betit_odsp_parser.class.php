<?php

	class Parser {
		private static $sportData = [
			[
				'name' => 'Football',
				'url' => 'soccer',
				'id' => 1,
			],
			[
				'name' => 'Tennis',
				'url' => 'tennis',
				'id' => 2,
			],
			[
				'name' => 'Basketball',
				'url' => 'basketball',
				'id' => 3,
			],
			[
				'name' => 'Hockey',
				'url' => 'hockey',
				'id' => 4,
			],
			[
				'name' => 'Handball',
				'url' => 'handball',
				'id' => 7,
			],
			[
				'name' => 'Baseball',
				'url' => 'baseball',
				'id' => 6,
			],
			[
				'name' => 'American-football',
				'url' => 'american-football',
				'id' => 5,
			],
			[
				'name' => 'Rugby-union',
				'url' => 'rugby-union',
				'id' => 8,
			],
			[
				'name' => 'Volleyball',
				'url' => 'volleyball',
				'id' => 12,
			],
			[
				'name' => 'Floorball',
				'url' => 'floorball',
				'id' => 9,
			],
			[
				'name' => 'Bandy',
				'url' => 'bandy',
				'id' => 10,
			],
			[
				'name' => 'Cricket',
				'url' => 'cricket',
				'id' => 13,
			],
			[
				'name' => 'Snooker',
				'url' => 'snooker',
				'id' => 15,
			],
			[
				'name' => 'Darts',
				'url' => 'darts',
				'id' => 14,
			],
			[
				'name' => 'Badminton',
				'url' => 'badminton',
				'id' => 21,
			],
			[
				'name' => 'Water-polo',
				'url' => 'water-polo',
				'id' => 22,
			],
			[
				'name' => 'Esports',
				'url' => 'esports',
				'id' => 36,
			],
			[
				'name' => 'Rugby League',
				'url' => 'rugby-league',
				'id' => 19,
			],
			[
				'name' => 'Boxing',
				'url' => 'boxing',
				'id' => 16,
			],
			[
				'name' => 'MMA',
				'url' => 'mma',
				'id' => 28,
			],
			[
				'name' => 'Futsal',
				'url' => 'futsal',
				'id' => 11,
			],
			[
				'name' => 'Beach Volleyball',
				'url' => 'beach-volleyball',
				'id' => 17,
			],
			[
				'name' => 'Aussie Rules',
				'url' => 'aussie-rules',
				'id' => 18,
			],
			[
				'name' => 'Pesäpallo',
				'url' => 'pesapallo',
				'id' => 30,
			],
			[
				'name' => 'Beach Soccer',
				'url' => 'beach-soccer',
				'id' => 26,
			],
		];

		private static $betting_names = [
			"1" => ["name" => "1X2", "short-name" => "1X2", "position" => "1", "outright" => false],
			"2" => ["name" => "Over/Under", "short-name" => "O/U", "position" => "4", "outright" => false],
			"3" => ["name" => "Home/Away", "short-name" => "Home/Away", "position" => "2", "outright" => false],
			"4" => ["name" => "Double Chance", "short-name" => "DC", "position" => "7", "outright" => false],
			"5" => ["name" => "Asian Handicap", "short-name" => "AH", "position" => "3", "outright" => false],
			"6" => ["name" => "Draw No Bet", "short-name" => "DNB", "position" => "5", "outright" => false],
			"7" => ["name" => "To Qualify", "short-name" => "TQ", "position" => "8", "outright" => false],
			"8" => ["name" => "Correct Score", "short-name" => "CS", "position" => "9", "outright" => false],
			"9" => ["name" => "Half Time / Full Time", "short-name" => "HT/FT", "position" => "10", "outright" => false],
			"10" => ["name" => "Odd or Even", "short-name" => "O/E", "position" => "11", "outright" => false],
			"11" => ["name" => "Winner", "short-name" => "Winner", "position" => "0", "outright" => true],
			"12" => ["name" => "European Handicap", "short-name" => "EH", "position" => "6", "outright" => false],
			"13" => ["name" => "Both Teams to Score", "short-name" => "BTS", "position" => "12", "outright" => false],
		];

		private static $column_names = [
			"1" => ["Home", "X", "Away"],
			"2" => ["Over", "Under"],
			"3" => ["Home", "Away"],
			"4" => ["1X", "12", "X2"],
			"5" => ["Home", "Away"],
			"6" => ["Home", "Away"],
			"7" => ["To Qualify", "TQ"],
			"8" => ["Odd"],
			"9" => ["Odd"],
			"10" => ["Odd", "Even"],
			"11" => ["Winner", "Winner"],
			"12" => ["Home", "X", "Away"],
			"13" => ["Yes", "No"],
		];

		public static $scope_names = [
			"1" => "FT including OT",
			"2" => "Full Time",
			"3" => "1st Half",
			"4" => "2nd Half",
			"5" => "1st Period",
			"6" => "2nd Period",
			"7" => "3rd Period",
			"8" => "1Q",
			"9" => "2Q",
			"10" => "3Q",
			"11" => "4Q",
			"12" => "1st Set",
			"13" => "2nd Set",
			"14" => "3rd Set",
			"15" => "4th Set",
			"16" => "5th Set",
			"17" => "1st Inning",
			"18" => "2nd Inning",
			"19" => "3rd Inning",
			"20" => "4th Inning",
			"21" => "5th Inning",
			"22" => "6th Inning",
			"23" => "7th Inning",
			"24" => "8th Inning",
			"25" => "9th Inning",
			"26" => "Next Set",
			"27" => "Current Set",
			"28" => "Next Game",
			"29" => "Current Game",
		];


		/**
		 * @param $needTypeSport
		 * @return string
		 */
		public static function getSportId($needTypeSport) {
			foreach (self::$sportData as $sport) {
				if ($sport["url"] === $needTypeSport) {
					return $sport["id"];
				}
			}

			return '';
		}

		/**
		 * @param      $url
		 * @param bool $headers
		 * @return bool|mixed|string
		 */
		public static function getHtmlContentFromUrl($url, $headers = false) {
			if (empty($url)) {
				return '';
			}

			$cookies = "./cookies.txt";

			sleep(1);
			$ch = curl_init($url);
			if ($headers) {
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			}
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			curl_setopt($ch, CURLOPT_PROXY, "52.236.180.0:21235");
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, "betit web123betit");

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0');

			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookies);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookies);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			$result = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			if ($httpCode === 301 || $httpCode === 302) {
				return false;
			} elseif ($httpCode === 404) {
				//страницы не существует
				return false;
			} elseif ($httpCode === 200) {
				return $result;
			} else {
				return false;
			}
		}

		/**
		 * @param $html
		 * @return mixed
		 */
		private static function getHash($html) {
			$re = '';
			if (strpos($html, 'PageNextMatches') !== false) {
				$re = '/PageNextMatches\((.+?)\)/';
			} elseif (strpos($html, 'PageEvent') !== false) {
				$re = '/PageEvent\((.+?)\);var menu_open/';
			}

			preg_match($re, $html, $matches);

			return json_decode($matches[1]);
		}

		public static function getBettingName($betting_id) {
			return self::$betting_names[$betting_id];
		}

		public static function getScopeName($scope_id) {
			return isset(self::$scope_names[$scope_id])?self::$scope_names[$scope_id]:"";
		}

		public static function getColumnNames($betting_id) {
			return self::$column_names[$betting_id];
		}

		public static function getColumnName($betting_id, $column_no) {
			return self::$column_names[$betting_id][$column_no];
		}

		public static function getScopeId($sport_id, $bettingTypeId) {
			$sportBetTypeScopeId = [
				"4" => ["3" => 1],
				"2" => ["1" => 2, "2" => 2, "3" => 2]];
			$betTypeScopeId = ["7" => 1];
			$sportScopeId = [
				"3" => 1,
				"5" => 1,
				"6" => 1,
				"13" => 1,
				"18" => 1];
			$scope = 2;
			if (isset($sportBetTypeScopeId[$sport_id]) && isset($sportBetTypeScopeId[$sport_id][$bettingTypeId])) {
				$scope = $sportBetTypeScopeId[$sport_id][$bettingTypeId];
			} elseif (isset($betTypeScopeId[$bettingTypeId])) {
				$scope = $betTypeScopeId[$bettingTypeId];
			} elseif (isset($sportScopeId[$sport_id])) {
				$scope = $sportScopeId[$sport_id];
			}
			return $scope;
		}

		public static function getDefaultBetType($sport_id) {
			$moneyLineSports = [
				'6' => 0,
				'2' => 1,
				'3' => 2,
				'5' => 3,
				'12' => 4,
				'14' => 5,
				'15' => 6,
				'13' => 7,
				'17' => 8,
				'18' => 9,
				'21' => 10,
				'28' => 11,
				'36' => 12,
			];
			return (isset($moneyLineSports[$sport_id])) ? 3 : 1;
		}

		/**
		 * @param      $url
		 * @return array|null
		 */
		public function getLeagueEvents($url) {
			$simpleDom = new simple_html_dom();
			$html = self::getHtmlContentFromUrl($url);
			$simpleDom->load($html);
			$resultArray = null;
			if ($simpleDom->find("#tournamentTable tr"))
				/**@var simple_html_dom $trow */
				foreach ($simpleDom->find("#tournamentTable tr") as $trow) {
					if (!$trow->find(".table-participant"))
						continue;
					$modifiedTime = trim(str_replace(['table-time', 'date', 'datet', 't'], '', $trow->find(".datet", 0)->attr["class"]));
					$modifiedTime = explode("-", $modifiedTime)[0];
					$res = [];
					$res["url"] = "http://www.oddsportal.com" . $trow->find("a", -1)->href;
					$res["title"] = $trow->find("a", -1)->plaintext;
					$res["date_event"] = $modifiedTime;
					$resultArray[] = $res;
				}
			return $resultArray;
		}

		public function getEventOddsTabs($item_url) {
			$sport_id = self::getSportId(explode('/', $item_url)[3]);
			$htmlOdd = self::getHtmlContentFromUrl($item_url);

			$hash = $this->getHash($htmlOdd);
			$hash_id = $hash->id;
			$hash_version_id = $hash->versionId;
			$hash_xhash = urldecode($hash->xhash);
			$bet_type = $this->getDefaultBetType($sport_id);
			$scope_id = $this->getScopeId($sport_id, $bet_type);

			$headers = ['Referer: ' . $item_url];
			$html = self::getHtmlContentFromUrl("http://fb.oddsportal.com/feed/match/$hash_version_id-$sport_id-$hash_id-$bet_type-$scope_id-$hash_xhash.dat", $headers);

			$html = substr($html, strpos($html, "{") - 1, -2);
			$struct = json_decode($html, true);
			$result = $this->getEventResultByHtml($htmlOdd);
			if (!array_key_exists('nav', $struct['d']))
				return [$result, []];
			$resultArray = [];
			foreach ($struct['d']['nav'] as $betting_id => $match_odd_value) {
				if ($betting_id == 12)
					continue;
				foreach ($match_odd_value as $scope_id => $value) {
					$resultArray[$betting_id][$scope_id] = "http://fb.oddsportal.com/feed/match/$hash_version_id-$sport_id-$hash_id-$betting_id-$scope_id-$hash_xhash.dat";
				}
			}
			return [$result, $resultArray];
		}

		public function getEventOddsForTab($item_url, $scope_url) {
			$headers = ['Referer: ' . $item_url];
			$html = self::getHtmlContentFromUrl($scope_url, $headers);

			$html = substr($html, strpos($html, "{") - 1, -2);

			return json_decode($html, true);
		}

		public function getLeagues() {
			$html = self::getHtmlContentFromUrl('http://www.oddsportal.com/');
			$simpleDom = new simple_html_dom();
			$simpleDom->load($html);
			$resultArray = [];
			/**@var simple_html_dom $sport */
			foreach ($simpleDom->find('#col-side #sports-menu li') as $sport) {
				$sport_name = substr($sport->find("a", 0)->href, 1, -1);
				/**@var simple_html_dom $country */
				foreach ($sport->find('ul li.country') as $country) {
					$country_name = $country->find("a", 0)->plaintext;
					if (strpos($country_name, "Popular") !== false) continue;
					/**@var simple_html_dom $league */
					foreach ($country->find('ul li') as $league) {
						$resultArray[] = [
							"sport_id" => $sport_name,
							"country_name" => $country_name,
							"title" => $league->find("a", 0)->plaintext,
							"url" => "http://www.oddsportal.com" . $league->find("a", 0)->href
						];
					}
				}
			}
			return $resultArray;
		}

		public function getEventResult($item_url) {
			$htmlOdd = self::getHtmlContentFromUrl($item_url);
			return $this->getEventResultByHtml($htmlOdd);
		}

		private function getEventResultByHtml($html) {
			$simpleDom = new simple_html_dom();
			$simpleDom->load($html);

			$raw_result = $simpleDom->find("#event-status", 0)->innertext;
			$result = $simpleDom->find("#event-status", 0)->plaintext;
			$modifiedTime = trim(str_replace(['table-time', 'date', 'datet', 't'], '', $simpleDom->find("#col-content", 0)->find(".datet", 0)->attr["class"]));
			$ret = ["is_finished" => false, "is_postponed" => false, "is_canceled" => false, "final_result" => null, "result" => null, "has_ot" => false, "date_event" => explode("-", $modifiedTime)[0]];
			if (strlen($result) < 5)
				return $ret;
			if ($result == "Postponed")
				$ret['is_postponed'] = true;
			elseif ($result == "Canceled" || strpos($result, 'walkover') !== false)
				$ret['is_canceled'] = true;
			else {
				$ret["is_finished"] = strpos($result, 'Final') !== false || strpos($result, 'retired') !== false;
			}

			if (strpos($result, 'awarded') !== false) {
				$ret['is_finished'] = true;
				list($a, $b) = explode(" - ", $simpleDom->find("#event-status", 0)->plaintext);
				if (strpos($result, $a) !== false) $ret['result'][] = [1, 0];
				elseif (strpos($result, $b) !== false) $ret['result'][] = [0, 1];
			} elseif (strpos($result, '(') !== false && strpos($result, ')') !== false) {
				$ret['is_finished'] = strpos($result, 'Final') !== false;
				$result_1 = substr($result, 0, strpos($result, "(") - 1);
				preg_match_all("(\d+:\d+)", $result_1, $out1);
				$ret['final_result'] = isset($out1[0]) && isset($out1[0][0]) ? explode(":", $out1[0][0]) : null;
				$result_1 = substr($raw_result, strripos($raw_result, "("));
				$result_1 = preg_replace("'<sup[^>]*?>.*?<\/sup>'si", "", $result_1);
				preg_match_all("(\d+:\d+)", substr(strip_tags($result_1), strpos($result_1, "(")), $out);
				foreach ($out[0] as $ou) {
					$ret['result'][] = explode(":", $ou);
				}
				$ret['has_ot'] = strpos($result, 'OT') !== false || strpos($result, 'penalties') !== false;
			} else {
				$ret['is_finished'] = strpos($result, 'Final') !== false;
				$ret['final_result'] = explode(":", trim(str_replace("Final result ", "", $result)));
			}
			return $ret;
		}
	}
