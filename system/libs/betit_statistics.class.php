<?php

	class Statistics {

		private static function getFTnoOT($result, $offset, $count) {
			$ret = [0, 0];
			for ($i = $offset; $i < $offset + $count; $i++) {
				$ret[0] += $result[$i][0];
				$ret[1] += $result[$i][1];
			}
			return $ret;
		}

		private static function getBettingSuccess1X2($forecast, $first, $second) {
			if ($first > $second && $forecast["column_id"] == 0 ||
				$first == $second && $forecast["column_id"] == 1 ||
				$first < $second && $forecast["column_id"] == 2)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessOverUnder($forecast, $first, $second) {
			$tmp = $forecast['column_id'] == 0 ? ($first + $second) - $forecast['handicap_value'] : $forecast['handicap_value'] - ($first + $second);

			if ($tmp <= -0.5)
				return -$forecast["percent"];
			if ($tmp == -0.25)
				return -$forecast["percent"] / 2; ///// * ($forecast["odd"] - 1)
			if ($tmp == 0)
				return 0;
			if ($tmp == 0.25)
				return ($forecast["odd"] - 1) * $forecast["percent"] / 2;
			if ($tmp >= 0.5)
				return $forecast["percent"] * ($forecast["odd"] - 1);
			return -$forecast["percent"];
		}

		private static function getBettingSuccessHomeAway($forecast, $first, $second) {
			if ($first > $second && $forecast["column_id"] == 0 ||
				$second > $first && $forecast["column_id"] == 1)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessDoubleChance($forecast, $first, $second) {
			if ($first >= $second && $forecast["column_id"] == 0 ||
				$first != $second && $forecast["column_id"] == 1 ||
				$first <= $second && $forecast["column_id"] == 2)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessAsianHandicap($forecast, $first, $second) {
			if ($forecast["column_id"] == 1)
				$forecast["handicap_value"] = -$forecast["handicap_value"];
			$tmp = ($forecast["column_id"] == 0 ? $first - $second : $second - $first) + $forecast["handicap_value"];
			if ($tmp <= -0.5)
				return -$forecast["percent"];
			if ($tmp == -0.25)
				return -$forecast["percent"] / 2;
			if ($tmp == 0)
				return 0;
			if ($tmp == 0.25)
				return $forecast["percent"] / 2 * ($forecast["odd"] - 1);
			if ($tmp >= 0.5)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessDrawNoBet($forecast, $first, $second) {
			if ($first > $second && $forecast["column_id"] == 0 || $first < $second && $forecast["column_id"] == 1)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			elseif ($first == $second)
				return 0;
			return -$forecast["percent"];
		}

		private static function getBettingSuccessCorrectScore($forecast, $first, $second) {
			$hp = explode(":", $forecast["handicap_value"]);
			if ((int)$hp[0] == $first && (int)$hp[1] == $second)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessHalfFullTime($forecast, $first, $second) {
			list($halftime, $fulltime) = explode("/", $forecast["handicap_value"]);
			if (
				($first[0] > $first[1] && $halftime == "1" ||
					$first[0] == $first[1] && $halftime == "X" ||
					$first[0] < $first[1] && $halftime == "2") &&

				($second[0] > $second[1] && $fulltime == "1" ||
					$second[0] == $second[1] && $fulltime == "X" ||
					$second[0] < $second[1] && $fulltime == "2")
			)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessOddEven($forecast, $first, $second) {
			if (($first + $second) % 2 == 0 && $forecast["column_id"] == 1 ||
				($first + $second) % 2 == 1 && $forecast["column_id"] == 0)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessBothTeamsScore($forecast, $first, $second) {
			$is_both = $first != 0 && $second != 0;
			if ($forecast["column_id"] == 0 && $is_both ||
				$forecast["column_id"] == 1 && !$is_both)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessToQualify($forecast, $first, $second) {
			if ($first > $second && $forecast["column_id"] == 0 ||
				$second > $first && $forecast["column_id"] == 1)
				return $forecast["odd"] * $forecast["percent"] - $forecast["percent"];
			return -$forecast["percent"];
		}

		private static function getBettingSuccessWinner($forecast, $first, $second) {
			//TODO
		}

		private static function getBettingSuccessEuropeanHandicap($forecast, $first, $second) {
			//TODO
		}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		private static function getSportResultSoccer($forecast, $result) {
			if ($forecast['betting_id'] == 9) //HalfFullTime
				return [
					2 => [$result[1], self::getFTnoOT($result, 1, 2)]
				];
			return [
				1 => [$result[0][0], $result[0][1]], //FT including OT
				2 => self::getFTnoOT($result, 1, 2), //FullTime
				3 => [$result[1][0], $result[1][1]], //1st Half
				4 => [$result[2][0], $result[2][1]]  //2nd Half
			];
		}

		private static function getSportResultTennis($forecast, $result) {
			if ($forecast["is_double_teams"]) {
				$key = 0;
				foreach ($result as $k => $res) {
					if ($res[0] != 0 || $res[1] != 0)
						$key = $k;
					else
						break;
				}
				$result[$key] = [$result[$key][0] > $result[$key][1] ? 1 : 0, $result[$key][0] < $result[$key][1] ? 1 : 0];
			}
			return [
				2 => $forecast['handicap_type'] == 2 ? self::getFTnoOT($result, 1, 5) : $result[0], //FullTime
				12 => [$result[1][0], $result[1][1]], //1st Set
				13 => [$result[2][0], $result[2][1]], //2st Set
				14 => [$result[3][0], $result[3][1]], //3st Set
				15 => [$result[4][0], $result[4][1]], //4st Set
				16 => [$result[5][0], $result[5][1]]  //5st Set
			];
		}

		private static function getSportResultBasketball($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 4), //Full Time
				8 => [$result[1][0], $result[1][1]], //1Q
				9 => [$result[2][0], $result[2][1]], //2Q
				10 => [$result[3][0], $result[3][1]], //3Q
				11 => [$result[4][0], $result[4][1]], //4Q
				3 => self::getFTnoOT($result, 1, 2), //1st Half
				4 => self::getFTnoOT($result, 3, 2)  //2nd Half
			];
		}

		private static function getSportResultHockey($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 3), //Full Time
				5 => [$result[1][0], $result[1][1]], //1st Period
				6 => [$result[2][0], $result[2][1]], //2st Period
				7 => [$result[3][0], $result[3][1]], //3st Period
			];
		}

		private static function getSportResultHandball($forecast, $result) {
			if ($forecast['betting_id'] == 9) //HalfFullTime
				return [
					2 => [$result[1], $result[0]]
				];
			return [
				2 => self::getFTnoOT($result, 1, 2), //FullTime
				3 => [$result[1][0], $result[1][1]], //1st Half
				4 => [$result[2][0], $result[2][1]]  //2nd Half
			];
		}

		private static function getSportResultBaseball($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 9),
				17 => [$result[1][0], $result[1][1]],
				18 => [$result[2][0], $result[2][1]],
				19 => [$result[3][0], $result[3][1]],
				20 => [$result[4][0], $result[4][1]],
				21 => [$result[5][0], $result[5][1]],
				22 => [$result[6][0], $result[6][1]],
				23 => [$result[7][0], $result[7][1]],
				24 => [$result[8][0], $result[8][1]],
				25 => [$result[9][0], $result[9][1]]
			];
		}

		private static function getSportResultAmericanFootball($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 4), //Full Time
				8 => [$result[1][0], $result[1][1]], //1Q
				9 => [$result[2][0], $result[2][1]], //2Q
				10 => [$result[3][0], $result[3][1]], //3Q
				11 => [$result[4][0], $result[4][1]], //4Q
				3 => self::getFTnoOT($result, 1, 2), //1st Half
				4 => self::getFTnoOT($result, 3, 2)  //2nd Half
			];
		}

		private static function getSportResultRugbyUnion($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 2), //Full Time
			];
		}

		private static function getSportResultRugbyLeague($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 2), //Full Time
			];
		}


		private static function getSportResultFutsal($forecast, $result) {
			if ($forecast['betting_id'] == 9) //HalfFullTime
				return [
					2 => [$result[1], $result[0]]
				];
			return [
				2 => self::getFTnoOT($result, 1, 2), //FullTime
			];
		}

		private static function getSportResultCricket($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => [$result[0][0], $result[0][1]]//Full Time
			];
		}

		private static function getSportResultSnooker($forecast, $result) {
			return [
				2 => [$result[0][0], $result[0][1]]//Full Time
			];
		}

		private static function getSportResultVolleyball($forecast, $result) {
			if ($forecast['betting_id'] == 2 && $forecast['scope_id'] == 2) //HalfFullTime
				return [
					2 => self::getFTnoOT($result, 1, 5)
				];
			return [
				2 => $result[0], //FullTime
				12 => [$result[1][0], $result[1][1]], //1st Set
				13 => [$result[2][0], $result[2][1]], //2st Set
				14 => [$result[3][0], $result[3][1]], //3st Set
				15 => [$result[4][0], $result[4][1]], //4st Set
				16 => [$result[5][0], $result[5][1]]  //5st Set
			];
		}

		private static function getSportResultBoxing($forecast, $result) {
			//TODO
		}

		private static function getSportResultBeachVolleyball($forecast, $result) {
			//$forecast['is_ot']
			//$forecast["is_double_teams"]
			return [
				2 => $forecast['handicap_type'] == 2 ? self::getFTnoOT($result, 1, 5) : $result[0], //FullTime
				12 => [$result[1][0], $result[1][1]], //1st Set
				13 => [$result[2][0], $result[2][1]], //2st Set
				14 => [$result[3][0], $result[3][1]], //3st Set
				15 => [$result[4][0], $result[4][1]], //4st Set
				16 => [$result[5][0], $result[5][1]]  //5st Set
			];
		}

		private static function getSportResultAussieRules($forecast, $result) {
			//TODO
		}

		private static function getSportResultBadminton($forecast, $result) {
			//$forecast['is_ot']
			//$forecast["is_double_teams"]
			return [
				2 => $forecast['handicap_type'] == 2 ? self::getFTnoOT($result, 1, 5) : $result[0], //FullTime
				12 => [$result[1][0], $result[1][1]], //1st Set
				13 => [$result[2][0], $result[2][1]], //2st Set
				14 => [$result[3][0], $result[3][1]], //3st Set
				15 => [$result[4][0], $result[4][1]], //4st Set
				16 => [$result[5][0], $result[5][1]]  //5st Set
			];
		}

		private static function getSportResultPesapallo($forecast, $result) {
			//TODO
		}

		private static function getSportResultEsports($forecast, $result) {
			return [
				2 => [$result[0][0], $result[0][1]],//Full Time
				12 => [$result[1][0], $result[1][1]],//Full Time
			];
		}

		private static function getSportResultMma($forecast, $result) {
			//TODO
		}

		private static function getSportResultFloorball($forecast, $result) {
			return [
				2 => self::getFTnoOT($result, 1, 2), //FullTime
			];
		}

		private static function getSportResultBandy($forecast, $result) {
			if ($forecast['betting_id'] == 9) //HalfFullTime
				return [
					2 => [$result[1], $result[0]]
				];
			return [
				2 => self::getFTnoOT($result, 1, 2), //FullTime
				3 => [$result[1][0], $result[1][1]], //1st Half
				4 => [$result[2][0], $result[2][1]]  //2nd Half
			];
		}

		private static function getSportResultDarts($forecast, $result) {
			//TODO
		}

		private static function getSportResultWaterPolo($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 4), //Full Time
				8 => [$result[1][0], $result[1][1]], //1Q
				9 => [$result[2][0], $result[2][1]], //2Q
				10 => [$result[3][0], $result[3][1]], //3Q
				11 => [$result[4][0], $result[4][1]], //4Q
				3 => self::getFTnoOT($result, 1, 2), //1st Half
				4 => self::getFTnoOT($result, 3, 2)  //2nd Half
			];
		}

		private static function getSportResultBeachSoccer($forecast, $result) {
			return [
				1 => [$result[0][0], $result[0][1]], //FT inc OT
				2 => self::getFTnoOT($result, 1, 3), //Full Time
				5 => [$result[1][0], $result[1][1]], //1st Period
				6 => [$result[2][0], $result[2][1]], //2st Period
				7 => [$result[3][0], $result[3][1]], //3st Period
			];
		}

		private static $sports = ["Soccer", "Tennis", "Basketball", "Hockey", "Handball", "Baseball", "AmericanFootball",
			"RugbyUnion", "RugbyLeague", "Volleyball", "Futsal", "Cricket", "Snooker", "Boxing", "BeachVolleyball",
			"AussieRules", "Badminton", "Pesapallo", "Esports", "Mma", "Floorball", "Bandy", "Darts", "WaterPolo", "BeachSoccer"];


		private static $bettings = ["1X2", "OverUnder", "HomeAway", "DoubleChance", "AsianHandicap", "DrawNoBet",
			"ToQualify", "CorrectScore", "HalfFullTime", "OddEven", "Winner", "EuropeanHandicap", "BothTeamsScore"];

		/**
		 * @param $forecast
		 * @param $flat
		 * @return mixed
		 */
		private static function makeEventStat($forecast, $flat) {
			$result = is_array($forecast['result']) ? $forecast['result'] : [];
			$result = array_merge($result, [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]);
			array_unshift($result, $forecast['final_result']);
			$forecast['success'] = $forecast['turnover'] = 0;
			$is_double_team = count(explode("/", $forecast['title'])) == 3;
			$forecast['success'] = $forecast['success_flat'] = 0;
			$forecast['success'] = 0;
			$forecast["win"] = $forecast["ret"] = $forecast["lose"] = 0;
			if (!($forecast["type"] == "odsp" && $forecast['is_canceled'])) {
				$forecast["is_double_teams"] = $is_double_team;
				$sport_results = call_user_func_array(['self', "getSportResult" . self::$sports[$forecast['sport_id'] - 1]], [$forecast, $result]);

				$f2 = $forecast;
				$f2['percent'] = $flat;
				$forecast['success'] = call_user_func_array(['self', "getBettingSuccess" . self::$bettings[$forecast['betting_id'] - 1]],
				                                            [$forecast, $sport_results[$forecast['period_id']][0], $sport_results[$forecast['period_id']][1]]);
				if ($forecast['success'] > 0)
					$forecast["win"] = 1;
				elseif ($forecast['success'] == 0)
					$forecast["ret"] = 1;
				else
					$forecast["lose"] = 1;
				$forecast['success_flat'] = call_user_func_array(['self', "getBettingSuccess" . self::$bettings[$f2['betting_id'] - 1]],
				                                                 [$f2, $sport_results[$f2['period_id']][0], $sport_results[$f2['period_id']][1]]);
			}

			return $forecast;
		}

		/**
		 * @param $forecasts
		 * @param $expresses
		 * @param $startBankroll
		 * @return array
		 */
		public function generateStatistics($forecasts, $expresses, $startBankroll) {
//        dump($forecasts, false);
			if (count($forecasts) == 0)
				return [[], $forecasts];
			$statistics = ["graph" => [[0, $startBankroll]], "graph_flat" => [[0, $startBankroll]], 'bankrolls' => [], 'bankrolls_flat' => []];

			$win = $lose = $return = 0;
			$turnover = $turnover_flat = 0;

			$bankroll = $bankroll_flat = $bankroll_flat_last = $startBankroll;

			$dropdown_local_max = $bankroll;
			$dropdown_local_min = 1E20;
			$dropdown = 1E-20;

			$dropdown_flat_local_max = $bankroll_flat;
			$dropdown_flat_local_min = 1E20;
			$dropdown_flat = 1E-20;

			$i = 0;
			$flat = [];

			$for_flat_forecasts = array_merge($forecasts, $expresses);
			foreach ($for_flat_forecasts as &$forecast) {
				if (!array_key_exists($forecast['percent'], $flat))
					$flat[$forecast['percent']] = 0;
				$flat[$forecast['percent']]++;
			}

			arsort($flat);
			$flat = array_keys($flat, reset($flat));
			$flat = array_sum($flat) / count($flat);
			$percent_sum = 0;

			$forecasts = array_merge($forecasts, self::makeExpresses($expresses, $flat));
			uksort($forecasts, function ($a, $b) use ($forecasts) {
				return $forecasts[$a]['date_forecast'] <=> $forecasts[$b]['date_forecast'];
			});
			//dump($forecasts);
			foreach ($forecasts as &$forecast) {
				if (!isset($forecast['is_express'])) {
					if (count($forecast['result']) == 0 && count($forecast["final_result"]) != 0)
						$forecast['result'] = [$forecast["final_result"]];

					$forecast = self::makeEventStat($forecast, $flat);
				}

				$percent_sum += $forecast["percent"];
				$bankroll *= 1 + $forecast['success'] / 100;
				$bankroll_flat *= 1 + $forecast['success_flat'] / 100;
				$turnover += $forecast['percent'];
				$win += $forecast["win"];
				$return += $forecast["ret"];
				$lose += $forecast["lose"];

				$statistics["bankrolls"][] = $bankroll;
				$statistics["bankrolls_flat"][] = $bankroll_flat;
				$statistics["graph"][] = [++$i, round($bankroll, 2)];
				$statistics["graph_flat"][] = [$i, round($bankroll_flat, 2)];

				if ($bankroll_flat - $bankroll_flat_last > 0) {
					$turnover_flat += $bankroll_flat - $bankroll_flat_last;
				}

				if ($bankroll >= $dropdown_local_max) {
					$dropdown = max($dropdown, ($dropdown_local_max - $dropdown_local_min) / $dropdown_local_max);
					$dropdown_local_max = $bankroll;
					$dropdown_local_min = $bankroll;
				} else {
					$dropdown_local_min = min($bankroll, $dropdown_local_min);
				}

				if ($bankroll_flat >= $dropdown_flat_local_max) {
					$dropdown_flat = max($dropdown_flat, ($dropdown_flat_local_max - $dropdown_flat_local_min) / $dropdown_flat_local_max);
					$dropdown_flat_local_max = $bankroll_flat;
					$dropdown_flat_local_min = $bankroll_flat;
				} else {
					$dropdown_flat_local_min = min($bankroll_flat, $dropdown_flat_local_min);
				}
				$bankroll_flat_last = $bankroll_flat;
			}
			$dropdown = max($dropdown, ($dropdown_local_max - $dropdown_local_min) / $dropdown_local_max) * 100;
			$dropdown_flat = max($dropdown, ($dropdown_flat_local_max - $dropdown_flat_local_min) / $dropdown_flat_local_max) * 100;

			if ($i == 0)
				return [[], $forecasts];

			$mean = $percent_sum / $i;
			$statistics["flat"] = $flat;
			$statistics["count_of_predict"] = [$win, $lose, $return];
			$statistics["mean_bet"] = round($mean, 2);

			$statistics["profit"] = round(end($statistics["bankrolls"]) - $startBankroll, 2);
			$statistics["profit_flat"] = round(end($statistics["bankrolls_flat"]) - $startBankroll, 2);

			$statistics["clear_profit_per"] = round($statistics["profit"] / $startBankroll * 100, 2);
			$statistics["clear_profit_flat_per"] = round($statistics["profit_flat"] / $startBankroll * 100, 2);

			$statistics["dropdown"] = round($dropdown, 2);
			$statistics["dropdown_flat"] = round($dropdown_flat / 100, 2); //TODO:
			$statistics["turnover_per"] = $turnover;
			$statistics["turnover_flat_per"] = round(100 * $turnover_flat / end($statistics["bankrolls"]), 4); //TODO:
			if ($turnover != 0)
				$statistics["turnover_per_bet"] = round(100 * $statistics["clear_profit_per"] / $statistics["turnover_per"], 2);
			else
				$statistics["turnover_per_bet"] = "~";
			if ($turnover_flat != 0)
				$statistics["turnover_flat_per_bet"] = round(100 * $statistics["clear_profit_flat_per"] / $statistics["turnover_flat_per"], 2);
			else
				$statistics["turnover_flat_per_bet"] = "~";
			$statistics["ROI"] = round(($statistics["clear_profit_per"] / $percent_sum) * 100, 2);
			$statistics["ROI_flat"] = round(($statistics["clear_profit_flat_per"] / $percent_sum) * 100, 2); //TODO:

			$statistics["racio"] = $statistics["dropdown"] > 0 ? round($statistics["clear_profit_per"] / $statistics["dropdown"], 2) : 0;
			$statistics["racio_flat"] = $statistics["dropdown_flat"] > 0 ? round($statistics["clear_profit_flat_per"] / $statistics["dropdown_flat"], 2) : 0;

			return [$statistics, $forecasts];
		}

		/**
		 * @param $expresses
		 * @param $flat
		 * @return array
		 */
		public static function makeExpresses($expresses, $flat) {
			$ret = [];
			foreach ($expresses as $express_id => &$express) {
				$express['is_express'] = true;
				$express['date_forecast'] = $express["open_time"];

				$express['win'] = 0;
				$express['ret'] = 0;
				$express['lose'] = 0;
				$express['odd'] = 1;
				$express['is_finished'] = true;
				$express['is_success'] = true;
				$express['success'] = 1;
				$express['success_flat'] = 1;
				$express['ret'] = 0;
				foreach ($express['forecasts'] as &$forecast) {
					if ($forecast['is_finished'] == 1) {
						if (count($forecast['result']) == 0 && count($forecast["final_result"]) != 0)
							$forecast['result'] = [$forecast["final_result"]];
						$forecast["percent"] = 1;
						$forecast = self::makeEventStat($forecast, $flat);
						$express['success'] *= $forecast["success"];
						$express['success_flat'] *= $forecast["success_flat"];
						$express['odd'] *= $forecast['odd'];
						$express['ret'] += $forecast['ret'];
						$forecast['is_success'] = ($forecast['win'] + $forecast['ret']) != 0; //if lose == 0 => is_win + is_ret != 0
						$express['is_success'] &= $forecast['is_success'];
						$express['is_finished'] &= true;
					} else {
						$express['is_finished'] = false;
						break;
					}
				}
				if ($express['is_finished']) {
					$express['express_id'] = $express_id;
					$express['ret'] = $express['ret'] > 0 ? 1 : 0;
					if ($express['is_success']) {
						$express['success'] = $express['success'] * ($express['percent'] - 1);
						$express['success_flat'] = $express['success_flat'] * ($express['percent'] - 1);
						$express['win'] = 1;
						$express['ret'] = 0;
					} else {
						$express['success'] = -$express['percent'];
						$express['success_flat'] = -$express['percent'];
						$express['lose'] = 1;
						$express['ret'] = 0;
					}
					$ret[] = $express;
				}
			}
			return $ret; //TODO continue expresses forecast in calculating stat. Check this code and fix this shitcode to normal code))

			/**
			 * ░░░░░░░░░░░░░░░░░░░░░░░▄▄
			 * ░░░░░░░░░░░░░░░░░░░░░▄▀░░▌
			 * ░░░░░░░░░░░░░░░░░░░▄▀▐░░░▌
			 * ░░░░░░░░░░░░░░░░▄▀▀▒▐▒░░░▌
			 * ░░░░░▄▀▀▄░░░▄▄▀▀▒▒▒▒▌▒▒░░▌
			 * ░░░░▐▒░░░▀▄▀▒▒▒▒▒▒▒▒▒▒▒▒▒█
			 * ░░░░▌▒░░░░▒▀▄▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄
			 * ░░░░▐▒░░░░░▒▒▒▒▒▒▒▒▒▌▒▐▒▒▒▒▒▀▄
			 * ░░░░▌▀▄░░▒▒▒▒▒▒▒▒▐▒▒▒▌▒▌▒▄▄▒▒▐
			 * ░░░▌▌▒▒▀▒▒▒▒▒▒▒▒▒▒▐▒▒▒▒▒█▄█▌▒▒▌                (это довольный котик. Ведь у него нет дилдлайнов)
			 * ░▄▀▒▐▒▒▒▒▒▒▒▒▒▒▒▄▀█▌▒▒▒▒▒▀▀▒▒▐░░░▄
			 * ▀▒▒▒▒▌▒▒▒▒▒▒▒▄▒▐███▌▄▒▒▒▒▒▒▒▄▀▀▀▀
			 * ▒▒▒▒▒▐▒▒▒▒▒▄▀▒▒▒▀▀▀▒▒▒▒▄█▀░░▒▌▀▀▄▄
			 * ▒▒▒▒▒▒█▒▄▄▀▒▒▒▒▒▒▒▒▒▒▒░░▐▒▀▄▀▄░░░░▀
			 * ▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▄▒▒▒▒▄▀▒▒▒▌░░▀▄
			 * ▒▒▒▒▒▒▒▒▀▄▒▒▒▒▒▒▒▒▀▀▀▀▒▒▒▄▀
			 * ▒▒▒▒▒▒▒▒▒▒▀▄▄▒▒▒▒▒▒▒▒▒▒▒▐
			 * ▒▒▒▒▒▒▒▒▒▒▒▒▒▀▀▄▄▄▒▒▒▒▒▒▌
			 * ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐
			 * ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐
			 */
		}

	}