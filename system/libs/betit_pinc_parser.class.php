<?php

	class Pinnacle {
		private static $baseUrl = "https://api.pinnacle.com/";
		private static $login = "SN864174";//"MK1037160";
		private static $password = "251186Mam!";//"JDnvr_3fvme4";

		/**
		 * @param $method
		 * @param $params
		 * @return bool|mixed|string
		 */
		public static function query($method, $params = []) {
			$url = self::$baseUrl . $method . "?" . http_build_query($params);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERPWD, self::$login . ":" . self::$password);

			curl_setopt($ch, CURLOPT_PROXY, "52.236.180.0:21235");
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, "betit web123betit");

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0');

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);
			curl_setopt($ch, CURLOPT_TIMEOUT, 90);

			$result = curl_exec($ch);
			curl_close($ch);
			if (strpos($result, '{') !== null)
				return json_decode($result, true);
			return false;
		}

		public function getSports() {
			$data = self::query("v2/sports", [

			]);
			if (!$data["sports"])
				return [];
			$ret = [];
			foreach ($data["sports"] as $sport) {
				$ret_sport["id"] = $sport["id"];
				$ret_sport["name"] = $sport["name"];
				$ret_sport["eventCount"] = $sport["eventCount"];
				$ret[] = $ret_sport;
			}
			return $ret;
		}

		public function getLeagues($sport_id) {
			$data = self::query("v2/leagues", [
				"sportId" => $sport_id
			]);
			if (!isset($data["leagues"]))
				return [];
			$ret = [];
			foreach ($data["leagues"] as $league) {
				$ret_league["id"] = $league["id"];
				$ret_league["country"] = $league["container"];
				$ret_league["name"] = $league["name"];
				$ret_league["eventCount"] = $league["eventCount"];
				$ret[] = $ret_league;
			}
			return $ret;
		}

		public function getOdds($sport_id, $leagues_id) {
			$data = self::query("v1/odds", [
				"sportId" => $sport_id,
				"leagueIds" => $leagues_id,
				"oddsFormat" => "Decimal"
			]);

			if (!isset($data["leagues"]))
				return [];
			$ret = [];
			foreach ($data["leagues"] as $league) {
				foreach ($league["events"] as $event) {
					$ret_event["id"] = $event['id'];
					$ret_event["awayScore"] = isset($event["awayScore"]) ? $event["awayScore"] : 0;
					$ret_event["homeScore"] = isset($event["homeScore"]) ? $event["homeScore"] : 0;
					$ret_event["periods"] = [];
					foreach ($event["periods"] as $period) {
						if (!isset($period["spreads"]) && !isset($period["totals"]) && !isset($period["teamTotal"]))
							continue;
						$ret_period["number"] = $period["number"];
						$ret_period["date"] = strtotime($period["cutoff"]);
						$ret_period["maxMoneyline"] = isset($event["maxMoneyline"]) ? $event["maxMoneyline"] : 0;
						$ret_period["spreads"] = [];
						if (isset($period["spreads"])) {
							foreach ($period["spreads"] as $spread) {
								unset($spread['altLineId']);
								$ret_period["spreads"][] = $spread;
							}
						}
						$ret_period["moneyline"] = isset($period["moneyline"]) ? $period["moneyline"] : [];
						$ret_period["totals"] = [];
						if (isset($period["totals"])) {
							foreach ($period["totals"] as $total) {
								unset($total['altLineId']);
								$ret_period["totals"][] = $total;
							}
						}
						$ret_period["teamTotal"] = isset($period["teamTotal"]) ? $period["teamTotal"] : [];
						$ret_event["periods"][] = $ret_period;
					}
					if (count($ret_event["periods"]) > 0)
						$ret[] = $ret_event;
				}
			}
			return $ret;
		}

		public function getEvent($sport_id, $leagues_id, $eventIds) {
			$data = self::query("v1/fixtures", [
				"sportId" => $sport_id,
				"leagueIds" => $leagues_id,
				"eventIds" => implode(",", $eventIds),
			]);
			if (!isset($data["league"]))
				return [];
			$ret = [];
			foreach ($data["league"] as $league) {
				if (!$league["events"])
					continue;
				foreach ($league["events"] as $event) {
					$ret[$event["id"]] = [
						"id" => $event['id'],
						"league_id" => $league['id'],
						"title" => $event["home"] . " - " . $event["away"],
						"date_event" => strtotime($event["starts"]),
						"is_finished" => $event['status'] != "O"
					];
				}
			}
			return $ret;
		}


	}