CREATE TABLE `cms_betit_events` (
  `id` int(11) NOT NULL,
  `league_id` int(11) NOT NULL,
  `date_event` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `periods` text,
  `is_finished` tinyint(1) NOT NULL DEFAULT '0',
  `result` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_betit_forecasts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` set('self','odsp','pinc','') NOT NULL,
  `event_id` int(11) NOT NULL,
  `betting_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `handicap` varchar(11) NOT NULL DEFAULT '0.00',
  `bookmaker_id` int(11) NOT NULL,
  `column_id` int(1) NOT NULL,
  `odd` float NOT NULL,
  `date_forecast` int(11) NOT NULL,
  `percent` int(3) NOT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Структура таблицы `cms_betit_odsp_bettings`
--

CREATE TABLE `cms_betit_odsp_bettings` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_short` varchar(10) NOT NULL,
  `ordering` int(11) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `cms_betit_odsp_bettings`
--

INSERT INTO `cms_betit_odsp_bettings` (`id`, `title`, `title_short`, `ordering`, `is_enabled`) VALUES
(1, '1X2', '1X2', 2, 1),
(2, 'Over/Under', 'O/U', 3, 1),
(3, 'Home/Away', 'Home/Away', 4, 1),
(4, 'Double Chance', 'DC', 5, 1),
(5, 'Asian Handicap', 'AH', 6, 1),
(6, 'Draw No Bet', 'DNB', 7, 1),
(7, 'To Qualify', 'TQ', 8, 1),
(8, 'Correct Score', 'CS', 9, 1),
(9, 'Half Time / Full Time', 'HT/FT', 10, 1),
(10, 'Odd or Even', 'O/E', 11, 1),
(11, 'Winner', 'Winner', 12, 1),
(12, 'European Handicap', 'EH', 13, 1),
(13, 'Both Teams to Score', 'BTS', 14, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_betit_odsp_bookmakers`
--

CREATE TABLE `cms_betit_odsp_bookmakers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_betit_odsp_bookmakers`
--

INSERT INTO `cms_betit_odsp_bookmakers` (`id`, `title`, `picture`, `ordering`, `is_enabled`) VALUES
(1, 'Interwetten', NULL, 2, 1),
(2, 'bwin', NULL, 3, 1),
(3, 'bet-at-home', NULL, 4, 1),
(4, 'Centrebet', NULL, 5, 1),
(5, 'Unibet', NULL, 6, 1),
(7, 'Betsson exch.', NULL, 7, 1),
(8, 'Stan James', NULL, 8, 1),
(9, 'Expekt', NULL, 9, 1),
(10, 'PartyBets', NULL, 10, 1),
(11, 'Gamebookers', NULL, 11, 1),
(12, 'Canbet', NULL, 12, 1),
(14, '10Bet', NULL, 13, 1),
(15, 'William Hill', NULL, 14, 1),
(16, 'bet365', NULL, 15, 1),
(17, 'Blue Square', NULL, 16, 1),
(18, 'Pinnacle', NULL, 17, 1),
(19, 'Sports Interaction', NULL, 18, 1),
(20, '5Dimes', NULL, 19, 1),
(21, 'Betfred', NULL, 20, 1),
(23, 'DOXXbet', NULL, 21, 1),
(24, 'Betsafe', NULL, 22, 1),
(26, 'Betway', NULL, 23, 1),
(27, '888sport', NULL, 24, 1),
(28, 'Ladbrokes', NULL, 25, 1),
(29, 'Sunmaker', NULL, 26, 1),
(30, 'BoyleSports', NULL, 27, 1),
(31, 'Intertops', NULL, 28, 1),
(32, 'Betclic', NULL, 29, 1),
(33, 'NordicBet', NULL, 30, 1),
(34, 'Bodog', NULL, 31, 1),
(35, 'Eurobet', NULL, 35, 1),
(38, 'Sportingbet', NULL, 38, 1),
(39, 'Betdaq', NULL, 39, 1),
(40, 'Sportbet', NULL, 40, 1),
(41, 'mybet', NULL, 41, 1),
(43, 'Betsson', NULL, 43, 1),
(44, 'Betfair Exchange', NULL, 44, 1),
(45, 'Chance.cz', NULL, 45, 1),
(46, 'iFortuna.cz', NULL, 46, 1),
(47, 'MAXITIP.cz', NULL, 47, 1),
(49, 'Tipsport.cz', NULL, 49, 1),
(53, 'bwin.it', NULL, 53, 1),
(54, 'Bet24', NULL, 54, 1),
(55, 'Noxwin', NULL, 55, 1),
(56, '188BET', NULL, 56, 1),
(57, 'Jetbull', NULL, 57, 1),
(59, 'BetUS', NULL, 59, 1),
(60, 'Paddy Power', NULL, 60, 1),
(64, 'Sports Alive', NULL, 64, 1),
(67, 'Internet1x2', NULL, 67, 1),
(68, 'BetCRIS', NULL, 68, 1),
(69, 'Bookmaker', NULL, 69, 1),
(70, 'Tipico', NULL, 70, 1),
(71, 'Coral', NULL, 71, 1),
(72, 'bets4all', NULL, 72, 1),
(73, 'Leonbets', NULL, 73, 1),
(74, 'Skybet', NULL, 74, 1),
(75, 'SBOBET', NULL, 75, 1),
(76, 'BetVictor', NULL, 76, 1),
(77, 'Players Only', NULL, 77, 1),
(78, 'BetOnline', NULL, 78, 1),
(79, 'BetDSI', NULL, 79, 1),
(80, '12BET', NULL, 80, 1),
(81, 'betoto', NULL, 81, 1),
(82, 'Heroes Sports', NULL, 82, 1),
(86, 'Bet7days', NULL, 86, 1),
(87, 'digibet', NULL, 87, 1),
(88, 'Legends', NULL, 88, 1),
(89, 'Sports-1', NULL, 89, 1),
(90, 'BetGun', NULL, 90, 1),
(91, 'BetPhoenix', NULL, 91, 1),
(92, 'Island Casino', NULL, 92, 1),
(93, 'The Greek', NULL, 93, 1),
(97, 'Stryyke', NULL, 97, 1),
(98, 'Superbahis', NULL, 98, 1),
(99, 'World Sports Exchange', NULL, 99, 1),
(100, 'Sportsbook', NULL, 100, 1),
(101, 'Paf', NULL, 101, 1),
(103, 'Redbet', NULL, 103, 1),
(105, 'totepool', NULL, 105, 1),
(106, 'betChronicle', NULL, 106, 1),
(107, 'Betboo', NULL, 107, 1),
(108, 'Sports', NULL, 108, 1),
(109, 'Oddsmaker', NULL, 109, 1),
(110, 'Betonline247', NULL, 110, 1),
(111, 'Instant Action Sports', NULL, 111, 1),
(112, 'Betinternet', NULL, 112, 1),
(113, 'Betcruise', NULL, 113, 1),
(114, 'Justbet', NULL, 114, 1),
(115, 'Bet770', NULL, 115, 1),
(118, 'Extrabet', NULL, 118, 1),
(119, 'Goalwin', NULL, 119, 1),
(120, 'Circlebet', NULL, 120, 1),
(121, 'Titanbet', NULL, 121, 1),
(122, 'EurosportBET', NULL, 122, 1),
(123, 'Bet911', NULL, 123, 1),
(124, 'Luxbet', NULL, 124, 1),
(125, 'Bestbet', NULL, 125, 1),
(127, 'Bestake', NULL, 127, 1),
(128, 'youwin', NULL, 128, 1),
(129, 'bwin.fr', NULL, 129, 1),
(130, 'Tobet', NULL, 130, 1),
(131, 'EurosportBET.fr', NULL, 131, 1),
(132, 'SAjOO FR', NULL, 132, 1),
(134, 'ParionsWeb.fr', NULL, 134, 1),
(135, 'betWize', NULL, 135, 1),
(136, 'BetBeast', NULL, 136, 1),
(137, 'Bet Victor', NULL, 137, 1),
(138, 'BetRedKings', NULL, 138, 1),
(139, 'France Pari', NULL, 139, 1),
(140, 'Betclic.it', NULL, 140, 1),
(141, 'Betclic.fr', NULL, 141, 1),
(142, 'Bets10', NULL, 142, 1),
(143, 'Sportingbet.au', NULL, 143, 1),
(144, 'Partybets.fr', NULL, 144, 1),
(145, 'BetVictor.de', NULL, 145, 1),
(146, 'AllYouBet', NULL, 146, 1),
(147, 'Dafabet', NULL, 147, 1),
(149, 'Interwetten.es', NULL, 149, 1),
(150, 'LBapuestas', NULL, 150, 1),
(151, 'Miapuesta', NULL, 151, 1),
(152, 'Triobet', NULL, 152, 1),
(153, 'PMU FR', NULL, 153, 1),
(154, '32Red Bet', NULL, 154, 1),
(155, 'Totosi', NULL, 155, 1),
(156, 'Getwin', NULL, 156, 1),
(157, 'Unibet.it', NULL, 157, 1),
(158, 'Sportsbet.com.au', NULL, 158, 1),
(159, 'Sisal', NULL, 159, 1),
(160, 'Unibet.fr', NULL, 160, 1),
(161, 'NetBet', NULL, 161, 1),
(162, 'STARLOTTOSPORT', NULL, 162, 1),
(163, 'eFortuna.pl', NULL, 163, 1),
(164, 'iFortuna.sk', NULL, 164, 1),
(165, 'STS', NULL, 165, 1),
(166, 'Nike', NULL, 166, 1),
(167, 'Teletip', NULL, 167, 1),
(168, 'Totomix', NULL, 168, 1),
(169, 'Tipos', NULL, 169, 1),
(170, 'Milenium', NULL, 170, 1),
(171, 'Betako', NULL, 171, 1),
(244, 'WagerWeb', NULL, 244, 1),
(293, 'Nike.sk', NULL, 293, 1),
(302, 'SBG Global', NULL, 302, 1),
(366, 'Victory Tip', NULL, 366, 1),
(367, 'Bet1128.it', NULL, 367, 1),
(368, 'Globetsport', NULL, 368, 1),
(369, 'Admiral', NULL, 369, 1),
(370, 'ShilBet', NULL, 370, 1),
(371, 'Kajot bet', NULL, 371, 1),
(372, 'WilliamHill.it', NULL, 372, 1),
(373, 'Top Bet', NULL, 373, 1),
(374, 'Interapostas', NULL, 374, 1),
(375, 'Interwetten.it', NULL, 375, 1),
(376, 'FortunaWin', NULL, 376, 1),
(377, 'Bovada', NULL, 377, 1),
(378, 'TonyBet', NULL, 378, 1),
(379, 'PaddyPower.it', NULL, 379, 1),
(380, 'Offsidebet', NULL, 380, 1),
(381, 'Marathonbet', NULL, 381, 1),
(382, 'Forvetbet', NULL, 382, 1),
(383, 'ComeOn', NULL, 383, 1),
(384, 'bet-at-home.it', NULL, 384, 1),
(385, 'SportsBettingOnline', NULL, 385, 1),
(386, 'Ucabet', NULL, 386, 1),
(387, 'SportsBetting', NULL, 387, 1),
(388, '90dakika', NULL, 388, 1),
(389, 'Heaven Bet', NULL, 389, 1),
(390, 'Matchbook', NULL, 390, 1),
(391, 'WSBets', NULL, 391, 1),
(392, 'bwin.es', NULL, 392, 1),
(393, 'Oddsring', NULL, 393, 1),
(394, 'Tipico.it', NULL, 394, 1),
(395, 'Tempobet', NULL, 395, 1),
(396, 'Oddset', NULL, 396, 1),
(397, 'Winline', NULL, 397, 1),
(398, 'NetBet.fr', NULL, 398, 1),
(399, 'Winner', NULL, 399, 1),
(400, 'HiperBet', NULL, 400, 1),
(401, 'mybet.it', NULL, 401, 1),
(402, 'Hepsibahis', NULL, 402, 1),
(403, 'Betrally', NULL, 403, 1),
(404, 'Seaniemac', NULL, 404, 1),
(405, 'Bet9', NULL, 405, 1),
(406, 'Sportium', NULL, 406, 1),
(407, 'Vistabet.gr', NULL, 407, 1),
(408, 'Sportingbet.gr', NULL, 408, 1),
(409, 'Stoiximan', NULL, 409, 1),
(410, 'Dhoze', NULL, 410, 1),
(411, 'Tipsport.sk', NULL, 411, 1),
(412, 'BET2BE', NULL, 412, 1),
(413, 'GoBetGo', NULL, 413, 1),
(414, 'SAZKAbet.cz', NULL, 414, 1),
(415, 'Dashbet', NULL, 415, 1),
(416, '18bet', NULL, 416, 1),
(417, '1xBet', NULL, 417, 1),
(418, 'Vernons', NULL, 418, 1),
(419, 'bet365.it', NULL, 419, 1),
(420, 'WonOdds', NULL, 420, 1),
(421, 'Betadonis', NULL, 421, 1),
(422, '5plusbet', NULL, 422, 1),
(423, 'NetBet.it', NULL, 423, 1),
(424, '138', NULL, 424, 1),
(425, 'RealDealBet', NULL, 425, 1),
(426, 'OtoBet', NULL, 426, 1),
(427, 'VitalBet', NULL, 427, 1),
(428, 'Kroon Casino', NULL, 428, 1),
(429, 'Betfair', NULL, 429, 1),
(430, 'Winmasters', NULL, 430, 1),
(431, 'SuperAposta', NULL, 431, 1),
(432, 'Bettilt', NULL, 432, 1),
(433, 'Guts', NULL, 433, 1),
(434, 'Jojobet', NULL, 434, 1),
(435, 'SportingbetMaster', NULL, 435, 1),
(436, 'Bet-52', NULL, 436, 1),
(437, 'BizimBahis', NULL, 437, 1),
(438, 'Fortuna.ro', NULL, 438, 1),
(439, 'Restbet', NULL, 439, 1),
(440, 'Marcaapuestas', NULL, 440, 1),
(441, 'Tipobet', NULL, 441, 1),
(442, 'VonBets', NULL, 442, 1),
(443, 'BetWorld', NULL, 443, 1),
(445, 'Sekabet', NULL, 445, 1),
(446, 'BetOlimp', NULL, 446, 1),
(447, 'Betclic.pt', NULL, 447, 1),
(448, 'Intragame', NULL, 448, 1),
(449, 'betcart', NULL, 449, 1),
(450, 'Grandbetting', NULL, 450, 1),
(451, 'Vulkanbet', NULL, 451, 1),
(452, 'BetEast', NULL, 452, 1),
(453, '1xStavka', NULL, 453, 1),
(454, 'Winline.ru', NULL, 454, 1),
(455, 'Leon.ru', NULL, 455, 1),
(456, 'Riccobet', NULL, 456, 1),
(457, 'Luckia.es', NULL, 457, 1),
(458, 'BetolimpMaster', NULL, 458, 1),
(459, 'Bet.pt', NULL, 459, 1),
(460, 'Tipbet', NULL, 460, 1),
(461, 'MrRingo', NULL, 461, 1),
(462, 'iForBet.pl', NULL, 462, 1),
(463, 'Tiplix', NULL, 463, 1),
(464, 'Bethard', NULL, 464, 1),
(465, 'Winmasters.ro', NULL, 465, 1),
(466, 'Betebet', NULL, 466, 1),
(467, 'Planetwin365', NULL, 467, 1),
(468, 'Coolbet', NULL, 468, 1),
(469, 'Babibet', NULL, 469, 1),
(470, 'Marsbet', NULL, 470, 1),
(471, '1xBet.kz', NULL, 471, 1),
(472, 'Totolotek.pl', NULL, 472, 1),
(473, 'ScandiBet', NULL, 473, 1),
(474, 'TestingOnly', NULL, 474, 1),
(475, 'Betser', NULL, 475, 1),
(476, 'Asianodds', NULL, 476, 1),
(477, 'Betmoon', NULL, 477, 1),
(478, 'Dotabet', NULL, 478, 1),
(479, 'Mobilbahis', NULL, 479, 1),
(480, 'Truvabet', NULL, 480, 1),
(481, 'BetCity.ru', NULL, 481, 1),
(482, 'BetCity', NULL, 482, 1),
(483, 'Inkabet.pe', NULL, 483, 1),
(484, 'ParionsSport', NULL, 484, 1),
(485, 'Smarkets', NULL, 485, 1),
(486, 'Olimp', NULL, 486, 1);

CREATE TABLE `cms_betit_odsp_events` (
  `id` int(11) NOT NULL,
  `league_id` int(11) NOT NULL,
  `url` varchar(300) NOT NULL,
  `date_event` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `is_finished` tinyint(1) NOT NULL DEFAULT '0',
  `is_postponed` tinyint(1) NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) NOT NULL DEFAULT '0',
  `final_result` json DEFAULT NULL,
  `result` json DEFAULT NULL,
  `has_ot` tinyint(1) DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `cms_betit_odsp_leagues` (
  `id` int(11) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_betit_odsp_periods` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `ordering` int(11) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `cms_betit_odsp_periods` (`id`, `title`, `ordering`, `is_enabled`) VALUES
(1, 'FT including OT', 1, 1),
(2, 'Full Time', 2, 1),
(3, '1st Half', 3, 1),
(4, '2nd Half', 4, 1),
(5, '1st Period', 5, 1),
(6, '2nd Period', 6, 1),
(7, '3rd Period', 7, 1),
(8, '1Q', 8, 1),
(9, '2Q', 9, 1),
(10, '3Q', 10, 1),
(11, '4Q', 11, 1),
(12, '1st Set', 12, 1),
(13, '2nd Set', 13, 1),
(14, '3rd Set', 14, 1),
(15, '4th Set', 15, 1),
(16, '5th Set', 16, 1),
(17, '1st Inning', 17, 1),
(18, '2nd Inning', 18, 1),
(19, '3rd Inning', 19, 1),
(20, '4th Inning', 20, 1),
(21, '5th Inning', 21, 1),
(22, '6th Inning', 22, 1),
(23, '7th Inning', 23, 1),
(24, '8th Inning', 24, 1),
(25, '9th Inning', 25, 1),
(26, 'Next Set', 26, 1),
(27, 'Current Set', 27, 1),
(28, 'Next Game', 28, 1),
(29, 'Current Game', 29, 1);


CREATE TABLE `cms_betit_odsp_sports` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `title_ru` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `last_update` int(11) DEFAULT NULL,
  `update_interval` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `cms_betit_odsp_sports` (`id`, `name`, `title`, `title_ru`, `url`, `is_enabled`, `last_update`, `update_interval`) VALUES
(1, 'soccer', 'Soccer', 'Футбол', 'http://www.oddsportal.com/matches/soccer/', 1, NULL, NULL),
(2, 'tennis', 'Tennis', 'Теннис', 'http://www.oddsportal.com/matches/tennis/', 1, NULL, NULL),
(3, 'basketball', 'Basketball', 'Баскетбол', 'http://www.oddsportal.com/matches/basketball/', 1, NULL, NULL),
(4, 'hockey', 'Hockey', 'Хоккей', 'http://www.oddsportal.com/matches/hockey/', 1, NULL, NULL),
(5, 'handball', 'Handball', 'Гандбол', 'http://www.oddsportal.com/matches/handball/', 1, NULL, NULL),
(6, 'baseball', 'Baseball', 'Бейсбол', 'http://www.oddsportal.com/matches/baseball/', 1, NULL, NULL),
(7, 'american-football', 'American Football', 'Американский Футбол', 'http://www.oddsportal.com/matches/american-football/', 1, NULL, NULL),
(8, 'rugby-union', 'Rugby Union', 'Регби Юнион', 'http://www.oddsportal.com/matches/rugby-union/', 1, NULL, NULL),
(9, 'rugby-league', 'Rugby league', 'Регби Лига', 'http://www.oddsportal.com/matches/rugby-league/', 1, NULL, NULL),
(10, 'volleyball', 'Volleyball', 'Волейбол', 'http://www.oddsportal.com/matches/volleyball/', 1, NULL, NULL),
(11, 'futsal', 'Futsal', 'Мини-Футбол', 'http://www.oddsportal.com/matches/futsal/', 1, NULL, NULL),
(12, 'cricket', 'Cricket', 'Крикет', 'http://www.oddsportal.com/matches/cricket/', 1, NULL, NULL),
(13, 'snooker', 'Snooker', 'Снукер', 'http://www.oddsportal.com/matches/snooker/', 1, NULL, NULL),
(14, 'boxing', 'Boxing', 'Бокс', 'http://www.oddsportal.com/matches/boxing/', 1, NULL, NULL),
(15, 'beach-volleyball', 'Beach Volleyball', 'Пляжный Волейбол', 'http://www.oddsportal.com/matches/beach-volleyball/', 1, NULL, NULL),
(16, 'aussie-rules', 'Aussie Rules', 'Австралийский Футбол', 'http://www.oddsportal.com/matches/aussie-rules/', 1, NULL, NULL),
(17, 'badminton', 'Badminton', 'Бадминтон', 'http://www.oddsportal.com/matches/badminton/', 1, NULL, NULL),
(18, 'pesapallo', 'Pesapallo', 'Финский бейсбол', 'http://www.oddsportal.com/matches/pesapallo/', 1, NULL, NULL),
(19, 'esports', 'eSports', 'Кибер-спорт', 'http://www.oddsportal.com/matches/esports/', 1, NULL, NULL),
(20, 'mma', 'MMA', 'MMA', 'http://www.oddsportal.com/matches/mma/', 1, NULL, NULL),
(21, 'floorball', 'Floorball', 'Флорбол', 'http://www.oddsportal.com/matches/floorball/', 1, NULL, NULL),
(22, 'bandy', 'Bandy', 'Хоккей с мячом', 'http://www.oddsportal.com/matches/bandy/', 1, NULL, NULL),
(23, 'darts', 'Darts', 'Дартс', 'http://www.oddsportal.com/matches/darts/', 1, NULL, NULL),
(24, 'water-polo', 'Water Polo', 'Водное Поло', 'http://www.oddsportal.com/matches/water-polo/', 1, NULL, NULL),
(25, 'beach-soccer', 'Beach Soccer', 'Пляжный Футбол', 'http://www.oddsportal.com/matches/beach-soccer/', 1, NULL, NULL);

CREATE TABLE `cms_betit_pinc_events` (
  `id` int(11) NOT NULL,
  `league_id` int(11) NOT NULL,
  `date_event` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `periods` text,
  `result` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `cms_betit_pinc_leagues` (
  `id` int(11) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `country_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_betit_pinc_periods` (
  `sport_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_short` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cms_betit_pinc_periods` (`sport_id`, `number`, `title`, `title_short`, `ordering`) VALUES
(1, 0, 'Match', 'FT', 0),
(1, 1, '1st Game', '1stG', 0),
(1, 2, '2nd Game', '2ndG', 0),
(1, 3, '3rd Game', '3rdG', 0),
(2, 0, 'Match', 'FT', 0),
(2, 1, '1st Half', '1st H', 0),
(2, 2, '2nd Half', '2nd H', 0),
(3, 0, 'Game', 'FT', 0),
(3, 1, '1st Half', '1st H', 0),
(3, 2, '2nd Half', '2nd H', 0),
(3, 3, '1st Inning', '1st Inn', 0),
(4, 0, 'Game', 'FT', 0),
(4, 1, '1st Half', '1st H', 0),
(4, 2, '2nd Half', '2nd H', 0),
(4, 3, '1st Quarter', '1st Q', 0),
(4, 4, '2nd Quarter', '2nd Q', 0),
(4, 5, '3rd Quarter', '3rd Q', 0),
(4, 6, '4th Quarter', '4th Q', 0),
(5, 0, 'Match', 'FT', 0),
(5, 1, '1st Set', '1st S', 0),
(5, 2, '2nd Set', '2nd S', 0),
(5, 3, '3rd Set', '3rd S', 0),
(6, 0, 'Fight', 'FT', 0),
(7, 0, 'Match', 'FT', 0),
(8, 0, 'Match', 'FT', 0),
(8, 1, '1st Inning', '1st Inn', 0),
(8, 2, '2nd Inning', '2nd Inn', 0),
(9, 0, 'Game', 'FT', 0),
(9, 1, '1st End', '1st End', 0),
(10, 0, 'Match', 'FT', 0),
(10, 1, '1st Set', '1st S', 0),
(10, 2, '2nd Set', '2nd S', 0),
(10, 3, '3rd Set', '3rd S', 0),
(10, 4, '4th Set', '4th S', 0),
(10, 5, '5th Set', '5th S', 0),
(11, 0, 'Match', 'FT', 0),
(11, 1, '1st Leg', '1st Leg', 0),
(11, 2, '2nd Leg', '2nd Leg', 0),
(11, 3, '3rd Leg', '3rd Leg', 0),
(11, 4, '4th Leg', '4th Leg', 0),
(11, 5, '5th Leg', '5th Leg', 0),
(11, 6, '6th Leg', '6th Leg', 0),
(12, 0, 'Match', 'Match', 0),
(12, 1, 'Map 1', 'Map 1', 0),
(12, 2, 'Map 2', 'Map 2', 0),
(12, 3, 'Map 3', 'Map 3', 0),
(12, 4, 'Map 4', 'Map 4', 0),
(12, 5, 'Map 5', 'Map 5', 0),
(12, 6, 'Map 6', 'Map 6', 0),
(12, 7, 'Map 7', 'Map 7', 0),
(13, 0, 'Match', 'FT', 0),
(13, 1, '1st Half', '1st H', 0),
(13, 2, '2nd Half', '2nd H', 0),
(14, 0, 'Match', 'FT', 0),
(14, 1, '1st Period', '1st P', 0),
(14, 2, '2nd Period', '2st P', 0),
(14, 3, '3rd Period', '2st P', 0),
(15, 0, 'Game', 'FT', 0),
(15, 1, '1st Half', '1st H', 0),
(15, 2, '2nd Half', '2nd H', 0),
(15, 3, '1st Quarter', '1st Q', 0),
(15, 4, '2nd Quarter', '2nd Q', 0),
(15, 5, '3rd Quarter', '3rd Q', 0),
(15, 6, '4th Quarter', '4th Q', 0),
(16, 0, 'Match', 'FT', 0),
(16, 1, '1st Half', '1st H', 0),
(16, 2, '2nd Half', '2nd H', 0),
(17, 0, 'Matchups', 'FT', 0),
(18, 0, 'Match', 'FT', 0),
(18, 1, '1st Half', '1st H', 0),
(18, 2, '2nd Half', '2nd H', 0),
(19, 0, 'Game', 'FT', 0),
(19, 1, '1st Period', '1st P', 0),
(19, 2, '2nd Period', '2nd P', 0),
(19, 3, '3rd Period', '3rd P', 0),
(20, 0, 'Race', 'FT', 0),
(22, 0, 'Fight', 'FT', 0),
(22, 1, 'Round 1', 'Rnd 1', 0),
(22, 2, 'Round 2', 'Rnd 2', 0),
(22, 3, 'Round 3', 'Rnd 3', 0),
(22, 4, 'Round 4', 'Rnd 4', 0),
(22, 5, 'Round 5', 'Rnd 5', 0),
(24, 0, 'Election', 'FT', 0),
(26, 0, 'Match', 'FT', 0),
(26, 1, '1st Half', '1st H', 0),
(26, 2, '2nd Half', '2nd H', 0),
(27, 0, 'Match', 'FT', 0),
(27, 1, '1st Half', '1st H', 0),
(27, 2, '2nd Half', '2nd H', 0),
(28, 0, 'Match', 'FT', 0),
(28, 1, '1st Frame', '1st F', 0),
(29, 0, 'Match', 'FT', 0),
(29, 1, '1st Half', '1st H', 0),
(29, 2, '2nd Half', '2nd H', 0),
(30, 0, 'Game', 'FT', 0),
(30, 1, '1st Half', '1st H', 0),
(30, 2, '2st Half', '2st H', 0),
(31, 0, 'Match', 'FT', 0),
(31, 1, '1st Game', '1st G', 0),
(31, 2, '2nd Game', '2nd G', 0),
(31, 3, '3rd Game', '3rd G', 0),
(31, 4, '4th Game', '4th G', 0),
(31, 5, '5th Game', '5th G', 0),
(32, 0, 'Match', 'FT', 0),
(32, 1, '1st Game', '1st G', 0),
(32, 2, '2nd Game', '2nd G', 0),
(32, 3, '3rd Game', '1st G', 0),
(32, 4, '4th Game', '1st G', 0),
(32, 5, '5th Game', '1st G', 0),
(32, 6, '6th Game', '1st G', 0),
(33, 0, 'Match', 'FT', 0),
(33, 1, '1st Set', '1st S', 0),
(33, 2, '2nd Set', '2nd S', 0),
(33, 3, '3rd Set', '3rd S', 0),
(33, 4, '4th Set', '4th S', 0),
(33, 5, '5th Set', '5th S', 0),
(34, 0, 'Match', 'FT', 0),
(34, 1, '1st Set', '1st S', 0),
(34, 2, '2nd Set', '2nd S', 0),
(34, 3, '3rd Set', '3rd S', 0),
(34, 4, '4th Set', '4th S', 0),
(34, 5, '5th Set', '5th S', 0),
(35, 0, 'Game', 'FT', 0),
(35, 1, '1st Set', '1st S', 0),
(35, 2, '2nd Set', '2nd S', 0),
(35, 3, '3rd Set', '3rd S', 0),
(35, 4, '4th Set', '4th S', 0),
(35, 5, '5th Set', '5th S', 0),
(36, 0, 'Match', 'FT', 0),
(36, 1, '1st Period', '1st P', 0),
(36, 2, '2nd Period', '2nd P', 0),
(36, 3, '3rd Period', '3rd P', 0),
(36, 4, '4th Period', '4th P', 0),
(39, 0, 'Game', 'FT', 0),
(39, 1, '1st Half', '1st H', 0),
(39, 2, '2nd Half', '2nd H', 0),
(39, 3, '1st Quarter', '1st Q', 0),
(39, 4, '2nd Quarter', '2nd Q', 0),
(39, 5, '3rd Quarter', '3rd Q', 0),
(39, 6, '4th Quarter', '4th Q', 0),
(40, 0, 'Matchups', 'FT', 0),
(41, 0, 'Matchups', 'FT', 0),
(42, 0, 'Matchups', 'FT', 0),
(43, 0, 'Matchups', 'FT', 0),
(44, 0, 'Matchups', 'FT', 0),
(45, 0, 'Matchups', 'FT', 0),
(46, 0, 'Matchups', 'FT', 0),
(47, 0, 'Matchups', 'FT', 0),
(48, 0, 'Matchups', 'FT', 0),
(49, 0, 'Matchups', 'FT', 0),
(50, 0, 'Matchups', 'FT', 0),
(51, 0, 'Matchups', 'FT', 0),
(52, 0, 'Matchups', 'FT', 0),
(53, 0, 'Matchups', 'FT', 0),
(54, 0, 'Matchups', 'FT', 0),
(55, 0, 'Match', 'Match', 0),
(56, 0, 'Matchups', 'FT', 0),
(57, 0, 'Matchups', 'FT', 0),
(58, 0, 'Matchups', 'FT', 0),
(60, 0, 'Matchups', 'FT', 0),
(62, 0, 'Matchup', 'FT', 0);


CREATE TABLE `cms_betit_pinc_sports` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_ru` varchar(255) DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `last_update` int(11) DEFAULT NULL,
  `update_interval` int(11) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `cms_betit_pinc_sports` (`id`, `title`, `title_ru`, `is_enabled`, `last_update`, `update_interval`, `ordering`) VALUES
(1, 'Badminton', 'Бадминтон', 1, NULL, NULL, 0),
(2, 'Bandy', 'Хоккей с мячом', 1, NULL, NULL, 0),
(3, 'Baseball', 'Бейсбол', 1, NULL, NULL, 0),
(4, 'Basketball', 'Баскетбол', 1, NULL, NULL, 0),
(5, 'Beach Volleyball', 'Пляжный волейбол', 1, NULL, NULL, 0),
(6, 'Boxing', 'Бокс', 1, NULL, NULL, 0),
(7, 'Chess', 'Шахматы', 1, NULL, NULL, 0),
(8, 'Cricket', 'Крикет', 1, NULL, NULL, 0),
(9, 'Curling', 'Кёрлинг', 1, NULL, NULL, 0),
(10, 'Darts', 'Дартс', 1, NULL, NULL, 0),
(11, 'Darts (Legs)', 'Дартс (ноги)', 1, NULL, NULL, 0),
(12, 'E Sports', 'Киберспорт', 1, NULL, NULL, 0),
(13, 'Field Hockey', 'Хоккей на траве', 1, NULL, NULL, 0),
(14, 'Floorball', 'Флорбол', 1, NULL, NULL, 0),
(15, 'Football', 'Американский Футбол', 1, NULL, NULL, 0),
(16, 'Futsal', 'Минифутбол', 1, NULL, NULL, 0),
(17, 'Golf', 'Гольф', 1, NULL, NULL, 0),
(18, 'Handball', 'Гандонбол', 1, NULL, NULL, 0),
(19, 'Hockey', 'Хоккей', 1, NULL, NULL, 0),
(20, 'Horse Racing', 'Лошадиные забеги', 1, NULL, NULL, 0),
(22, 'Mixed Martial Arts', 'MMA', 1, NULL, NULL, 0),
(24, 'Politics', 'Политикс', 1, NULL, NULL, 0),
(26, 'Rugby League', 'Рэгби лига', 1, NULL, NULL, 0),
(27, 'Rugby Union', 'Рэгби Юнион', 1, NULL, NULL, 0),
(28, 'Snooker', 'Снукер', 1, NULL, NULL, 0),
(29, 'Soccer', 'Футбол', 1, NULL, NULL, 0),
(30, 'Softball', 'Софтбол', 1, NULL, NULL, 0),
(31, 'Squash', 'Сквош', 1, NULL, NULL, 0),
(32, 'Table Tennis', 'Пинг-Понг', 1, NULL, NULL, 0),
(33, 'Tennis', 'Теннис', 1, NULL, NULL, 0),
(34, 'Volleyball', 'Волейбол', 1, NULL, NULL, 0),
(35, 'Volleyball (Points)', 'Волейбол (очки)', 1, NULL, NULL, 0),
(36, 'Water Polo', 'Водное поло', 1, NULL, NULL, 0),
(39, 'Aussie Rules', 'Австралийский футбол', 1, NULL, NULL, 0),
(40, 'Alpine Skiing', 'Альпийские лыжи', 1, NULL, NULL, 0),
(41, 'Biathlon', 'Биатлон', 1, NULL, NULL, 0),
(42, 'Ski Jumping', 'Прыжки на лыжах с трамплина', 1, NULL, NULL, 0),
(43, 'Cross Country', 'Легкоатлетический кросс', 1, NULL, NULL, 0),
(44, 'Formula 1', 'Формула 1', 1, NULL, NULL, 0),
(45, 'Cycling', 'Велосипедные гонки', 1, NULL, NULL, 0),
(46, 'Bobsleigh', 'Бобслей', 1, NULL, NULL, 0),
(47, 'Figure Skating', 'Фигурное катание', 1, NULL, NULL, 0),
(48, 'Freestyle Skiing', 'Фристайл', 1, NULL, NULL, 0),
(49, 'Luge', 'Ссаный спорт', 1, NULL, NULL, 0),
(50, 'Nordic Combined', 'Лыжное двоеборье', 1, NULL, NULL, 0),
(51, 'Short Track', 'Шорт-трек', 1, NULL, NULL, 0),
(52, 'Skeleton', 'Скелетон', 1, NULL, NULL, 0),
(53, 'Snow Boarding', 'Сноуборд', 1, NULL, NULL, 0),
(54, 'Speed Skating', 'Скоростной спуск', 1, NULL, NULL, 0),
(55, 'Olympics', 'Олимпиада', 1, NULL, NULL, 0),
(56, 'Athletics', 'Атлетика', 1, NULL, NULL, 0),
(57, 'Crossfit', 'Кроссфит', 1, NULL, NULL, 0),
(58, 'Entertainment', 'Разработка', 1, NULL, NULL, 0),
(60, 'Drone Racing', 'Гонки на дронах', 1, NULL, NULL, 0),
(62, 'Poker', 'Покер', 1, NULL, NULL, 0);


CREATE TABLE `cms_betit_users` (
  `id` int(11) NOT NULL,
  `start_bankroll` int(11) NOT NULL,
  `history` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cms_users_fields` (`id`, `ctype_id`, `name`, `title`, `hint`, `ordering`, `fieldset`, `type`, `is_in_list`, `is_in_item`, `is_in_filter`, `is_private`, `is_fixed`, `is_fixed_type`, `is_system`, `values`, `options`, `groups_read`, `groups_edit`, `filter_view`) VALUES
(NULL, NULL, 'betit_bankroll', 'Начальный банкролл', NULL, 4, 'Прогнозы', 'number', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '---\nfilter_range: null\nunits: у.е.\nlabel_in_list: none\nlabel_in_item: left\nis_required: 1\nis_digits: 1\nis_alphanumeric: null\nis_email: null\nis_unique: null\n', '---\n- 0\n', '---\n- 0\n', '---\n- 0\n'),
(NULL, NULL, 'betit_state_percent', 'Ставка по-умолчанию', NULL, 5, 'Прогнозы', 'string', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '---\nmin_length: 0\nmax_length: 5\nshow_symbol_count: null\nis_autolink: null\nlabel_in_list: none\nlabel_in_item: left\nis_required: null\nis_digits: null\nis_alphanumeric: null\nis_email: null\nis_unique: null\n', '---\n- 0\n', '---\n- 0\n', '---\n- 0\n');


ALTER TABLE `cms_betit_events`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_forecasts`
--
ALTER TABLE `cms_betit_forecasts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_odsp_bettings`
--
ALTER TABLE `cms_betit_odsp_bettings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_odsp_bookmakers`
--
ALTER TABLE `cms_betit_odsp_bookmakers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_odsp_events`
--
ALTER TABLE `cms_betit_odsp_events`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url` (`url`) USING BTREE;

--
-- Индексы таблицы `cms_betit_odsp_leagues`
--
ALTER TABLE `cms_betit_odsp_leagues`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url` (`url`) USING BTREE;

--
-- Индексы таблицы `cms_betit_odsp_periods`
--
ALTER TABLE `cms_betit_odsp_periods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_odsp_sports`
--
ALTER TABLE `cms_betit_odsp_sports`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_pinc_events`
--
ALTER TABLE `cms_betit_pinc_events`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_pinc_leagues`
--
ALTER TABLE `cms_betit_pinc_leagues`
  ADD UNIQUE KEY `id` (`id`,`sport_id`);

--
-- Индексы таблицы `cms_betit_pinc_periods`
--
ALTER TABLE `cms_betit_pinc_periods`
  ADD UNIQUE KEY `sport_id` (`sport_id`,`number`);

--
-- Индексы таблицы `cms_betit_pinc_sports`
--
ALTER TABLE `cms_betit_pinc_sports`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cms_betit_users`
--
ALTER TABLE `cms_betit_users`
  ADD PRIMARY KEY (`id`) USING BTREE;


ALTER TABLE `cms_betit_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cms_betit_forecasts`
--
ALTER TABLE `cms_betit_forecasts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `cms_betit_odsp_bettings`
--
ALTER TABLE `cms_betit_odsp_bettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `cms_betit_odsp_bookmakers`
--
ALTER TABLE `cms_betit_odsp_bookmakers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=487;

--
-- AUTO_INCREMENT для таблицы `cms_betit_odsp_events`
--
ALTER TABLE `cms_betit_odsp_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT для таблицы `cms_betit_odsp_leagues`
--
ALTER TABLE `cms_betit_odsp_leagues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=568;

--
-- AUTO_INCREMENT для таблицы `cms_betit_odsp_periods`
--
ALTER TABLE `cms_betit_odsp_periods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `cms_betit_odsp_sports`
--
ALTER TABLE `cms_betit_odsp_sports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `cms_betit_pinc_sports`
--
ALTER TABLE `cms_betit_pinc_sports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
